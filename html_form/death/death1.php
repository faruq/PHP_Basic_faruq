<html>
	<head>
		<title>Death Certificate Form</title>
	</head>
	<body>
		<fieldset>
			<table>
				<form action="getvalue.php" method="post">
					<center><h2><u>Appendix A-Simple Us Death Certificate Form.</u></h2></center>
						<tr>
							<td>First Name:<input type="text" name="f_name" id="f_name" value="" /></td>
							<td>Last Name:<input type="text" name="l_name" id="l_name" value="" /></td>
							<td>Middle Name:<input type="text" name="m_name" id="m_name" value="" /></td>
						</tr>
						<tr>
							<td>Death of Birth:<input type="date" name="dob" id="dob" value="" /></td>
							<td>Gender:<select name="gen">
									<option>Select Your Gender</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</select>
							</td>
							<td>Social Security Number:<input type="number" name="ssn" id="ssn" value="" /></td>
							<td>Facility Name:<input type="text" name="fn" id="fn" value="" /></td>
						</tr>
						
						<!--second side-->
						
						<tr>
							<td>
								<center><h4><u>Decent of Hispanic origin.</u></h4></center>
								<input type="radio" name="no" value="No,not Spanish/Hispanic/Latino" checked>No,not Spanish/Hispanic/Latino
								<input type="radio" name="no" value="Yes,Mexican,Mexican American,Chicano">Yes,Mexican,Mexican American,Chicano
								<input type="radio" name="no" value="Yes,Puerto Rican">Yes,Puerto Rican
								<input type="radio" name="no" value="Yes,Cuban">Yes,Cuban<br />
								<input type="radio" name="no" value="No,Other Spanish/Hispanic/Latino(Specify)">No,Other Spanish/Hispanic/Latino(Specify)
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="text" name="txt" id="txt" value="" /></td>
						</tr>
						
							<!--3rd side-->
							
						<tr>
							<td>
								<center><h4><u>Decend's Race.</u></h4>
								<b>Check one more race to indicate what the decent considered himself or herself to be.</b></center>
								<input type="radio" name="dec_race" value="White" checked>White
								<input type="radio" name="dec_race" value="Filipino">Filipino
								<input type="radio" name="dec_race" value="Native Hawaiian">Native Hawaiian<br />
							</td>
						</tr>	
						<tr>
							<td><input type="radio" name="dec_race" value="Black or African American">Black or African American
								<input type="radio" name="dec_race" value="Japanese">Japanese
								<input type="radio" name="dec_race" value="Guamanian or Chamorro">Guamanian or Chamorro<br />
							</td>
						</tr>
						<tr>
							<td><input type="radio" name="dec_race" value="African Indian or Alaska Native(name of the enrolled or principal tribe)">African Indian or Alaska Native(name of the enrolled or principal tribe)<br />
								<input type="text" name="dec_txt1" value="">
								<input type="radio" name="dec_race" value="Korean">Korean
								<input type="radio" name="dec_race" value="Samoan">Samoan<br />
							</td>
						</tr>
						<tr>
							<td><input type="radio" name="dec_race" value="Vietnames">Vietnames
								<input type="radio" name="dec_race" value="Other pacific Islander(specify)">Other pacific Islander(specify)<br />
								<input type="text" name="dec_txt2" value="">
								<input type="radio" name="dec_race" value="Asian Indian">Asian Indian<br />
							</td>
						</tr>
						<tr>
							<td><input type="radio" name="dec_race" value="Other Asian(specify)">Other Asian(specify)<br />
								<input type="text" name="dec_txt3" value="">
								<input type="radio" name="dec_race" value="Chines">Chines
								<input type="radio" name="dec_race" value="Other(Specify)">Other(Specify)<br />
								<input type="text" name="dec_txt4" value="">
							</td>
						</tr>
						
						<!--4th side-->
						
						<tr>
							<td align="center"><h4><u>Item Must be Completed by person who pronounces or citified death.</u></h4></td>
						</tr>
						<tr>
							<td>Date pronounced dead:<input type="date" name="dpd" id="dpd" value="" /></td>
							<td>Time Pronounced dead:<input type="time" name="tpd" id="tpd" value="" /></td>
							<td>Signature of person pronouncing death:<input type="text" name="pdate" id="pdate" value="" /></td>
						</tr>
						
						<tr>
							<td>Licence Number:<input type="number" name="lnum" id="lnum" value="" /></td>
							<td>Date signed:<input type="date" name="datesign" id="datesign" value="" /></td>
							<td>Actual or presumed date of birth:<input type="date" name="dob2" id="dob2" value="" /></td>
						</tr>
						
						<tr>
							<td>Actual or presumed time of death:<input type="time" name="tod" id="tod" value="" /></td>
							<td>Was medical examiner or coroner contacted :<input type="radio" name="coroner" value="Yes" checked>Yes
																		   <input type="radio" name="coroner" value="No">No</td>
						</tr>
						
						<!--5th side-->
						
						<tr>
							<td align="center"><h4><u>Cause of death(see instruction and example).PART 1.</u></h4></td>
						</tr>
						<tr>
							<td>a.immediate cause(Final disease or condition resulting death):<input type="text" name="imd_cas" id="imd_cas" value="" /></td>
							<td>Due to(or as a consequence of):<input type="text" name="due_to" id="due_to" value="" /></td>
							<td>Onset of death:<input type="text" name="ondeath" id="ondeath" value="" /></td>
						</tr>
						
						<tr>
							<td>b.Sequentially list conditons(if any, leading to the cause list on line a.):<input type="text" name="seq_list" id="seq_list" value="" /></td>
							<td>Due to(or as a consequence of):<input type="text" name="due_to_b" id="due_to_b" value="" /></td>
							<td>Onset of death:<input type="text" name="ondeath_b" id="ondeath_b" value="" /></td>
						</tr>
						
						<tr>
							<td>c.Enter the underlying cause(diseases or injury that initiated the event of resulting of death ):<input type="text" name="under_cas" id="under_cas" value="" /></td>
							<td>Due to(or as a consequence of):<input type="text" name="due_to_c" id="due_to_c" value="" /></td>
							<td>Onset of death:<input type="text" name="ondeath_c" id="ondeath_c" value="" /></td>
						</tr>
						
						<tr>
							<td>d.Last:<input type="text" name="last" id="last" value="" /></td>
							<td>Onset of death:<input type="text" name="ondeath_d" id="ondeath_d" value="" /></td>
						</tr>
						
						<!--6th side-->	
						
						<tr>
							<td align="center"><b><u>PART 2.Enter other significant conditions contributing to death but not resulting in the underlying cause given in PART 1.</u></b></td>
						</tr>
						<tr>
							<td>
								<textarea name="message" rows="3" cols="30">
								</textarea>
							</td>
						</tr>
						
						<tr>
							<td>Was an autospy performed?</td>
						</tr>
						<tr>
								<td><input type="radio" name="autospy" value="Yes" checked>Yes
								<input type="radio" name="autospy" value="No">No</td>
						</tr>
						
						<tr>
							<td>Were autospy finding available to complete the cause of death?</td>
						</tr>
						<tr>
								<td><input type="radio" name="autospy2" value="Yes" checked>Yes
								<input type="radio" name="autospy2" value="No">No</td>
						</tr>
						
						<tr>	
							<td>Did tobacco use contribute the death?</td>
						</tr>
						<tr>
							<td><input type="radio" name="tobacco" value="Yes" checked>Yes
								<input type="radio" name="tobacco" value="No">No
								<input type="radio" name="tobacco" value="Probably" >Probably
								<input type="radio" name="tobacco" value="unknown">unknown</td>
								
						</tr>
									
						<!--7th side-->
											
						<tr>
							<td align="center"><b><u>If Female.</u></b></td>
						</tr>
						
						<tr>
							<td>
								<input type="radio" name="if_female" value="Not pregnant within past year" checked>Not pregnant within past year
								<input type="radio" name="if_female" value="pregnant at time of death">pregnant at time of death
								<input type="radio" name="if_female" value="Not pregnant but pregnant within 42 days of death">Not pregnant but pregnant within 42 days of death
							</td>
						</tr>
						
						<tr>
							<td>
								<input type="radio" name="if_female" value="Not pregnant but pregnant within 43 days to 1 year before death">Not pregnant but pregnant within 43 days to 1 year before death
								<input type="radio" name="if_female" value="Unknown if pregnant within the past year">Unknown if pregnant within the past year
							</td>
						</tr>
								
						<!--8th side-->
										
						<tr>
							<td align="center"><b><u>Manner of death.</u></b></td>
						</tr>
					
						<tr>
							<td>
								<input type="radio" name="manner_of_death" value="Natural" checked>Natural 
								<input type="radio" name="manner_of_death" value="Homicide">Homicide
								<input type="radio" name="manner_of_death" value="Accident">Accident
								<input type="radio" name="manner_of_death" value="Pending Investigation">Pending Investigation
								<input type="radio" name="manner_of_death" value="Suicide">Suicide
								<input type="radio" name="manner_of_death" value="Could not be determined">Could not be determined
							</td>
						</tr>
						
						<!--9th side-->
					
						<tr>
							<td align="center"><b><u>Injury.</u></b></td>
						</tr>
						
						<tr>
							<td>Date of injury:<input type="date" name="doi" id="doi" value="" /></td>
							<td>Time of injury:<input type="time" name="toi" id="toi" value="" /></td>
							<td>Place of injury:<input type="text" name="poi" id="poi" value="" /></td>
							<td>Injury at work:<input type="radio" name="injury" value="Yes" checked>Yes
												<input type="radio" name="injury" value="No">No</td>
						</tr>
											
						<!--10th side-->
									
						<tr>
							<td align="center"><b><u>Location of injury.</u></b></td>
						</tr>

						<tr>
							<td>State:<input type="text" name="state" id="state" value="" /></td>
							<td>City or Town:<input type="text" name="cot" id="cot" value="" /></td>
							<td>Street Number:<input type="number" name="number" id="number" value="" /></td>
						</tr>
						<tr>
							<td>Apartment Number:<input type="number" name="number2" id="number2" value="" /></td>
							<td>Zip Code:<input type="number" name="number3" id="number3" value="" /></td>
							
						</tr>
						
						<!--11th side-->
					
						<tr>
							<td align="center"><b><u>Describe How Injury Occurred.</u></b></td>
						</tr>
						<tr>
							<td><textarea name="description" cols="80" rows="5" ></textarea></td>
						</tr>
						
						<tr>
							<td>Specify:</td>
						</tr>
						
						<tr>
							<td>
								<input type="radio" name="specify" value="Driver/Operator" checked>Driver/Operator
								<input type="radio" name="specify" value="Passenger">Passenger
								<input type="radio" name="specify" value="Pedestrian">Pedestrian
								<input type="radio" name="specify" value="Other(specify)">Other(specify)
								<input type="text" name="specify_txt" value="">
							</td>
						</tr>
										
						<!--12th side-->
										
						<tr>
							<td align="center"><b><u>Certifier.</u></b></td>
						</tr>
						
						<tr>
							<td>Check only one:</td>
						</tr>
						
						<tr>
							<td><input type="radio" name="certifier" value="certifying physician" checked>certifying physician</td>
						</tr>
						
						<tr>
							<td><input type="radio" name="certifier" value="Pronouncing and certifying physician">Pronouncing and certifying physician</td>
						</tr>
							<td><input type="radio" name="certifier" value="Medical Examiner or coroner">Medical Examiner or coroner</td>
						</tr>
						
						<tr>
							<td>Signature of certifier:<input type="text" name="soc" value=""></td>
						</tr>
						
						<!--13th side-->
						
						<tr>
							<td align="center"><b><u>Person Completing cause of death.</u></b></td>
						</tr>
						
						<tr>
							<td>Name:<input type="text" name="p_name" id="p_name" value="" /></td>
							<td>Address:<input type="text" name="address" id="address" value="" /></td>
							<td>Zip Code:<input type="number" name="num" id="num" value="" /></td>
						</tr>
						<tr>
							<td>Title of Certifier:<input type="text" name="toc" id="toc" value="" /></td>
							<td>Licence Number:<input type="number" name="num2" id="num2" value="" /></td>
							<td>Date of Certified:<input type="date" name="doc" id="doc" value="" /></td>
						</tr>
						
						<tr>
							<td><input type="submit" name="sub" id="sub" value="Submit"></td>
						</tr>
															
				</form> 
			</table>
		</fieldset><br />
									
			<table>
				<tr><td>Rev 1.1-2013-09-13</td></tr>
				<tr><td>copyright &copy; 2013:IHE international,Inc.</td></tr>
			</table>
										
	</body>
</html>
