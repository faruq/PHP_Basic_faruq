<?php
include_once('app/application.php');
//debug($_SESSION);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> Death Certified </title>
    </head> 
    <body>
        <p><a href="create.html">Create New</a> </p>
        
		<fieldset><legend> Death Reporting For Vital Records / 1st part </legend>
					<h4> Decedent's Name (Include AKA's if any)  </h4>
        
        <table border="1">
            <tr>
                <th>Sl</th>
                <th>First Name</th>
                <th>Middle Name</th>
				<th>Last Name</th>
				<th>Date of Birth</th>
                <th>Gender</th>
				<th>S.S Number</th>
                <th>Faculty Name</th>
                <th>Action</th>
            </tr>
			
           <?php
			   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
			   foreach($_SESSION['details'] as $key=>$value){
           ?>    
           
            <tr>
                <td><?php echo $key+1;?></td>
				
                <td><?php 
					if(array_key_exists('f_name', $value) && !empty($value['f_name'])){
						echo $value['f_name'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('m_name', $value) && !empty($value['m_name'])){
						echo $value['m_name'];
					}
                ?></td>
				
				<td><?php 
					if(array_key_exists('l_name', $value) && !empty($value['l_name'])){
						echo $value['l_name'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('dob', $value) && !empty($value['dob'])){
						echo $value['dob'];
					}
                ?></td>
				
				 <td><?php
					if(array_key_exists('gender', $value) && !empty($value['gender'])){
						echo $value['gender'];
					}
                ?></td>
				
				<td><?php 
					if(array_key_exists('ssn', $value) && !empty($value['ssn'])){
						echo $value['ssn'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('fn', $value) && !empty($value['fn'])){
						echo $value['fn'];
					}
                ?></td>
				
                <td>
                    <a href="Details.php?id=<?php echo $key;  ?>">View</a>
                    |<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
                    |<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
                </td>
				
            </tr>
			
            <?php
				 }
			   }else{
           ?>
				<tr><td colspan="4">
					No data available
					</td>  
				</tr>
            <?php
           		}
            ?>
        </table>
        </fieldset>
		
		<fieldset><legend> Decedent of Hispanic Origin / 2nd part </legend>
					<h4> Check the box that best describes whether the decedent is Spanish I Hispanic I Latino. Check the "No" box if decedent is not Spanish I Hispanic /Latino </h4>
        
        <table border="1">
			
				<tr>
					<th> SN </th>
					<th> Decedent of Hispanic Origin </th>
					<th> Decedent of Hispanic Origin / text </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
				?>
			
				<tr>
					<td> <?php echo $key+1; ?></td>
					
					<td><?php 
					if(array_key_exists('origin', $value) && !empty($value['origin'])){
						echo $value['origin'];
					}
                    ?></td>
				  
					<td><?php
						if(array_key_exists('txt1', $value) && !empty($value['txt1'])){
							echo $value['txt1'];
						}
					?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">View</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
					<?php
						 }
					   }else{
				   ?>
						<tr>
							<td colspan="4">
								No data available
							</td>  
						</tr>
					<?php
						}
					?>
			
			</table>
		</fieldset>
		
		<fieldset> <legend> Decedents Race / 3rd part </legend>
					<h4> check one or more races to indicate what the Decedent considered himself or herself to be </h4>
			
		<table border="1">
				<tr>
					<th> SN </th>
					<th> Decedents Race </th>
					<th> Decedents Race / text</th>
					<th> Decedents Race / text2</th>
					<th> Decedents Race / text3</th>
					<th> Decedents Race / text4</th>
					<th> Action</th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
				?> 
			
				<tr>
					<td> <?php echo $key+1; ?></td>
					
					<td><?php 
					if(array_key_exists('race', $value) && !empty($value['race'])){
						echo $value['race'];
					}
                    ?></td>
					
					<td><?php
						if(array_key_exists('txt2', $value) && !empty($value['txt2'])){
							echo $value['txt2'];
						}
					?></td>
				  
					<td><?php
						if(array_key_exists('txt42', $value) && !empty($value['txt42'])){
							echo $value['txt42'];
						}
					?></td>
					
					<td><?php
						if(array_key_exists('txt52', $value) && !empty($value['txt52'])){
							echo $value['txt52'];
						}
					?></td>
					
					<td><?php
						if(array_key_exists('txt62', $value) && !empty($value['txt62'])){
							echo $value['txt62'];
						}
					?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">View</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
					<?php
						 }
					   }else{
				   ?>
						<tr>
							<td colspan="4">
								No data available
							</td>  
						</tr>
					<?php
						}
					?>
				
		</table>
		</fieldset>
		
		<fieldset> <legend> Items Must be Completed by Person Who Pronounces or Certifies Death. / 4th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> Date Pronounced Dead </th>
					<th> License Number </th>
					<th> Actual Or Presumed Tirne Of Death </th>
					<th> Signalure Of Person Pronouncing Death </th>
					<th> Actual Or Presumed Date Of Blab </th>
					<th> Time Pronounced Dead </th>
					<th> Date Signed </th>
					<th> Was Medical Examinar Or Coroner Contacted ? </th>
					<th> Action </th>
				</tr>
				
			<?php
			   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
			   foreach($_SESSION['details'] as $key=>$value){
           ?>    
           
            <tr>
                <td><?php echo $key+1;?></td>
				
                <td><?php 
					if(array_key_exists('txt3', $value) && !empty($value['txt3'])){
						echo $value['txt3'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('txt4', $value) && !empty($value['txt4'])){
						echo $value['txt4'];
					}
                ?></td>
				
				<td><?php 
					if(array_key_exists('txt5', $value) && !empty($value['txt5'])){
						echo $value['txt5'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('txt6', $value) && !empty($value['txt6'])){
						echo $value['txt6'];
					}
                ?></td>
				
				 <td><?php
					if(array_key_exists('txt7', $value) && !empty($value['txt7'])){
						echo $value['txt7'];
					}
                ?></td>
				
				<td><?php 
					if(array_key_exists('txt8', $value) && !empty($value['txt8'])){
						echo $value['txt8'];
					}
                  ?></td>
				  
				  <td><?php 
					if(array_key_exists('txt9', $value) && !empty($value['txt9'])){
						echo $value['txt9'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('contacted', $value) && !empty($value['contacted'])){
						echo $value['contacted'];
					}
                ?></td>
				
                <td>
                    <a href="Details.php?id=<?php echo $key;  ?>">Details</a>
                    |<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
                    |<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
                </td>
				
            </tr>
			
            <?php
				 }
			   }else{
           ?>
				<tr>
					<td colspan="4">
						No data available
					</td>  
				</tr>
            <?php
           		}
            ?>
        </table>
		</fieldset>

		<fieldset> <legend> Cause of death(see instruction and example).PART 1. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> a.immediate cause(Final disease or condition resulting death) </th>
					<th> Due to(or as a consequence of) </th>
					<th> Onset of death </th>
					<th> b.Sequentially list conditons(if any, leading to the cause list on line a.) </th>
					<th> Due to(or as a consequence of) </th>
					<th> Onset of death </th>
					<th> c.Enter the underlying cause(diseases or injury that initiated the event of resulting of death ) </th>
					<th> Due to(or as a consequence of) </th>
					<th> Onset of death </th>
					<th> d.Last </th>
					<th> Onset of death </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('imd_cas', $value) && !empty($value['imd_cas'])){
							echo $value['imd_cas'];
						}
					  ?></td>
					  
					<td><?php
						if(array_key_exists('due_to', $value) && !empty($value['due_to'])){
							echo $value['due_to'];
						}
					?></td>
					
					<td><?php 
						if(array_key_exists('ondeath', $value) && !empty($value['ondeath'])){
							echo $value['ondeath'];
						}
					  ?></td>
					  
					<td><?php
						if(array_key_exists('seq_list', $value) && !empty($value['seq_list'])){
							echo $value['seq_list'];
						}
					?></td>
					
					 <td><?php
						if(array_key_exists('due_to_b', $value) && !empty($value['due_to_b'])){
							echo $value['due_to_b'];
						}
					?></td>
					
					<td><?php 
						if(array_key_exists('ondeath_b', $value) && !empty($value['ondeath_b'])){
							echo $value['ondeath_b'];
						}
					 ?></td>
					  
					  <td><?php 
						if(array_key_exists('under_cas', $value) && !empty($value['under_cas'])){
							echo $value['under_cas'];
						}
					  ?></td>
					  
					<td><?php
						if(array_key_exists('due_to_c', $value) && !empty($value['due_to_c'])){
							echo $value['due_to_c'];
						}
					?></td>
					
					<td><?php 
						if(array_key_exists('ondeath_c', $value) && !empty($value['ondeath_c'])){
							echo $value['ondeath_c'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('last', $value) && !empty($value['last'])){
							echo $value['last'];
						}
					  ?></td>
					  
					<td><?php
						if(array_key_exists('ondeath_d', $value) && !empty($value['ondeath_d'])){
							echo $value['ondeath_d'];
						}
					?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>	
		
		<fieldset> <legend> PART 2.Enter other significant conditions contributing to death but not resulting in the underlying cause given in PART 1. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> Message </th>
					<th> Was an autospy performed? </th>
					<th> Were autospy finding available to complete the cause of death? </th>
					<th> Did tobacco use contribute the death? </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('message', $value) && !empty($value['message'])){
							echo $value['message'];
						}
					  ?></td>
					  
					<td><?php
						if(array_key_exists('autospy', $value) && !empty($value['autospy'])){
							echo $value['autospy'];
						}
					?></td>
					
					<td><?php 
						if(array_key_exists('autospy2', $value) && !empty($value['autospy2'])){
							echo $value['autospy2'];
						}
					  ?></td>
					  
					<td><?php
						if(array_key_exists('tobacco', $value) && !empty($value['tobacco'])){
							echo $value['tobacco'];
						}
					?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>	
		
		<fieldset> <legend> If Female. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> If Female. </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('if_female', $value) && !empty($value['if_female'])){
							echo $value['if_female'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>	
		
		<fieldset> <legend> Manner of death. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> Manner of death </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('manner_of_death', $value) && !empty($value['manner_of_death'])){
							echo $value['manner_of_death'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>	
		
		<fieldset> <legend> Injury. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> Date of injury </th>
					<th> Time of injury </th>
					<th> Place of injury </th>
					<th> Injury al work </th>
					
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('doi', $value) && !empty($value['doi'])){
							echo $value['doi'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('toi', $value) && !empty($value['toi'])){
							echo $value['toi'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('poi', $value) && !empty($value['poi'])){
							echo $value['poi'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('injury', $value) && !empty($value['injury'])){
							echo $value['injury'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>

		<fieldset> <legend> Location of injury. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> State </th>
					<th> City or Town </th>
					<th> Street Number </th>
					<th> Apartment Number </th>
					<th> Zip Code </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('state', $value) && !empty($value['state'])){
							echo $value['state'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('cot', $value) && !empty($value['cot'])){
							echo $value['cot'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('number', $value) && !empty($value['number'])){
							echo $value['number'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('number2', $value) && !empty($value['number2'])){
							echo $value['number2'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('number3', $value) && !empty($value['number3'])){
							echo $value['number3'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>		
		
		<fieldset> <legend> Describe How Injury Occurred. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> How Injury Occurred </th>
					<th> Specify </th>
					<th> Specify </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('description', $value) && !empty($value['description'])){
							echo $value['description'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('specify', $value) && !empty($value['specify'])){
							echo $value['specify'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('specify_txt', $value) && !empty($value['specify_txt'])){
							echo $value['specify_txt'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>
		
		<fieldset> <legend> Certifier. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> Certifier </th>
					<th> Signature of certifier </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('certifier', $value) && !empty($value['certifier'])){
							echo $value['certifier'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('soc', $value) && !empty($value['soc'])){
							echo $value['soc'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>
		
		<fieldset> <legend> Person Completing cause of death. / 5th part </legend>

			<table border="1">
				<tr>
					<th> SN </th>
					<th> Name </th>
					<th> Address </th>
					<th> Zip Code </th>
					<th> Title of Certifier </th>
					<th> Licence Number </th>
					<th> Date of Certified </th>
					<th> Action </th>
				</tr>
				
				<?php
				   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
				   foreach($_SESSION['details'] as $key=>$value){
			   ?>    
			   
				<tr>
					<td><?php echo $key+1;?></td>
					
					<td><?php 
						if(array_key_exists('p_name', $value) && !empty($value['p_name'])){
							echo $value['p_name'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('address', $value) && !empty($value['address'])){
							echo $value['address'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('num', $value) && !empty($value['num'])){
							echo $value['num'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('toc', $value) && !empty($value['toc'])){
							echo $value['toc'];
						}
					  ?></td><td><?php 
						if(array_key_exists('num2', $value) && !empty($value['num2'])){
							echo $value['num2'];
						}
					  ?></td>
					  
					  <td><?php 
						if(array_key_exists('doc', $value) && !empty($value['doc'])){
							echo $value['doc'];
						}
					  ?></td>
					
					<td>
						<a href="Details.php?id=<?php echo $key;  ?>">Details</a>
						|<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
						|<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
					</td>
					
				</tr>
				
				<?php
					 }
				   }else{
			   ?>
					<tr>
						<td colspan="4">
							No data available
						</td>  
					</tr>
				<?php
					}
				?>
			</table>
		</fieldset>
		
		
		<?php
			//session_destroy();
		?>
    </body>
</html>
