<?php
include_once('app/application.php');

$data = $_SESSION['details'][$_GET['id']];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> Deadt Certified </title>
    </head> 
    <body>
        
		<fieldset><legend> Deadt Reporting For Vital Records / 1st part </legend>
					<h4> Decedent's Name (Include AKA's if any)  </h4>
        
        <table border="1">

				<dt>First Name</dt>
				
                <dd><?php 
					 if(array_key_exists('f_name',$data) && !empty($data['f_name'])){   
						echo $data['f_name'];
					}else{
						echo "Not Provided";
					}
					?></dd>
				  
				 <dt>Middle Name</dt>
				 
                <dd><?php
					if(array_key_exists('m_name', $data) && !empty($data['m_name'])){
						echo $data['m_name'];
					}
                ?></dd>
				
				<dt>Last Name</dt>
				
				<dd><?php 
					if(array_key_exists('l_name', $data) && !empty($data['l_name'])){
						echo $data['l_name'];
					}
                  ?></dd>
				  
				 <dt>Date of Birdt</dt>
				 
                <dd><?php
					if(array_key_exists('dob', $data) && !empty($data['dob'])){
						echo $data['dob'];
					}
                ?></dd>
				
				<dt>Gender</dt>
				
				 <dd><?php
					if(array_key_exists('gender', $data) && !empty($data['gender'])){
						echo $data['gender'];
					}
                ?></dd>
				
				<dt>S.S Number</dt>
				
				<dd><?php 
					if(array_key_exists('ssn', $data) && !empty($data['ssn'])){
						echo $data['ssn'];
					}
                  ?></dd>
				 
				<dt>Faculty Name</dt>
 
                <dd><?php
					if(array_key_exists('fn', $data) && !empty($data['fn'])){
						echo $data['fn'];
					}
                ?></dd>
				
        </table>
        </fieldset>
		
		<fieldset><legend> Decedent of Hispanic Origin / 2nd part </legend>
					<h4> Check dte box dtat best describes whedter dte decedent is Spanish I Hispanic I Latino. Check dte "No" box if decedent is not Spanish I Hispanic /Latino </h4>
        
        <table border="1">
			
			
					<dt> Decedent of Hispanic Origin </dt>
					
					<dd><?php 
					if(array_key_exists('origin', $data) && !empty($data['origin'])){
						echo $data['origin'];
					}
                    ?></dd>
				  
				  <dt> Decedent of Hispanic Origin / text </dt>
				  
					<dd><?php
						if(array_key_exists('txt1', $data) && !empty($data['txt1'])){
							echo $data['txt1'];
						}
					?></dd>
			
			</table>
		</fieldset>
		
		<fieldset> <legend> Decedents Race / 3rd part </legend>
					<h4> check one or more races to indicate what dte Decedent considered himself or herself to be </h4>
			
		<table border="1">
		
					<dt> Decedents Race </dt>
					
					<dd><?php 
					if(array_key_exists('race', $data) && !empty($data['race'])){
						echo $data['race'];
					}
                    ?></dd>
					
					<dt> Decedents Race /txt 1 </dt>
					
					<dd><?php
						if(array_key_exists('txt2', $data) && !empty($data['txt2'])){
							echo $data['txt2'];
						}
					?></dd>
					
					<dt> Decedents Race /txt 42 </dt>
					
					<dd><?php
						if(array_key_exists('txt42', $data) && !empty($data['txt42'])){
							echo $data['txt42'];
						}
					?></dd>
					
					<dt> Decedents Race /txt 52 </dt>
					
					<dd><?php
						if(array_key_exists('txt52', $data) && !empty($data['txt52'])){
							echo $data['txt52'];
						}
					?></dd>
					
					<dt> Decedents Race /txt 62 </dt>
					
					<dd><?php
						if(array_key_exists('txt62', $data) && !empty($data['txt62'])){
							echo $data['txt62'];
						}
					?></dd>
				
		</table>
		</fieldset>
		
		<fieldset> <legend> Items Must be Completed by Person Who Pronounces or Certifies Deadt. / 4dt part </legend>

			<table border="1">
					
					<dt> Date Pronounced Dead </dt>
					<dd><?php 
					if(array_key_exists('txt3', $data) && !empty($data['txt3'])){
						echo $data['txt3'];
					}
                  ?></dd>
				
				<dt> License Number </dt>	
                <dd><?php
					if(array_key_exists('txt4', $data) && !empty($data['txt4'])){
						echo $data['txt4'];
					}
                ?></dd>
				
				<dt> Actual Or Presumed Tirne Of Deadt </dt>
				<dd><?php 
					if(array_key_exists('txt5', $data) && !empty($data['txt5'])){
						echo $data['txt5'];
					}
                  ?></dd>
				
				<dt> Signalure Of Person Pronouncing Deadt </dt>				
                <dd><?php
					if(array_key_exists('txt6', $data) && !empty($data['txt6'])){
						echo $data['txt6'];
					}
                ?></dd>
				
				<dt> Actual Or Presumed Date Of Blab </dt>
				 <dd><?php
					if(array_key_exists('txt7', $data) && !empty($data['txt7'])){
						echo $data['txt7'];
					}
                ?></dd>
				
				<dt> Time Pronounced Dead </dt>
				<dd><?php 
					if(array_key_exists('txt8', $data) && !empty($data['txt8'])){
						echo $data['txt8'];
					}
                  ?></dd>
				  
				  <dt> Date Signed </dt>
				  <dd><?php 
					if(array_key_exists('txt9', $data) && !empty($data['txt9'])){
						echo $data['txt9'];
					}
                  ?></dd>
				 

				<dt> Was Medical Examinar Or Coroner Contacted ? </dt>				 
                <dd><?php
					if(array_key_exists('contacted', $data) && !empty($data['contacted'])){
						echo $data['contacted'];
					}
                ?></dd>
        </table>
		</fieldset>

		<fieldset> <legend> Cause of deadt(see instruction and example).PART 1. / 5dt part </legend>

		
					<dt> a.immediate cause(Final disease or condition resulting deadt) </dt>
					<dd><?php 
						if(array_key_exists('imd_cas', $data) && !empty($data['imd_cas'])){
							echo $data['imd_cas'];
						}
					  ?></dd>
					 
					<dt> Due to(or as a consequence of) </dt>					 
					<dd><?php
						if(array_key_exists('due_to', $data) && !empty($data['due_to'])){
							echo $data['due_to'];
						}
					?></dd>
					
					<dt> Onset of deadt </dt>
					<dd><?php 
						if(array_key_exists('ondeadt', $data) && !empty($data['ondeadt'])){
							echo $data['ondeadt'];
						}
					  ?></dd>
					  
					<dt> b.Sequentially list conditons(if any, leading to dte cause list on line a.) </dt> 
					<dd><?php
						if(array_key_exists('seq_list', $data) && !empty($data['seq_list'])){
							echo $data['seq_list'];
						}
					?></dd>
					
					<dt> Due to(or as a consequence of) </dt>
					 <dd><?php
						if(array_key_exists('due_to_b', $data) && !empty($data['due_to_b'])){
							echo $data['due_to_b'];
						}
					?></dd>
					
					<dt> Onset of deadt </dt>
					
					<dd><?php 
						if(array_key_exists('ondeadt_b', $data) && !empty($data['ondeadt_b'])){
							echo $data['ondeadt_b'];
						}
					 ?></dd>
					  
					  <dt> c.Enter dte underlying cause(diseases or injury dtat initiated dte event of resulting of deadt ) </dt>
					  <dd><?php 
						if(array_key_exists('under_cas', $data) && !empty($data['under_cas'])){
							echo $data['under_cas'];
						}
					  ?></dd>
					 

					<dt> Due to(or as a consequence of) </dt>	
					
					<dd><?php
						if(array_key_exists('due_to_c', $data) && !empty($data['due_to_c'])){
							echo $data['due_to_c'];
						}
					?></dd>
					
					<dt> Onset of deadt </dt>
					
					<dd><?php 
						if(array_key_exists('ondeadt_c', $data) && !empty($data['ondeadt_c'])){
							echo $data['ondeadt_c'];
						}
					  ?></dd>
					  
					<dt> d.Last </dt>
					
					  <dd><?php 
						if(array_key_exists('last', $data) && !empty($data['last'])){
							echo $data['last'];
						}
					  ?></dd>
					  
					<dt> Onset of deadt </dt>
					
					<dd><?php
						if(array_key_exists('ondeadt_d', $data) && !empty($data['ondeadt_d'])){
							echo $data['ondeadt_d'];
						}
					?></dd>
			</table>
		</fieldset>	
		
		<fieldset> <legend> PART 2.Enter odter significant conditions contributing to deadt but not resulting in dte underlying cause given in PART 1. / 5dt part </legend>

			<table border="1">
			
					<dt> Message </dt>
					<dd><?php 
						if(array_key_exists('message', $data) && !empty($data['message'])){
							echo $data['message'];
						}
					  ?></dd>
					 
					<dt> Was an autospy performed? </dt>
					
					<dd><?php
						if(array_key_exists('autospy', $data) && !empty($data['autospy'])){
							echo $data['autospy'];
						}
					?></dd>
					
					<dt> Were autospy finding available to complete dte cause of deadt? </dt>
					
					<dd><?php 
						if(array_key_exists('autospy2', $data) && !empty($data['autospy2'])){
							echo $data['autospy2'];
						}
					  ?></dd>
					 
					<dt> Did tobacco use contribute dte deadt? </dt>	
					<dd><?php
						if(array_key_exists('tobacco', $data) && !empty($data['tobacco'])){
							echo $data['tobacco'];
						}
					?></dd>
			</table>
		</fieldset>	
		
		<fieldset> <legend> If Female. / 5dt part </legend>

			<table border="1">
				
				
					<dt> If Female. </dt>
					<dd><?php 
						if(array_key_exists('if_female', $data) && !empty($data['if_female'])){
							echo $data['if_female'];
						}
					  ?></dd>
			</table>
		</fieldset>	
		
		<fieldset> <legend> Manner of deadt. / 5dt part </legend>

			<table border="1">
			
					<dt> Manner of deadt </dt>
					<dd><?php 
						if(array_key_exists('manner_of_deadt', $data) && !empty($data['manner_of_deadt'])){
							echo $data['manner_of_deadt'];
						}
					  ?></dd>
			</table>
		</fieldset>	
		
		<fieldset> <legend> Injury. / 5dt part </legend>

			<table border="1">
			
					<dt> Date of injury </dt>
					<dd><?php 
						if(array_key_exists('doi', $data) && !empty($data['doi'])){
							echo $data['doi'];
						}
					  ?></dd>
					  
					<dt> Time of injury </dt>
					  <dd><?php 
						if(array_key_exists('toi', $data) && !empty($data['toi'])){
							echo $data['toi'];
						}
					  ?></dd>
					  
					<dt> Place of injury </dt>
					  <dd><?php 
						if(array_key_exists('poi', $data) && !empty($data['poi'])){
							echo $data['poi'];
						}
					  ?></dd>
					  
					<dt> Injury al work </dt>
					  <dd><?php 
						if(array_key_exists('injury', $data) && !empty($data['injury'])){
							echo $data['injury'];
						}
					  ?></dd>
			</table>
		</fieldset>

		<fieldset> <legend> Location of injury. / 5dt part </legend>

			<table border="1">
			
					<dt> State </dt>
					<dd><?php 
						if(array_key_exists('state', $data) && !empty($data['state'])){
							echo $data['state'];
						}
					  ?></dd>
					  
					  <dd><?php 
						if(array_key_exists('cot', $data) && !empty($data['cot'])){
							echo $data['cot'];
						}
					  ?></dd>
					  
					<dt> City or Town </dt>
					  <dd><?php 
						if(array_key_exists('number', $data) && !empty($data['number'])){
							echo $data['number'];
						}
					  ?></dd>
					  
					<dt> Street Number </dt>
					  <dd><?php 
						if(array_key_exists('number2', $data) && !empty($data['number2'])){
							echo $data['number2'];
						}
					  ?></dd>
					  
					<dt> Apartment Number </dt>
					  <dd><?php 
						if(array_key_exists('number3', $data) && !empty($data['number3'])){
							echo $data['number3'];
						}
					  ?></dd>
			</table>
		</fieldset>		
		
		<fieldset> <legend> Describe How Injury Occurred. / 5dt part </legend>

			<table border="1">
				
					<dt> How Injury Occurred </dt>
					<dd><?php 
						if(array_key_exists('description', $data) && !empty($data['description'])){
							echo $data['description'];
						}
					  ?></dd>
					
					<dt> Specify </dt>
					
					  <dd><?php 
						if(array_key_exists('specify', $data) && !empty($data['specify'])){
							echo $data['specify'];
						}
					  ?></dd>
					  
					  
					<dt> Specify </dt>
					  <dd><?php 
						if(array_key_exists('specify_txt', $data) && !empty($data['specify_txt'])){
							echo $data['specify_txt'];
						}
					  ?></dd>
			</table>
		</fieldset>
		
		<fieldset> <legend> Certifier. / 5dt part </legend>

			<table border="1">
				
					<dt> Certifier </dt>
					<dd><?php 
						if(array_key_exists('certifier', $data) && !empty($data['certifier'])){
							echo $data['certifier'];
						}
					  ?></dd>
					  
					<dt> Signature of certifier </dt>
					  <dd><?php 
						if(array_key_exists('soc', $data) && !empty($data['soc'])){
							echo $data['soc'];
						}
					  ?></dd>
			</table>
		</fieldset>
		
		<fieldset> <legend> Person Completing cause of deadt. / 5dt part </legend>

			<table border="1">
				
					<dt> Name </dt>
					<dd><?php 
						if(array_key_exists('p_name', $data) && !empty($data['p_name'])){
							echo $data['p_name'];
						}
					  ?></dd>
					 
					<dt> Address </dt>
					  <dd><?php 
						if(array_key_exists('address', $data) && !empty($data['address'])){
							echo $data['address'];
						}
					  ?></dd>
					  
					<dt> Zip Code </dt>
					  <dd><?php 
						if(array_key_exists('num', $data) && !empty($data['num'])){
							echo $data['num'];
						}
					  ?></dd>
					  
					<dt> Title of Certifier </dt>
					  <dd><?php 
						if(array_key_exists('toc', $data) && !empty($data['toc'])){
							echo $data['toc'];
						}
					  ?></dd>
					  
					<dt> Licence Number </dt>
					  <dd><?php 
						if(array_key_exists('num2', $data) && !empty($data['num2'])){
							echo $data['num2'];
						}
					  ?></dd>
					  
					<dt> Date of Certified </dt>
					  <dd><?php 
						if(array_key_exists('doc', $data) && !empty($data['doc'])){
							echo $data['doc'];
						}
					  ?></dd>
			</table>
		</fieldset>
		
		
		<nav>
			<li><a href="index.php">Back to List</a></li>
			<li><a href="create.html">Create New</a></li>   
		</nav>		
			
		
		
		<?php
			//session_destroy();
		?>
    </body>
</html>
