<?php
include_once('app/application.php');

$data = $_SESSION['details'][$_GET['id']];
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title> Death Certified Edit</title>
</head>
	
	<form action="save.php" method="post">
		<input type="hidden" name="id" value="<?php echo $_GET['id']?>" />
	
			<fieldset>	<legend> Death Reporting For Vital Records / 1st part </legend>
								<h4> Decedent's Name (Include AKA's if any)  </h4>
					
					<table>
							<tr>
								<td>
									<label for="first_name"> First Name </label>
									<input type="text" name="f_name" id="first_name" autofocus="autofocus" value="<?php if(array_key_exists('f_name',$data) && !empty($data['f_name'])){   
            echo $data['f_name'];
        }else{
            echo "";
        } ?>" />
								</td>
								
								<td>
									<label for="mid_name"> Middle Name </label>
									<input type="text" name="m_name" id="mid_name" value="<?php if(array_key_exists('m_name',$data) && !empty($data['m_name'])){   
            echo $data['m_name'];
        }else{
            echo "";
        } ?>" />
								</td>
								
								<td>
									<label for="last_name"> Last Name </label>
									<input type="text" name="l_name" id="last_name" value="<?php if(array_key_exists('l_name',$data) && !empty($data['l_name'])){   
            echo $data['l_name'];
        }else{
            echo "";
        } ?>" />
								</td>
							</tr>
							<tr>	
								<td>
									<label for="do_birth"> Date of Birth </label>
									<input type="date" name="dob" id="do_birth" value="<?php if(array_key_exists('dob',$data) && !empty($data['dob'])){   
            echo $data['dob'];
        }else{
            echo "";
        } ?>" />
								</td>
								
								<td>
									Gender :<select name="gender">
												<option value=""> Select Gender </option>
												<option value="male" <?php if($data['gender'] == "male") echo 'selected="selected"'; ?> > Male </option>
												<option value="female" <?php if($data['gender'] == "female") echo 'selected="selected"'; ?> > Female </option>
											</select>		
								</td>
							</tr>
							
							<tr>		
								<td>
									<label for="Social_sn">  S.S Number  </label>
									<input type="text" name="ssn" id="Social_sn" value="<?php if(array_key_exists('ssn',$data) && !empty($data['ssn'])){   
            echo $data['ssn'];
        }else{
            echo "";
        } ?>" />
								</td>
								
								<td>
									<label for="factrty_name">  Factrty Name  </label>
									<input type="text" name="fn" id="factrty_name" value="<?php if(array_key_exists('fn',$data) && !empty($data['fn'])){   
            echo $data['fn'];
        }else{
            echo "";
        } ?>" />
								</td>
							</tr>
						</table>
						
				</fieldset>
				
				<fieldset>	<legend> Decedent of Hispanic Origin / 2nd part </legend>
								<h4> Check the box that best describes whether the decedent is Spanish I Hispanic I Latino. Check the "No" box if decedent is not Spanish I Hispanic /Latino: </h4>
						
						<table>
							<tr>
								<td> <input type="radio" name="origin" value="NO not Spanish/Hispanic/Latino" <?php echo ($data['origin']=='NO not Spanish/Hispanic/Latino')?'checked':'' ?> /> NO not Spanish/Hispanic/Latino </td> 
								<td> <input type="radio" name="origin" value="Yes,Mexican, Mexican Amencan, Chicano" <?php echo ($data['origin']=='Yes,Mexican, Mexican Amencan, Chicano')?'checked':'' ?> /> Yes,Mexican, Mexican Amencan, Chicano </td>
								<td> <input type="radio" name="origin" value="Yes, Puerto Rican" <?php echo ($data['origin']=='Yes, Puerto Rican')?'checked':'' ?> /> Yes, Puerto Rican </td>
							</tr>
							<tr>	
								<td> <input type="radio" name="origin" value="Yes, Cuban" <?php echo ($data['origin']=='Yes, Cuban')?'checked':'' ?> /> Yes, Cuban </td>
								<td> <input type="radio" name="origin" value="Yes, other Spanish/Hispanic/Latino (Specrfy)," <?php echo ($data['origin']=='Yes, other Spanish/Hispanic/Latino (Specrfy),')?'checked':'' ?> /> Yes, other Spanish/Hispanic/Latino (Specrfy), </td>
								<td> <input type="text" name="txt1" id="txt1" value="<?php if(array_key_exists('txt1',$data) && !empty($data['txt1'])){   
            echo $data['txt1'];
        }else{
            echo "";
        } ?>" /> </td>
							</tr>
						</table>
						
				</fieldset>
					
				<fieldset> <legend> Decedents Race / 3rd part </legend>
							   <h4> check one or more races to indicate what the Decedent considered himself or herself to be:  </h4>
						<table>	
							<tr>
								<td> <input type="radio" name="race" value="White" <?php echo ($data['race']=='White')?'checked':'' ?> /> White </td>
								<td> <input type="radio" name="race" value="Black or uncap Amencan" <?php echo ($data['race']=='Black or uncap Amencan')?'checked':'' ?> />  Black or uncap Amencan </td>
								<td> <input type="radio" name="race" value="American Indian or as Native (Name of the enrolled or principal tribe" <?php echo ($data['race']=='American Indian or as Native (Name of the enrolled or principal tribe')?'checked':'' ?> />  American Indian or as Native (Name of the enrolled or principal tribe </td>
							</tr>
							<tr>	
								<td> <input type="text" name="txt2" id="txt2" value="<?php if(array_key_exists('txt2',$data) && !empty($data['txt2'])){   
            echo $data['txt2'];
        }else{
            echo "";
        } ?>" /> </td>		
								<td> <input type="radio" name="race" value="Asian Indian" <?php echo ($data['race']=='Asian Indian')?'checked':'' ?> /> Asian Indian </td>
								<td> <input type="radio" name="race" value="Chinese" <?php echo ($data['race']=='White')?'checked':'' ?> /> Chinese </td>
							</tr>
							<tr>	
								<td> <input type="radio" name="race" value=" Fitdpino" <?php echo ($data['race']=='Fitdpino')?'checked':'' ?> /> Fitdpino </td>
								<td> <input type="radio" name="race" value="Korean" <?php echo ($data['race']=='Korean')?'checked':'' ?> /> Korean </td>
								<td> <input type="radio" name="race" value="Vietnamese" <?php echo ($data['race']=='Vietnamese')?'checked':'' ?> /> Vietnamese </td>
							</tr>
							<tr>	
								<td> <input type="radio" name="race" value="Other Asian (Specrfy" <?php echo ($data['race']=='Other Asian (Specrfy')?'checked':'' ?> /> Other Asian (Specrfy) </td>
								<td> <input type="text" name="txt42" id="txt42" value="<?php if(array_key_exists('txt42',$data) && !empty($data['txt42'])){   
            echo $data['txt42'];
        }else{
            echo "";
        } ?>" /> </td>
								<td> <input type="radio" name="race" value="Nature Hawaiian" <?php echo ($data['race']=='Nature Hawaiian')?'checked':'' ?> /> Nature Hawaiian </td>
							</tr>
							<tr>	
								<td> <input type="radio" name="race" value="Guamanian or Chamorro" <?php echo ($data['race']=='Guamanian or Chamorro')?'checked':'' ?> /> Guamanian or Chamorro </td>
								<td> <input type="radio" name="race" value="Samoan" <?php echo ($data['race']=='Samoan')?'checked':'' ?> /> Samoan </td>
								<td> <input type="radio" name="race" value="Other Page Islander (Speedy)" <?php echo ($data['race']=='Other Page Islander (Speedy)')?'checked':'' ?> /> Other Page Islander (Speedy) </td>
							</tr>
							<tr>	
								<td> <input type="text" name="txt52" id="txt52" value="<?php if(array_key_exists('txt52',$data) && !empty($data['txt52'])){   
            echo $data['txt52'];
        }else{
            echo "";
        } ?>" /> </td>	
								<td> <input type="radio" name="race" value="Other (Specrfy)" <?php echo ($data['race']=='Other (Specrfy)')?'checked':'' ?> /> Other (Specrfy)  </td>
								<td> <input type="text" name="txt62" id="txt62" value="<?php if(array_key_exists('txt62',$data) && !empty($data['txt62'])){   
            echo $data['txt62'];
        }else{
            echo "";
        } ?>" /> </td>		
							</tr>
						</table>
				</fieldset>
					
				<fieldset> <legend> Items Must be Completed by Person Who Pronounces or Certifies Death. / 4th part </legend>
						
						<table>
							<tr>
								<td>
								   <label> Date Pronounced Dead </label>
								   <input type="date" name="txt3" id="txt3" value="<?php if(array_key_exists('txt3',$data) && !empty($data['txt3'])){   
            echo $data['txt3'];
        }else{
            echo "";
        } ?>" /> </td>		
								
								<td>
								   <label> Actual Or Presumed Time Of Death </label>
								   <input type="time" name="txt4" id="txt4" value="<?php if(array_key_exists('txt4',$data) && !empty($data['txt4'])){   
            echo $data['txt4'];
        }else{
            echo "";
        } ?>" /> </td>	
								
								<td>
								   <label> tdcense Number </label>
								   <input type="text" name="txt5" id="txt5" value="<?php if(array_key_exists('txt5',$data) && !empty($data['txt5'])){
			echo $data['txt5'];
		} else{
			echo "";
		} ?>" /> </td>	
								
							</tr>
							
							<tr>	
								<td>
								   <label> Signalure Of Person Pronouncing Death  </label>
								   <input type="text" name="txt6" id="txt6" value="<?php if(array_key_exists('txt6',$data) && !empty($data['txt6'])){
			echo $data['txt6'];
		} else{
			echo "";
		} ?>" /> </td>	
								
								<td>
								   <label> Actual Or Presumed Date Of Birth </label>
								   <input type="date" name="txt7" id="txt7" value="<?php if(array_key_exists('txt7',$data) && !empty($data['txt7'])){
			echo $data['txt7'];
		} else{
			echo "";
		} ?>" /> </td>	
								
								<td>
								   <label> Time Pronounced Dead  </label>
								   <input type="time" name="txt8" id="txt8" value="<?php if(array_key_exists('txt8',$data) && !empty($data['txt8'])){
			echo $data['txt8'];
		} else{
			echo "";
		} ?>" /> </td>
							</tr>
							<tr>	
								<td>
								   <label> Date Signed </label>
								   <input type="text" name="txt9" id="txt9" value="<?php if(array_key_exists('txt9',$data) && !empty($data['txt9'])){
			echo $data['txt9'];
		} else{
			echo "";
		} ?>" /> </td>
								
								<td>
									<label> Was Medical Examinar Or Coroner Contacted ? </label>
									<input type="radio" name="contacted" id="contacted" value="Yes" <?php echo ($data['contacted']=='Yes')?'checked':'' ?> /> Yes
									<input type="radio" name="contacted" id="contacted" value="No" <?php echo ($data['contacted']=='No')?'checked':'' ?> /> No
									
								</td>
							</tr>
						</table>		
				</fieldset>
				
				<fieldset> <legend> Items Must be Completed by Person Who Pronounces or Certifies Death. / 5th part </legend>
							<h4> PART I. Enter the chain of events - diseases, ippmes, or complications-that   directly caused Inc death. DO NOT enter terminal events such as Approximateinterval cardiac arrest, respiratory arrest, or ventrcular fibrillaton wfthout showing the etiobgy DO NOT ABBREVIATE. Enter only one cause on a line Add addRional lines rf necessary </h4>
					<table>
					
							<tr>
								<td>a.immediate cause(Final disease or condition resulting death):<input type="text" name="imd_cas" id="imd_cas" value="<?php if(array_key_exists('imd_cas',$data) && !empty($data['imd_cas'])){
			echo $data['imd_cas'];
		} else{
			echo "";
		} ?>" /></td>
								<td>Due to(or as a consequence of):<input type="text" name="due_to" id="due_to" value="<?php if(array_key_exists('due_to',$data) && !empty($data['due_to'])){
			echo $data['due_to'];
		} else{
			echo "";
		} ?>" /></td>
								<td>Onset of death:<input type="text" name="ondeath" id="ondeath" value="<?php if(array_key_exists('ondeath',$data) && !empty($data['ondeath'])){
			echo $data['ondeath'];
		} else{
			echo "";
		} ?>" /></td>
							</tr>
							
							<tr>
								<td>b.Sequentially list conditons(if any, leading to the cause list on line a.):<input type="text" name="seq_list" id="seq_list" value="<?php if(array_key_exists('seq_list',$data) && !empty($data['seq_list'])){
			echo $data['seq_list'];
		} else{
			echo "";
		} ?>" /></td>
								<td>Due to(or as a consequence of):<input type="text" name="due_to_b" id="due_to_b" value="<?php if(array_key_exists('due_to_b',$data) && !empty($data['due_to_b'])){
			echo $data['due_to_b'];
		} else{
			echo "";
		} ?>" /></td>
								<td>Onset of death:<input type="text" name="ondeath_b" id="ondeath_b" value="<?php if(array_key_exists('ondeath_b',$data) && !empty($data['ondeath_b'])){
			echo $data['ondeath_b'];
		} else{
			echo "";
		} ?>" /></td>
							</tr>
							
							<tr>
								<td>c.Enter the underlying cause(diseases or injury that initiated the event of resulting of death ):<input type="text" name="under_cas" id="under_cas" value="<?php if(array_key_exists('under_cas',$data) && !empty($data['under_cas'])){
			echo $data['under_cas'];
		} else{
			echo "";
		} ?>" /></td>
								<td>Due to(or as a consequence of):<input type="text" name="due_to_c" id="due_to_c" value="<?php if(array_key_exists('due_to_c',$data) && !empty($data['due_to_c'])){
			echo $data['due_to_c'];
		} else{
			echo "";
		} ?>" /></td>
								<td>Onset of death:<input type="text" name="ondeath_c" id="ondeath_c" value="<?php if(array_key_exists('ondeath_c',$data) && !empty($data['ondeath_c'])){
			echo $data['ondeath_c'];
		} else{
			echo "";
		} ?>" /></td>
							</tr>
							
							<tr>
								<td>d.Last:<input type="text" name="last" id="last" value="<?php if(array_key_exists('last',$data) && !empty($data['last'])){
			echo $data['last'];
		} else{
			echo "";
		} ?>" /></td></td>
								<td>Onset of death:<input type="text" name="ondeath_d" id="ondeath_d" value="<?php if(array_key_exists('ondeath_d',$data) && !empty($data['ondeath_d'])){
			echo $data['ondeath_d'];
		} else{
			echo "";
		} ?>" /></td>
							</tr>
							
								
								<!-- part 6th -->
							<tr>
								<td align="center"><h4>PART 2.Enter other significant conditions contributing to death but not resulting in the underlying cause given in PART 1.</h4></td>
							</tr>
							
							<tr>
								<td>
									<hr />
								</td>
							</tr>
							
							<tr>
								<td>
									<textarea name="message" rows="3" cols="30" >
									</textarea>
								</td>
							</tr>
							
							<tr>
								<td>Was an autospy performed?</td>
							</tr>
							<tr>
									<td><input type="radio" name="autospy" value="Yes" <?php echo ($data['autospy']=='Yes')?'checked':'' ?> >Yes
									<input type="radio" name="autospy" value="No" <?php echo ($data['autospy']=='No')?'checked':'' ?>>No</td>
							</tr>
							
							<tr>
								<td>Were autospy finding available to complete the cause of death?</td>
							</tr>
							<tr>
									<td><input type="radio" name="autospy2" value="Yes" <?php echo ($data['autospy2']=='Yes')?'checked':'' ?> >Yes
									<input type="radio" name="autospy2" value="No" <?php echo ($data['autospy2']=='No')?'checked':'' ?> >No</td>
							</tr>
							
							<tr>	
								<td>Did tobacco use contribute the death?</td>
							</tr>
							<tr>
								<td><input type="radio" name="tobacco" value="Yes" <?php echo ($data['tobacco']=='Yes')?'checked':'' ?> >Yes
									<input type="radio" name="tobacco" value="No" <?php echo ($data['tobacco']=='No')?'checked':'' ?> >No
									<input type="radio" name="tobacco" value="Probably" <?php echo ($data['tobacco']=='Probably')?'checked':'' ?> >Probably
									<input type="radio" name="tobacco" value="unknown" <?php echo ($data['tobacco']=='unknown')?'checked':'' ?> >unknown
								</td>
									
							</tr>
								
							<!-- part 7th -->
						
						<tr>
							<td align="center"><b>If Female</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>
						
						<tr>
							<td>
								<input type="radio" name="if_female" value="Not pregnant within past year" <?php echo ($data['if_female']=='Not pregnant within past year')?'checked':'' ?> >Not pregnant within past year
								<input type="radio" name="if_female" value="pregnant at time of death" <?php echo ($data['if_female']=='pregnant at time of death')?'checked':'' ?> >pregnant at time of death
								<input type="radio" name="if_female" value="Not pregnant but pregnant within 42 days of death" <?php echo ($data['if_female']=='Not pregnant but pregnant within 42 days of death')?'checked':'' ?> >Not pregnant but pregnant within 42 days of death
							</td>
						</tr>
						
						<tr>
							<td>
								<input type="radio" name="if_female" value="Not pregnant but pregnant within 43 days to 1 year before death" <?php echo ($data['if_female']=='Not pregnant but pregnant within 43 days to 1 year before death')?'checked':'' ?> >Not pregnant but pregnant within 43 days to 1 year before death
								<input type="radio" name="if_female" value="Unknown if pregnant within the past year" <?php echo ($data['if_female']=='Unknown if pregnant within the past year')?'checked':'' ?> >Unknown if pregnant within the past year
							</td>
						</tr>
								
									
						<!--8th side-->
										
						<tr>
							<td align="center"><b>Manner of death</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>
					
						<tr>
							<td>
								<input type="radio" name="manner_of_death" value="Natural" <?php echo ($data['manner_of_death']=='Natural')?'checked':'' ?> >Natural 
								<input type="radio" name="manner_of_death" value="Homicide" <?php echo ($data['manner_of_death']=='Homicide')?'checked':'' ?> >Homicide
								<input type="radio" name="manner_of_death" value="Accident" <?php echo ($data['manner_of_death']=='Accident')?'checked':'' ?> >Accident
								<input type="radio" name="manner_of_death" value="Pending Investigation" <?php echo ($data['manner_of_death']=='Pending Investigation')?'checked':'' ?> >Pending Investigation
								<input type="radio" name="manner_of_death" value="Suicide" <?php echo ($data['manner_of_death']=='Suicide')?'checked':'' ?>>Suicide
								<input type="radio" name="manner_of_death" value="Could not be determined" <?php echo ($data['manner_of_death']=='Could not be determined')?'checked':'' ?> >Could not be determined
							</td>
						</tr>
						
							
							
						<!--9th side-->
					
						<tr>
							<td align="center"><b>Injury</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>
						
						<tr>
							<td>Date of injury:<input type="date" name="doi" id="doi" value="<?php if(array_key_exists('doi',$data) && !empty($data['doi'])){
			echo $data['doi'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Time of injury:<input type="time" name="toi" id="toi" value="<?php if(array_key_exists('toi',$data) && !empty($data['toi'])){
			echo $data['toi'];
		} else{
			echo "";
		} ?>" /></td>
							<td>Place of injury:<input type="text" name="poi" id="poi" value="<?php if(array_key_exists('poi',$data) && !empty($data['poi'])){
			echo $data['poi'];
		} else{
			echo "";
		} ?>" /></td>
						</tr>
						<tr>		
							<td>Injury at work:<input type="radio" name="injury" value="Yes"  <?php echo ($data['injury']=='Yes')?'checked':'' ?> >Yes
												<input type="radio" name="injury" value="No"  <?php echo ($data['injury']=='No')?'checked':'' ?>>No</td>
						</tr>
							
								
						<!--10th side-->
									
						<tr>
							<td align="center"><b>Location of injury</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>

						<tr>
							<td>State:<input type="text" name="state" id="state" value="<?php if(array_key_exists('state',$data) && !empty($data['state'])){
			echo $data['state'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>City or Town:<input type="text" name="cot" id="cot" value="<?php if(array_key_exists('cot',$data) && !empty($data['cot'])){
			echo $data['cot'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Street Number:<input type="number" name="number" id="number"value="<?php if(array_key_exists('number',$data) && !empty($data['number'])){
			echo $data['number'];
		} else{
			echo "";
		} ?>" /> </td>
						</tr>
						<tr>
							<td>Apartment Number:<input type="number" name="number2" id="number2" value="<?php if(array_key_exists('number2',$data) && !empty($data['number2'])){
			echo $data['number2'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Zip Code:<input type="number" name="number3" id="number3" value="<?php if(array_key_exists('number3',$data) && !empty($data['number3'])){
			echo $data['number3'];
		} else{
			echo "";
		} ?>" /> </td>
							
						</tr>
						
						
							
						<!--11th side-->
					
						<tr>
							<td align="center"><b>Describe How Injury Occurred</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>	
						
						<tr>
							<td><textarea name="description" cols="80" rows="5" ></textarea></td>
						</tr>
						
						<tr>
							<td>Specify:</td>
						</tr>
						
						<tr>
							<td>
								<input type="radio" name="specify" value="Driver/Operator"  <?php echo ($data['specify']=='Driver/Operator')?'checked':'' ?> >Driver/Operator
								<input type="radio" name="specify" value="Passenger" <?php echo ($data['specify']=='Passenger')?'checked':'' ?> >Passenger
								<input type="radio" name="specify" value="Pedestrian" <?php echo ($data['specify']=='Pedestrian')?'checked':'' ?> >Pedestrian
								<input type="radio" name="specify" value="Other(specify)" <?php echo ($data['specify']=='Other(specify)')?'checked':'' ?> >Other(specify)
								<input type="text" name="specify_txt" value="<?php if(array_key_exists('specify_txt',$data) && !empty($data['specify_txt'])){
			echo $data['specify_txt'];
		} else{
			echo "";
		} ?>" /> </td>
						</tr>
							
							
						<!--12th side-->
										
						<tr>
							<td align="center"><b>Certifier</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>
						
						<tr>
							<td>Check only one:</td>
						</tr>
						
						<tr>
							<td><input type="radio" name="certifier" value="certifying physician"  <?php echo ($data['certifier']=='certifying physician')?'checked':'' ?> >certifying physician</td>
						</tr>
						
						<tr>
							<td><input type="radio" name="certifier" value="Pronouncing and certifying physician" <?php echo ($data['certifier']=='Pronouncing and certifying physician')?'checked':'' ?> >Pronouncing and certifying physician</td>
						</tr>
							<td><input type="radio" name="certifier" value="Medical Examiner or coroner" <?php echo ($data['certifier']=='Medical Examiner or coroner')?'checked':'' ?> >Medical Examiner or coroner</td>
						</tr>
						
						<tr>
							<td>Signature of certifier:<input type="text" name="soc" value="<?php if(array_key_exists('soc',$data) && !empty($data['soc'])){
			echo $data['soc'];
		} else{
			echo "";
		} ?>" /> </td>
						</tr>
						
							
							
						<!--13th side-->
						
						<tr>
							<td align="center"><b>Person Completing cause of death</b></td>
						</tr>
						
						<tr>
							<td>
								<hr />
							</td>
						</tr>
						
						<tr>
							<td>Name:<input type="text" name="p_name" id="p_name" value="<?php if(array_key_exists('p_name',$data) && !empty($data['p_name'])){
			echo $data['p_name'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Address:<input type="text" name="address" id="address" value="<?php if(array_key_exists('address',$data) && !empty($data['address'])){
			echo $data['address'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Zip Code:<input type="number" name="num" id="num" value="<?php if(array_key_exists('num',$data) && !empty($data['num'])){
			echo $data['num'];
		} else{
			echo "";
		} ?>" /> </td>
						</tr>
						<tr>
							<td>Title of Certifier:<input type="text" name="toc" id="toc" value="<?php if(array_key_exists('toc',$data) && !empty($data['toc'])){
			echo $data['toc'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Licence Number:<input type="number" name="num2" id="num2" value="<?php if(array_key_exists('num2',$data) && !empty($data['num2'])){
			echo $data['num2'];
		} else{
			echo "";
		} ?>" /> </td>
							<td>Date of Certified:<input type="date" name="doc" id="doc" value="<?php if(array_key_exists('doc',$data) && !empty($data['doc'])){
			echo $data['doc'];
		} else{
			echo "";
		} ?>" /> </td>
						</tr>		
					</table>
				</fieldset>				
						
				<fieldset>
					<input type="submit" value="Submit" id="sub" name="sub" />
					<input type="reset" value="Reset" id="res" name="res" />
					
					<a href="index.php"> Cancel </a>
				</fieldset>
				
				

	</form>
	
<body>
</body>
</html>
