<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> Death Certified </title>
    </head> 
    <body>
        <p><a href="create.html">Create New</a> </p>
        
		<fieldset><legend> Death Reporting For Vital Records / 1st part </legend>
					<h4> Decedent's Name (Include AKA's if any)  </h4>
        
        <table border="1">
            <tr>
                <th>Sl</th>
                <th>First Name</th>
                <th>Middle Name</th>
				<th>Last Name</th>
				<th>Date of Birth</th>
                <th>Gender</th>
				<th>S.S Number</th>
                <th>Faculty Name</th>
                <th>Action</th>
            </tr>
			
           <?php
			   if(array_key_exists('details',$_SESSION) && !empty($_SESSION['details'])){
			   foreach($_SESSION['details'] as $key=>$value){
           ?>    
           
            <tr>
                <td><?php echo $key+1;?></td>
				
                <td><?php 
					if(array_key_exists('f_name', $value) && !empty($value['f_name'])){
						echo $value['f_name'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('m_name', $value) && !empty($value['m_name'])){
						echo $value['m_name'];
					}
                ?></td>
				
				<td><?php 
					if(array_key_exists('l_name', $value) && !empty($value['l_name'])){
						echo $value['l_name'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('dob', $value) && !empty($value['dob'])){
						echo $value['dob'];
					}
                ?></td>
				
				 <td><?php
					if(array_key_exists('gender', $value) && !empty($value['gender'])){
						echo $value['gender'];
					}
                ?></td>
				
				<td><?php 
					if(array_key_exists('ssn', $value) && !empty($value['ssn'])){
						echo $value['ssn'];
					}
                  ?></td>
				  
                <td><?php
					if(array_key_exists('fn', $value) && !empty($value['fn'])){
						echo $value['fn'];
					}
                ?></td>
				
                <td>
                    <a href="Details.php?id=<?php echo $key;  ?>">View</a>
                    |<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
                    |<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
                </td>
				
            </tr>
			
            <?php
				 }
			   }else{
           ?>
				<tr><td colspan="4">
					No data available
					</td>  
				</tr>
            <?php
           		}
            ?>
        </table>
        </fieldset>
    </body>