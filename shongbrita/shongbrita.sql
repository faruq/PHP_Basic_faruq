-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2015 at 09:35 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shongbrita`
--

-- --------------------------------------------------------

--
-- Table structure for table `certified_songbritan`
--

CREATE TABLE IF NOT EXISTS `certified_songbritan` (
`sl` int(4) NOT NULL,
  `name` varchar(32) NOT NULL,
  `abortan` varchar(4) NOT NULL,
  `abortan_eng` int(4) NOT NULL,
  `position` varchar(4) NOT NULL,
  `mobile` varchar(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `certified_songbritan`
--

INSERT INTO `certified_songbritan` (`sl`, `name`, `abortan`, `abortan_eng`, `position`, `mobile`) VALUES
(41, 'লাবণ্য শিল্পী', '১ম', 1, '', '০১৭১২ ৭০৮ ০৫'),
(42, 'নাজমা আক্তার নাজু', '২য়', 2, '', ''),
(43, 'আহামেদ রাহাত খান', '২য়', 2, '', ''),
(44, '', '', 0, '', ''),
(45, 'অজিত কুমার হালদার', '২য়', 2, '', ''),
(46, 'নাজমা সুলতানা লিলি', '২য়', 2, '', ''),
(48, 'মোঃ তুহিন হোসেন', '৩য়', 3, '', ''),
(49, 'আশরাফ সিদ্দিক আপেল', '৩য়', 3, '', ''),
(50, 'মোঃ ফয়সাল	', '৩য়', 3, '', ''),
(51, 'হাসান ইউসুফ খান', '৩য়', 3, '', ''),
(52, 'জান্নাতুল খোসবু সারমিন', '৪র্থ', 4, '', ''),
(53, 'সুলতানা ইয়াসমিন রিমু', '৪র্থ', 4, '', ''),
(55, 'আছিমা তাসনীম নন্দী', '৫ম	', 5, '', ''),
(56, 'মোস্তফা সরোয়ার নিটোল', '৫ম	', 5, '', ''),
(57, 'ফারহানা আহামেদ চৈতী', '৬ষ্ঠ', 6, '', ''),
(58, 'জাকিয়া সুলতানা মুক্তা', '৭ম', 7, '', ''),
(59, 'আব্দুল্লাহ আল মামুন', '৭ম', 7, '', ''),
(60, 'হাসান মুরাদ চৌধুরী', '৭ম', 7, '', ''),
(61, 'সাইফুল ইসলাম জয়', '৭ম', 7, '', ''),
(62, 'এম আর রাসেল	৭ম', '৭ম', 7, '', ''),
(63, 'রিফাত রেজওয়ানা অনন্যা', '৮ম', 8, '', ''),
(64, 'আবির সিদ্দিকী শুভ্র', '৮ম', 8, '', ''),
(65, 'মোগনিউজ্জামান প্রিন্স', '৮ম', 8, '', ''),
(66, 'হাসান সাঈদ', '৮ম', 8, '', ''),
(67, 'শাহাবুদ্দিন', '৯ম', 9, '', ''),
(68, 'হিমা খান', '৯ম	', 9, '', ''),
(69, 'রানা আলী খান	', '১০ম', 10, '', ''),
(70, 'তানভীর ইসলাম অপু', '১০ম', 10, '', ''),
(71, 'নওরিন আফরিন', '১০ম', 10, '', ''),
(72, 'পূর্বা চৌধুরী', '১০ম', 10, '', ''),
(73, 'সামসি আরা', '১১তম', 11, '', ''),
(74, 'নাজমুল হক চৌধুরী', '১১তম', 11, '', ''),
(75, 'মাসউদুর রহমান', '১১তম', 11, '', ''),
(76, 'আউয়াল ফয়সাল', '১২তম', 12, '', ''),
(77, 'অর্পিতা পাল', '১২তম', 12, '', ''),
(78, 'মোমিনুল ইসলাম অনন্ত', '১২তম', 12, '', ''),
(79, 'জয়া রায়', '১২তম', 12, '', ''),
(80, 'রাজু আহমেদ', '১২তম', 12, '', ''),
(81, 'মামুন সিদ্দিক', '১২তম', 12, '', ''),
(82, 'মহিউদ্দিন সুমন', '১২তম', 12, '', ''),
(83, 'মনি শংকর মৃধা', '	১৩ত', 13, '', ''),
(84, 'দুলালী রাণী কু-ু', '১৩তম', 13, '', ''),
(85, 'হাম্মাদুর রহমান সোহাগ', '১৩তম', 13, '', ''),
(86, 'রোমেল আশরাফ', '১৩তম', 13, '', ''),
(87, 'সাবরিনা', '১৩তম', 13, '', ''),
(88, 'হিমেল', '১৩তম', 13, '', ''),
(89, 'আন্দালিব নাগমা কসÍুরী', '১৪তম', 14, '', ''),
(90, 'আনোয়ার লতিফ', '১৪তম', 14, '', ''),
(91, 'হ্যাপী আক্তার', '১৪তম', 14, '', ''),
(92, 'কেফায়েত উল্লাহ', '১৪তম', 14, '', ''),
(93, 'ইফফাত আরা তাবাসসুম(প্রিমা)', '১৪তম', 14, '', ''),
(94, 'রায়হানা রহমা রুমি', '১৪তম', 14, '', ''),
(95, 'এম এইচ কাউসার', '১৪তম', 14, '', ''),
(96, 'তৌহিদা আফরিন তিথি', '১৪তম', 14, '', ''),
(97, 'সোহাগ', '১৪তম', 14, '', ''),
(98, 'কিরণ শেখর কুন্ডুূ', '১৫তম', 15, '', ''),
(99, 'রাজিব আহমেদ', '	১৫ত', 15, '', ''),
(100, 'সাজেদা হক সুমি', '১৫তম', 15, '', ''),
(101, 'মোঃ শফিকুল ইসলাম', '১৫তম', 15, '', ''),
(102, 'মোঃ সোহেল রানা', '১৫তম', 15, '', ''),
(105, 'তানজুম তামান্না', '১৫তম', 15, '', ''),
(106, 'কাউসার তাহমিনা তানিয়া', '১৫তম', 15, '', ''),
(107, 'ইয়াসমিন আক্তার পিংকি', '১৫তম', 15, '', ''),
(108, 'সাজেদা হক সুমি', '১৫তম', 15, '', ''),
(109, 'মোঃ শফিকুল ইসলাম', '১৫তম', 15, '', ''),
(110, 'মোঃ সোহেল রানা', '১৫তম', 15, '', ''),
(111, 'মকছুদুল করিম সোহেল', '১৫তম', 15, '', ''),
(112, 'ইয়াসমিন আক্তার পিংকি', '১৫তম', 15, '', ''),
(113, 'তানজুম তামান্না', '১৫তম', 15, '', ''),
(114, 'কাউসার তাহমিনা তানিয়া', '১৫তম', 15, '', ''),
(115, 'সোনিয়া খানম', '১৫তম', 15, '', ''),
(116, 'অত্রী কর্মকার	', '১৫তম', 15, '', ''),
(117, 'সাদিয়া ইসলাম পর্শিয়া', '১৫তম', 15, '', ''),
(118, 'আবু রায়হান', '১৫তম', 15, '', ''),
(119, 'ছাবিকুন্নাহার রোজি', '১৫তম', 15, '', ''),
(120, 'বেলাল হোসেন কাউসার	', '১৫তম', 15, '', ''),
(121, 'ইয়াসমিন আক্তার লিপি', '১৫তম', 15, '', ''),
(122, 'জাহিদ হাসান', '১৫তম', 15, '', ''),
(123, 'সাহিদা পারভিন নিশা', '১৫তম', 15, '', ''),
(124, 'শারমিন সুলতানা লিজা', '১৫তম', 15, '', ''),
(125, 'নূর ইসলাম খান ববির', '১৫তম', 15, '', ''),
(126, 'পারভেজ হোসেন', '১৫তম', 15, '', ''),
(127, 'মোর্শেদ	১৫তম', '১৫তম', 15, '', ''),
(131, 'রতন বসাক', '১৫তম', 15, '', ''),
(132, 'আমেনা আক্তার নূরুন', '	১৬ত', 16, '', ''),
(133, 'তাহমিনা আক্তার মিলি', '১৬তম', 16, '', ''),
(134, '	মিরাজুল ইসলাম ', '১৬তম', 16, '', ''),
(135, 'রতন বসাক', '১৬তম', 16, '', ''),
(136, 'রোমানা আক্তার', '১৬তম', 16, '', ''),
(137, 'তাহেরা আক্তার', '১৬তম', 16, '', ''),
(138, 'রাইশা নওশিন', '১৬তম', 16, '', ''),
(139, 'দিলরুবা শারমিন লিনা', '১৬তম', 16, '', ''),
(140, 'স্বরজিত রায়', '১৬তম', 16, '', ''),
(141, 'রুবেল রানা', '১৬তম', 16, '', ''),
(142, 'আব্দুল রহিম', '১৬তম', 16, '', ''),
(145, 'মোঃ রুবেল হোসেন', '১৭তম', 17, '', ''),
(146, 'আকলিমা খানম নিরা', '১৭তম', 17, '', ''),
(147, 'কবীর হোসাইন', '১৭তম', 17, '', ''),
(148, 'জান্নাত আরা সোনিয়া', '১৭তম', 17, '', ''),
(150, 'ফারজানা ইয়াসমিন মুমু', '১৭তম', 17, '', ''),
(151, 'নাহিদ সুলতানা নিঝুম', '১৭তম', 17, '', ''),
(152, 'মোঃ আমিনুল ইসলাম', '১৭তম', 17, '', ''),
(153, 'আরিফুল ইসলাম', '১৭তম', 17, '', ''),
(154, 'ফাতেহা বেলী', '১৮তম', 18, '', ''),
(155, 'কামরুজ্জামান কাঁকন', '১৮তম', 18, '', ''),
(156, 'তামান্না তাবাসসুম খান', '১৮তম', 18, '', ''),
(157, 'হুমায়রা আফরিন', '১৮তম', 18, '', ''),
(158, 'তাসলিমা আফরোজ শেলী', '১৮তম', 18, '', ''),
(159, 'শাহানাজ পারভীন নুপুর', '১৮তম', 18, '', ''),
(160, 'জাহিদুল হাসান', '১৮তম', 18, '', ''),
(161, 'ফাহমিদা বিশ্বাস লিলি', '১৮তম', 18, '', ''),
(162, 'সম্পা রাণী দাস', '১৮তম', 18, '', ''),
(163, 'মিলি আক্তার', '১৮তম', 18, '', ''),
(164, 'রিফাত আরা করীম তিস্তা', '১৯তম', 19, '', ''),
(165, 'রওনক জাহান জেমি', '১৯তম', 19, '', ''),
(166, 'রুমানা করিম তন্বী', '১৯তম', 19, '', ''),
(167, 'নাবিলা তাবাসসুম চৌধুরী', '১৯তম', 19, '', ''),
(168, 'আমেনা বেগম সাথী', '১৯তম', 19, '', ''),
(169, 'কামাল মাহমুদ বাপ্পী', '১৯তম', 19, '', ''),
(170, 'শেখ জিন্নাত সুলতানা', '১৯তম', 19, '', ''),
(171, 'মোঃ তরিকুল ইসলাম', '১৯তম', 19, '', ''),
(172, 'জুবাইদা গুলশান', '১৯তম', 19, '', ''),
(173, 'জয়ন্ত সুতার জয়', '১৯তম', 19, '', ''),
(174, '	হাফিজ আল আসাদ', '১৯তম', 19, '', ''),
(175, 'হালিমা আক্তার সুমী', '১৯তম', 19, '', ''),
(176, 'মাফিয়া সুলতানা ', '১৯তম', 19, '', ''),
(177, 'মোঃ হেলাল উদ্দিন', '১৯তম', 19, '', ''),
(178, 'রাজীব	', '১৯তম', 19, '', ''),
(179, '	মীনাক্ষী সেন', '২০তম', 20, '', ''),
(180, 'মোঃ আব্দুল কাইয়ুম', '২০তম', 20, '', ''),
(181, 'মোছাঃ রুবি বেগম', '২০তম', 20, '', ''),
(182, 'তানভীর আহমেদ', '২০তম', 20, '', ''),
(183, 'সুরমা আক্তার', '২০তম', 20, '', ''),
(184, 'মিরাজুল ইসলাম', '২০তম', 20, '', ''),
(185, 'ফাতেমা বন্যা	', '২০তম', 20, '', ''),
(186, 'সাদিয়া আফসানা', '২০তম', 20, '', ''),
(187, '	কাজী মোঃ মাহাদী হাসান', '২০তম', 20, '', ''),
(189, 'লিয়াকত সালমান', '২১তম', 21, '', ''),
(190, 'কমলেশ সরকার', '২১তম', 21, '', ''),
(191, 'ফারহানা আক্তার', '২১তম', 21, '', ''),
(192, 'বকুল রানী রায়', '২১তম', 21, '', ''),
(193, 'সায়মা শারমিন ', '২১তম', 21, '', ''),
(194, 'সানজিদা বিনতে ইয়ার লীনা', '২১তম', 21, '', ''),
(195, 'তামান্না জাহান', '২১তম', 21, '', ''),
(196, 'রীতা রাণী ম-ল	', '২১তম', 21, '', ''),
(197, 'খায়রুল', '২১তম', 21, '', ''),
(198, 'রনি সুলতানা ', '২১তম', 21, '', ''),
(199, 'নিয়ামত আলী এনায়েত', '২১তম', 21, '', ''),
(200, 'মোঃ শেখ সাদী', '২১তম', 21, '', ''),
(201, 'মীরা আক্তার ', '২১তম', 21, '', ''),
(202, 'সুমাইয়া বিনতে হক', '২১তম', 21, '', ''),
(203, 'শারমিন আক্তার নিপা ', '২১তম', 21, '', ''),
(204, 'হুসাইন শাহরিয়ার', '২১তম', 21, '', ''),
(205, 'নয়ন তারা', '২১তম', 21, '', ''),
(206, 'মোঃ ইউসুফ হোসেন', '২১তম', 21, '', ''),
(207, 'বিউটি আক্তার', '২১তম', 21, '', ''),
(209, 'সাদিয়া সাইদ', '২২তম', 22, '', ''),
(210, 'ফারজানা আলম লীনা', '২২তম', 22, '', ''),
(211, 'আবুল বারাকাত আকিব', '২২তম', 22, '', ''),
(212, 'সুরাইয়া মুনমুন শাপলা', '২২তম', 22, '', ''),
(213, 'সৈয়দা আয়েশা নাঈম', '২২তম', 22, '', ''),
(214, 'নয়ন মিয়া ', '২২তম', 22, '', ''),
(215, 'মোঃ শফিকুল ইসলাম', '২২তম', 22, '', ''),
(216, 'মুতাছিম বিল্লাহ', '২২তম', 22, '', ''),
(217, 'কুলসুমা খাতুন সেলিনা', '২২তম', 22, '', ''),
(218, 'নাসরিন আক্তার ', '২২তম', 22, '', ''),
(219, 'নওশাদ মহসিন পাটোয়ারী', '২২তম', 22, '', ''),
(220, 'নাহিদ আক্তার', '২২তম', 22, '', ''),
(221, 'সাব্বির হোসেন', '২২তম', 22, '', ''),
(222, 'রেজাউল আউয়াল', '২২তম', 22, '', ''),
(223, 'ফরিদা ইয়াসমিন লিন্ডা', '২২তম', 22, '', ''),
(224, 'মাহমুদুল হাসান', '২২তম', 22, '', ''),
(225, 'ইমরান হোসেন', '২২তম', 22, '', ''),
(226, 'সাবিকুন নাহার', '২২তম', 22, '', ''),
(227, 'মৌসুমী আক্তার', '২২তম', 22, '', ''),
(228, 'আফসানা ফেরদৌস', '২২তম', 22, '', ''),
(229, 'রাজেশ্বর দাস', '২২তম', 22, '', ''),
(230, 'মোঃ বখতিয়ার অর্ণিব', '২২তম', 22, '', ''),
(232, 'ফারহানা আলম লামিয়া', '২২তম', 22, '', ''),
(233, 'ফাউজিয়া বিনতে রফিক', '২৩তম', 23, '', ''),
(234, 'জয়শ্রী অধিকারী', '২৩তম', 23, '', ''),
(235, 'মোঃ ইকবাল হোসেন লাবু', '২৩তম', 23, '', ''),
(236, 'তন্ময় বাড়ৈ', '২৩তম', 23, '', ''),
(237, 'ওমর ফারুক মজুমদার', '২৩তম', 23, '', ''),
(238, 'তানজিন পিয়াল', '২৩তম', 23, '', ''),
(239, 'মোঃ জোবায়ের', '২৩তম', 23, '', ''),
(240, 'রুবেল ব্যাপারী রাহাত', '২৩তম', 23, '', ''),
(241, 'রিমানা সেতু', '২৩তম', 23, '', ''),
(242, 'সনম সিদ্দিকী সিতি ', '২৩তম', 23, '', ''),
(243, 'জহুরা খাতুন', '২৩তম', 23, '', ''),
(244, 'ফাহিমা আক্তার', '২৩তম', 23, '', ''),
(245, 'মিনহাজ রুবেল', '২৩তম', 23, '', ''),
(246, '	মোঃ রায়হান', '২৩তম', 23, '', ''),
(247, 'জান্নাতুল ফেরদৌস পিনাক', '২৩তম', 23, '', ''),
(248, 'মমতাজ হোসেন সুলতান', '২৩তম', 23, '', ''),
(249, 'গোলাম সারোয়ার', '২৩তম', 23, '', ''),
(250, 'নাজমুল হাসান সোহাগ', '২৩তম', 23, '', ''),
(251, 'হাবিবুর রহমান	২৩তম', '২৩তম', 23, '', ''),
(252, 'মৌজে জান্নাত জাহিদা', '১৭তম', 17, '', ''),
(253, 'জুয়েল আহমেদ', '২২তম', 22, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `executive_committe`
--

CREATE TABLE IF NOT EXISTS `executive_committe` (
`sl` int(2) NOT NULL,
  `designation` varchar(15) NOT NULL,
  `name` varchar(32) NOT NULL,
  `mobile` varchar(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `executive_committe`
--

INSERT INTO `executive_committe` (`sl`, `designation`, `name`, `mobile`) VALUES
(1, 'সভাপতি', 'এ কে এম সামছুদ্দহা ', '০১৭১১২২২৬৯৯'),
(2, 'সহ-সভাপতি', 'ফারুক মজুমদার', '০১৭১৯২৭৯৩৮'),
(3, 'সহ-সভাপতি', 'ফারুক মজুমদার', '০১৭১৯২৭৯৩৮'),
(4, 'সহ-সভাপতি', 'ফারুক মজুমদার', '০১৭১৯২৭৯৩৮'),
(5, 'সহ-সভাপতি', 'ফারুক মজুমদার', '০১৭১৯২৭৯৩৮');

-- --------------------------------------------------------

--
-- Table structure for table `home_article`
--

CREATE TABLE IF NOT EXISTS `home_article` (
`sl` int(3) NOT NULL,
  `article_id` int(3) NOT NULL,
  `article_heading` varchar(80) NOT NULL,
  `full_article` varchar(2000) NOT NULL,
  `picture` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_article`
--

INSERT INTO `home_article` (`sl`, `article_id`, `article_heading`, `full_article`, `picture`, `date`, `upload_date`) VALUES
(27, 0, 'কাজী নজরুল ইসলামের জন্মবার্ষিকী উপলক্ষে আবৃত্তি প্রযোজনা ‘অগ্নিঝরা রুদ্রবীণা’', 'জাতীয় কবি কাজী নজরুল ইসলামের ১১৬ তম জন্মবার্ষিকী উপলক্ষে আবৃত্তি প্রযোজনা ‘অগ্নিঝরা রুদ্রবীণা’ এর ২য় মঞ্চায়ন করল সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র। গত ২৮ মে ২০১৫, বৃহস্পতিবার সন্ধ্যা ৭টায় শাহবাগ পাবলিক লাইব্রেরীর শওকত ওসমান মিলনায়তন – এ মঞ্চায়ন অনুষ্ঠিত হয়েছে।\r\nআবৃত্তি পরিবেশনায় কাজী নজরুল ইসলামের লেখা ২৩ টি কবিতা দৃশ্য কল্পের পরম্পরায় সাজিয়ে উপস্থাপন করা হয়েছে। “কভু প্রশান্ত, কভু অশান্ত দারুণ স্বেচ্ছাচারী/আরুণ খুনের তরুণ, আমি বিধির দর্পহারী” স্লোগানে প্রযোজনাটির গ্রন্থনা করেছেন আরুণ আরিফ এবং নির্দেশনায় ছিলেন এ কে এম সামছুদ্দোহা।\r\nঅনুষ্ঠানটিতে একক ও দলীয় আবৃত্তি পরিবেশন করেন তরুণ আবৃত্তি শিল্পীবৃন্দ।\r\nমানুষ, নারী, ইশ্বর, সাম্যবাদী, বাংলাদেশ, অভিযান সহ জাতীয় কবির লেখা বিভিন্ন বিখ্যাত কবিতা আবৃতি করেন সংবৃতা আবৃত্তি শিল্পীরা।\r\nঅনুষ্টানটিতে এ কে এম সামছুদ্দোহা, শামছি আরা, লাবন্য শিল্পী, হ্যাপী আকতার, কাকন জামান, হাম্মাদ সোহাগ সহ বেশ কয়েকজন আবৃত্তিশিল্পী তাদের আবৃত্তি পরিবেশন করেন।\r\n আবৃত্তি প্রযোজনাটির ২য় মঞ্চায়নে আবৃত্তিপ্রেমীদের সরব উপস্থিতিতে প্রাণবন্ত ছিল এ কবিতা পাঠের আসর।\r\nউল্লেখ্য, আবৃত্তি শিল্পের চর্চায় ও বিকাশের লক্ষে সংবৃতা নিয়মিত আবৃত্তি কর্মশালা আয়োজন ও ব্যক্তি বা বিষয় ভিত্তিক আবৃত্তি প্রযোজনার নিয়মিত মঞ্চায়নের করে যাচ্ছে প্রায় দশ বছর সময় ধরে।', './images/hpa_image/agnijhara3.png', '2015-05-28', '2015-05-30 05:15:24'),
(28, 0, 'সংবৃতার উদীয়মান আবৃত্তিশিল্পীদের নিয়ে অনু প্রযোজনা “স্নেহে-সংগ্রামে জননী”', 'ঢাকা বিশ্ববিদ্যালয়ের টিএসসি কেন্দ্রিক গতিশীল আবৃত্তি সংগঠন সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র আগামী ২১মে ২০১৫ বৃহস্পতিবার সন্ধ্যা ৬.০০ টায় কেন্দ্রিয় গণগ্রন্থাগারের সেমিনার কক্ষে আয়োজন করতে যাচ্ছে সংবৃতার নতুন সম্ভাবনাময়ী আবৃত্তিশিল্পীদের নিয়ে অনু প্রযোজনা শহীদ-জননী জাহানারা ইমাম কে নিবেদিত পঙক্তিমালা  স্নেহে-সংগ্রামে জননী । সর্বশেষ সমাপ্ত হওয়া দুটি আবর্তনের শিক্ষার্থীরা অংশ নেবেন এ প্রযোজনাতে । এছাড়াও অনু প্রযোজনার পাশাপাশি থাকবে মুক্তিযুদ্ধকালিন রণাঙ্গন  থেকে  মুক্তিযোদ্ধাদের লেখা কয়েকটি চিঠি পাঠ । এতে অংশ নেবেন সংবৃতার প্রতিশ্রুতিশীল কয়েকজন আবৃত্তিশিল্পী । ঘরোয়া ভাবে আয়োজিত এ অনুষ্ঠানে সংবৃতার সভাপতি, সাধারণ সম্পাদক সহ দলের সকল সিনিয়র সদস্যরা উপস্থিত থাকবেন ।', './images/hpa_image/Untitled-1.png', '2015-05-21', '2015-05-30 05:21:16'),
(29, 0, 'ভাষা সংগ্রাম নিয়ে আবৃত্তি প্রযোজনা “একুশ” অনুষ্ঠিত', 'বাঙালির জাতীয় চেতনায় একুশে ফেব্রুয়ারী এক নতুন দিগন্তের সূচনা করেছিল যার হাত ধরেই আমরা পাই স্বাধীন বাংলাদেশ। ভাষা সংগ্রামের মাস ফেব্রুয়ারীকে সামনে সামনে রেখে তাই ১৯ ফেব্রুয়ারী, বৃহস্পতিবার সন্ধ্যা ৬:৩০ এ শাহবাগ এর জাতীয় পাবলিক লাইব্রেরীর শওকত ওসমান স্মৃতি মিলনায়তন – এ ভাষা আন্দোলনের কবিতা নিয়ে সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র এর আবৃত্তি অনুষ্ঠান ‘একুশ’ পরিবেশন করা হয়।\r\nএ আবৃত্তি পরিবেশনায় ভাষা আন্দোলনের উপর লেখা বিভিন্ন কবির লেখা ২৭ টি কবিতা দৃশ্য কল্পের পরম্পরায় সাজিয়ে উপস্থাপন করা হয়েছে। আবৃত্তি প্রযোজনাটির স্লোগান ছিল “পুস্পের মত ঝরেপড়া থোকা থোকা রক্তের ছোপ/বাংলার মানুষের ধমনীতে পুঞ্জীভূত হোক”। আরুণ আরিফের গ্রন্থনা এবং এ কে এম সামছুদ্দোহার নির্দেশনায় অনুষ্ঠানাটিতে একক ও দলীয় আবৃত্তি পরিবেশন করেন তরুণ আবৃত্তি শিল্পীবৃন্দ।\r\nতোফাজ্জল হোসেনের ‘একুশের গান’, আসাদ চৌধুরীর ‘ফাল্গুন এলেই’, দাউদ হায়দারের ‘একুশ’, শহীদুল্লাহ কায়সারের ‘শহীদের মাকে’, আবু জাফর ওবায়দুল্লাহের ‘কোন এক মাকে’, মাহবুব উল আলম চৌধুরীর ‘কাঁদতে আসিনি ফাঁসির দাবি নিয়ে এসেছি’ এবং আলাউদ্দিন আল আজাদের ‘স্মৃতিস্তম্ভ’ সহ বেশ কয়েকটি কবিতা আবৃত্তি করেন সংবৃতার তরুণ আবৃত্তি শিল্পীরা।\r\nঅনুষ্টানটিতে এ কে এম সামছুদ্দোহা, শামছি আরা, লাবন্য শিল্পী, হ্যাপী আকতার, কাকন জামান, হাম্মাদ সোহাগ সহ বেশ কয়েকজন আবৃত্তিশিল্পী তাদের আবৃত্তি পরিবেশন করেন।\r\nআবৃত্তি প্রযোজনাটির ৪র্থ মঞ্চায়নে আবৃত্তিপ্রেমীদের সরব উপস্থিতিতে প্রাণবন্ত ছিল এ কবিতা পাঠের আসর। প্রায় এক হাজারের মত কবিতাপ্রেমী তন্ময় হয়ে উপভোগ করেন বসন্তের সান্ধ্য কালীন এ আবৃত্তি আয়োজন।\r\nউল্লেখ্য, আবৃত্তি শিল্পের চর্চায় ও বিকাশের লক্ষে সংবৃতা নিয়মিত আবৃত্তি কর্মশালা আয়োজন ও ব্যক্ত', './images/hpa_image/Unti-1.png', '2015-02-19', '2015-05-30 05:24:56'),
(30, 0, '  সংবৃতার অমর একুশে স্মরণ', 'গতকাল ১৯ শে ফেব্রুয়ারী সন্ধ্যা ৬.৩০ এ কেন্দ্রীয় গণগ্রন্থারের শওকত ওসমান স্মৃতি মিলনায়তনে গতিশীল আবৃত্তি সংগঠন সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র মহান ২১ শে ফেব্রুয়ারী উপলক্ষে আয়োজন করে আবৃত্তি অনুষ্ঠান একুশ’এর । অনুষ্ঠানে প্রায় সতের জন আবৃত্তি শিল্পী ৩০ টির অধিক আবৃত্তি পরিবেশন করেন । উল্লেখযোগ্যদের মধ্যে এ কে এম সামছুদ্দোহা , মাসুদ পারভেজ, লাবণ্য শিল্পী, মোগনিউজ্জামান প্রিন্স,শামসি আরা,  হাম্মাদ সোহাগ, হ্যাপি আক্তার  আবৃত্তি পরিবেশন করেন । পুরো অনুষ্ঠানটি সাজানো হয়েছিল ভাষা দিবস নির্ভর কবিতা দিয়ে । এবারের অনুষ্ঠানের মঞ্চ সজ্জা ছিল চোখে পড়ার মত ।\r\n\r\nপ্রযোজনাটির গ্রন্থনায় ছিলেন : অরূণ আরিফ এবং নির্দেশনায় ছিলেন এ কে এম সামছুদ্দোহা ।', './images/hpa_image/Untitled-11.png', '2015-02-20', '2015-06-06 04:29:33'),
(32, 0, 'সমত্বরণ ২০১৫', 'টি এস সি, ঢাকা বিশ্ববিদ্যালয় কেন্দ্রিক  গতিশীল আবৃত্তি সংগঠন সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র আগামী ৬ ফেব্রুয়ারী ‘১৫ শুক্রবার সকাল ১০.৩০ এ মুনির চৌধুরী সম্মেলন কক্ষ টি এস সি, ঢাকা বিশ্ববিদ্যালয়ে আয়োজন করতে যাচ্ছে  সর্বশেষ সমাপ্ত হওয়া দুটি আবর্তন (২৩ ও ২৪) কে নিয়ে সমত্বরণ অনুষ্ঠানের । অনুষ্ঠানে ২৩ ও ২৪ তম আবর্তনের যে সকল শিক্ষার্থী কৃতকার্য হয়েছে তাদের কে সনদপত্র প্রদান করা হবে । এছাড়া দুটি আবর্তনের সদস্যদের নিয়ে আলাদা ভাবে আবৃত্তি প্রতিযোগিতা অনুষ্ঠিত হবে এবং বিজয়ীদের পুরস্কৃত করা হবে । যে সকল শিক্ষার্থী ক্লাসে শতভাগ উপস্থিত ছিল তাদেরও পুরস্কৃত করা হবে ।\r\n\r\nএর পাশাপাশি ২০১২, ২০১৩ ও ২০১৪ সালের সংবৃতার শ্রেষ্ঠ কর্মী ঘোষণা করা হবে এবং সম্মাননা প্রদান করা হবে ।', './images/hpa_image/Untitled-3.png', '2015-01-31', '2015-06-06 04:50:49'),
(33, 0, 'সংবৃতার মুক্তিযুদ্ধ ভিত্তিক আবৃত্তি প্রযোজনা “সবুজ শাড়িতে লাল রক্তের ছোপ” মঞ্চায়', 'গত ২৬ শে ডিসেম্বর ‘১৪ শুক্রবার সন্ধ্যা ৬.০০ টায় সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র ঢাকা বিশ্ববিদ্যালয়ের নাটমণ্ডল অডিটোরিয়ামে আয়োজন করে মুক্তিযুদ্ধ ভিত্তিক কবিতা নিয়ে আবৃত্তি প্রযোজনা সবুজ শাড়িতে লাল রক্তের ছোপ ।  ১৮ জন আবৃত্তি শিল্পী ৩২ টি কবিতা আবৃত্তি করেন । মঞ্চে সিনিয়র পারফরমারদের পাশাপাশি তরুণ প্রতিশ্রুতিশীল আবৃত্তি শিল্পী ও ছিলেন । কবিতা আবৃত্তির সাথে সাথে আবহ সঙ্গীত কবিতাকে একটি নতুন মাত্রা এনে দেয় । যেন চোখের সামনে মুক্তিযুদ্ধের এক একটি দৃশ্য ভেসে উঠছে । ১ ঘন্টা ২০ মিনিটের এ প্রযোজনাটি ছিল সংবৃতার ১০ম প্রযোজনা এবং  ৯ম মঞ্চায়ন ।  গ্রন্থনায় ছিলেন সামসুজ্জামান বাবু এবং নির্দেশনায় ছিলেন এ কে এম সামছুদ্দোহা ।', './images/hpa_image/Untitled-4.png', '2014-12-27', '2015-06-06 04:58:26'),
(34, 0, 'সংবৃতার উচ্চতর আবৃত্তি কর্মশালা সম্পন্ন', 'গত ২১ শে নভেম্বর ’১৪ শুক্রবার সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র ঢাকা বিশ্ববিদ্যালয়ের ছাত্র-শিক্ষক কেন্দ্রের অভ্যন্তরীন ক্রীড়া কক্ষে আয়োজন করে দিনব্যাপী উচ্চতর আবৃত্তি কর্মশালা । কর্মশালায় প্রশিক্ষক হিসেবে ছিলেন দেশবরেণ্য আবৃত্তিশিল্পী ও প্রশিক্ষক আশরাফুল আলম, নিরঞ্জন অধিকারী, হাসান আরিফ ও গোলাম সারোয়ার । সংবৃতার শতাধিক সদস্য এ কর্মশালায় অংশগ্রহণ করে । কর্মশালা শেষে প্রশিক্ষনার্থীদের হাতে সনদপত্র তুলে দেন  আবৃত্তিশিল্পী ও প্রশিক্ষক  আশরাফুল আলম ।', './images/hpa_image/Untitled-5.png', '2014-11-08', '2014-11-07 18:00:00'),
(35, 0, 'সংবৃতার উচ্চতর আবৃত্তি কর্মশালা', 'আগামী ২১ শে নভেম্বর ’১৪ ইং তারিখে “সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র” মুনির চৌধুরী সম্মেলন কক্ষ,টিএসসি ঢাকা বিশ্ববিদ্যালয়ে আয়োজন করতে যাচ্ছে দিনব্যাপী উচ্চতর আবৃত্তি কর্মশালার । সংবৃতার সকল সদস্য এ কর্মশালায় ১৫০ টাকা নিবন্ধন ফি দিয়ে নিবন্ধন করতে পারবেন । সকাল ৮:৩০ এ কর্মশালা শুরু হয়ে শেষ হবে সন্ধ্যা ৬:৩০ এ । চা বিরতি সহ মধ্যাহ্ন ভোজের ব্যবস্থা থাকবে । কর্মশালায় ক্লাস নেবেন দেশবরেণ্য আবৃত্তি প্রশিক্ষক আশরাফুল আলম, নিরঞ্জন অধিকারী, হাসান আরিফ, গোলাম সারওয়ার, আহকাম উল্লাহ্ । কর্মশালা শেষে সনদপত্র বিতরন করা হবে ।\r\n\r\nবি.দ্র: সকলকে ১৫ নভেম্বর এর মধ্যে নিবন্ধন করতে বলা হয়েছে ।\r\n\r\nপক্ষে,\r\nসামসুজ্জামান বাবু\r\nসাধারণ সম্পাদক\r\nসংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র', './images/hpa_image/Untitled-6.png', '2014-11-08', '2014-11-07 18:00:00'),
(36, 0, 'নৌ-ভ্রমণ ‘১৪', 'গত ৩১ শে অক্টোবর ‘১৪ শুক্রবার সংবৃতা তার সকল সদস্যদের নিয়ে আয়োজন করে ঢাকা টু চাঁদপুর নৌ-ভ্রমণের । একশ’র কাছাকাছি সদস্য এ ভ্রমণে অংশগ্রহন করে । সকাল ৭:২০ এ লঞ্চ সদরঘাট থেকে চাঁদপুরের উদ্দেশ্যে ছেড়ে যায় এবং সন্ধ্যা ৬:০০ সদরঘাট ফিরে আসে । সংবৃতার সকল সদস্যদের কাছে স্মরণীয় হয়ে থাকবে এ নৌ-ভ্রমণ ।', './images/hpa_image/Untitled-7.png', '2014-11-05', '2015-06-06 05:20:50'),
(37, 0, 'সংবৃতার দুই দিনব্যাপী আবৃত্তি উৎসব সমাপ্ত', 'বিপুল উৎসাহ উদ্দীপনার মধ্য দিয়ে শনিবার শেষ হয়েছে আবৃত্তি সংগঠন সংবৃতার দুই দিনব্যাপী আবৃত্তি উৎসব। ঢাকা বিশ্ববিদ্যালয়ের ছাত্র-শিক্ষক কেন্দ্র (টিএসসি) মিলনায়তনে অনুষ্টিত হওয়া এ উৎসবের স্লোগান ছিল ‘শুধু অকারণ পুলকে, নদী জলে পড়া আলোর মতন ছুটে যা ঝলকে ঝলকে’। সংগঠনের নবম প্রতিষ্ঠাবার্ষিকী উপলক্ষে এ উৎসবের আয়োজন করা হয় যা বঙ্গবন্ধুর নামে উৎসর্গ করা হয়।\r\n\r\nবিকালে দুই দিনব্যাপী এ উৎসবের সমাপনী পর্বে আবৃত্তি প্রতিযোগিতায় বিজয়ীদের মধ্যে পুরস্কার বিতরণ করে অনুষ্টানের সমাপ্তি টানেন বেসামরিক বিমান পরিবহণ ও পর্যটন মন্ত্রী রাশেদ খান মেনন ।এতে বিশেষ অতিথি হিসেবে আর উপস্হিত ছিলেন স্বাধীন বাংলা বেতার কেন্দ্রের কন্ঠসৈনিক আশরাফুল আলম, ও সম্মিলিত সাংস্কৃতিক জোটের সাধারণ সম্পাদক হাসান আরিফ। অনুষ্টানটিতে সভাপতিত্ব করেন সংগঠনের সভাপতি এ কে এম শামসুদ্দোহা ।\r\n\r\nপ্রতিযোগিতায় বিভিন্ন শিক্ষাপ্রতিষ্ঠান থেকে তিন শতাধিক প্রতিযোগী অংশ নেয়।উৎসবে আবৃত্তি প্রতিযোগিতার পাশাপাশি দেশবরেণ্য আবৃত্তিশিল্পীদের একক আবৃত্তির পাশাপাশি বিভিন্ন সংগঠনের বৃন্দ প্রযোজনা এবং পূর্ণাঙ্গ প্রযোজনা পরিবেশিত হয়। শনিবার সমাপনী দিনে দলগত পরিবেশনায় অংশ নেয় স্বরশ্রুতি, স্বর ব্যাঞ্জন, সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র। একক আবৃত্তিতে অংশ নেন ফয়জুল আলম পাপ্পু, শিমুল মুস্তাফা, জি এম মোর্শেদ, শিরিন ইসলাম, শামীম মহিউদ্দিন, বাপ্পি আকন্দ, গোলাম সারওয়ার, রেজীনা ওয়ালী লীনা, ফখরুল ইসলাম তারা, আশরাফুল আলম, শান্তা শ্রাবণী, আহসান উল্লাহ তমাল প্রমুখ।\r\n\r\nসংবৃতা আবৃত্তিচর্চা ও বিকাশ কেন্দ্র বাংলাদেশের একটি প্রগতিশীল স্বেচ্ছাসেবী আবৃত্তি সংগঠন। প্রায় সহস্রাধিক সদস্য নিয়ে সংবৃতা আবৃত্তির নিয়মিত প্রযোজনা মঞ্চায়ন ও কর্মশালা আয়োজনের মাধ্যমে আবৃত্তির বিকাশে সক্রিয়ভাবে কাজ করে যাচ্ছে। সংবৃতা’র ৯ম প্রতিষ্ঠাবর্ষে এই আবৃত্তি উৎসব আয়োজন করা হয় ।\r\n\r\nউৎসব বিদ্যালয়, মহাবিদ্যালয়, বিশ্ববিদ্যালয় পর্যায় এর শিক্ষার্থীদের অংশগ্রহণে আবৃত্তি প্রতিযোগিতা অনুষ্ঠিত হয়। প্রতিযোগিতায় বিভিন্ন শিক্ষা প্রতিষ্ঠান থেকে সহস্রাধিক প্রতিযোগী অংশগ্রহণ করে। প্রতিযোগিতায় ৪ টি পর্বে বিজয়ীদের শিশু পর্যায়ে- কবি শামসুর রাহমান পদক, বিদ্যালয় পর্যায়ে – শিল্পী ওয়াহিদুল হক পদক, কলেজ পর্যায়ে – বাক্-শিল্পাচার্য নরেন বিশ্বাস পদক ও বিশ্ববিদ্যালয় পর্যায়ে – আবৃত্তিশিল্পী গোলাম মুস্তাফা পদক প্', './images/hpa_image/Untitled-8.png', '2014-08-25', '2015-06-06 05:27:47'),
(38, 0, 'দুই দিনব্যাপী সংবৃতা আবৃত্তি উৎসব ২০১৪ উদযাপন শুরু', 'বিপুল উৎসাহ উদ্দীপনার মধ্য দিয়ে শুক্রবার শুরু হয়েছে আবৃত্তি সংগঠন সংবৃতার দুই দিনের আবৃত্তি উৎসব।ঢাকা বিশ্ববিদ্যালয়ের ছাত্র-শিক্ষক কেন্দ্র (টিএসসি) মিলনায়তনে শুরু হওয়া এ উৎসবের স্লোগান ‘শুধু অকারণ পুলকে, নদী জলে পড়া আলোর মতন ছুটে যা ঝলকে ঝলকে’। সংগঠনের নবম প্রতিষ্ঠাবার্ষিকী উপলক্ষে ষষ্ঠ বারের মতো উৎসবের আয়োজন করা হয়েছে। এবারের উৎসব বঙ্গবন্ধু শেখ মুজিবুর রহমানকে উৎসর্গ করা হয়েছে।\r\n\r\nসকালে মঙ্গল প্রদীপ জ্বেলে দুই দিনব্যাপী এ উৎসব উদ্বোধন করেন তথ্য মন্ত্রী হাসানুল হক ইনু । বিশেষ অতিথি ছিলেন ঢাকা জেলা পুলিশ সুপার হাবিবুর রহমান, জাতীয় গৃহায়ণ কর্তৃপক্ষের চেয়ারম্যান শহীদ উল্লাহ খন্দকার এবং বাংলাদেশ আবৃত্তি সমন্বয় পরিষদের সাধারণ সম্পাদক আহ্কাম উল্লাহ । অনুষ্টানটিতে সভাপতিত্ব করেন সংগঠনের সভাপতি এ কে এম শামসুদ্দোহা।\r\n\r\n5_edপ্রথম দিনে ছিল একক আবৃত্তি, বিভিন্ন সংগঠনের বৃন্দ পরিবেশনা এবং আবৃত্তি প্রতিযোগিতা। প্রতিযোগিতায় বিভিন্ন শিক্ষাপ্রতিষ্ঠান থেকে তিন শতাধিক প্রতিযোগী অংশ নেয়।\r\n\r\nউৎসবের প্রথম দিনে দেশবরেণ্য আবৃত্তিশিল্পীদের একক আবৃত্তির পাশাপাশি বিভিন্ন সংগঠনের বৃন্দ প্রযোজনা এবং পূর্ণাঙ্গ প্রযোজনা পরিবেশিত হয়। বৃন্দ পরিবেশনায় অংশ নেয় প্রজন্মকন্ঠ, ষড়জ সাংস্কৃতিক অঙ্গন, চারুকন্ঠ । বিচার হিসাবে উপস্থিত ছিলেন: এনামুল হক বাবু, মীর বরকত, রফিকুল ইসলাম, ইকবাল খোরশেদ, মাসকুর-এ সাত্তার কল্লোল, ঝর্ণা সরকার,  সুপ্রভা সেবতি,  প্রমুখ। দিনশেষে পূর্নাঙ্গ প্রযোজনা ‘মুখোমুখি দাড়াঁবার এইতো সময়’ পরিবেশন করে মুক্তধারা আবৃত্তি চর্চা কেন্দ্র।\r\n\r\nশনিবার সমাপনী দিনে ঢাকা বিশ্ববিদ্যালয়ের ছাত্র-শিক্ষক কেন্দ্র (টিএসসি) মিলনায়তনে প্রতিযোগিতার বিজয়ীদের মধ্যে পুরস্কার বিতরণ করা হবে।এতে প্রধান অতিথি থাকবেন বেসামরিক বিমান পরিবহণ ও পর্যটন মন্ত্রী রাশেদ খান মেনন ।\r\n\r\nসংবৃতা আবৃত্তিচর্চা ও বিকাশ কেন্দ্র বাংলাদেশের একটি প্রগতিশীল স্বেচ্ছাসেবী আবৃত্তি সংগঠন। প্রায় সহস্রাধিক সদস্য নিয়ে সংবৃতা আবৃত্তির নিয়মিত প্রযোজনা মঞ্চায়ন ও কর্মশালা আয়োজনের মাধ্যমে আবৃত্তির বিকাশে সক্রিয়ভাবে কাজ করে যাচ্ছে। সংবৃতা’র ৯ম প্রতিষ্ঠাবর্ষে এই আবৃত্তি উৎসব আয়োজন করা হয় ।\r\n\r\nউৎসবে বিদ্যালয়, মহাবিদ্যালয়, বিশ্ববিদ্যালয় পর্যায় এর শিক্ষার্থীদের অংশগ্রহণে আবৃত্তি প্রতিযোগিতা অনুষ্ঠিত হয়।প্রতিযোগিতায় বিভিন্ন শিক্ষা প্রতিষ্ঠান থেকে সহস্রাধি', './images/hpa_image/4_ed.jpg', '2014-08-22', '2015-06-06 06:36:27'),
(39, 0, 'সাফল্যের সাত বছর', 'সপ্তমবর্ষে পদার্পন করল সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র। ২৯ জুলাই ২০০৫ শুক্রবার বিকাল ৪টায় ঢাকা বিশ্ববিদ্যালয়ের কার্জন হল এর পদার্থ বিজ্ঞান বিভাগের সামনে¡ ৩৫ জন আবৃত্তি প্রেমীর সমন্বয়ে সংবৃতা আবৃত্তিচর্চা ও বিকাশ কেন্দ্র নামে সংগঠনের যাত্রা শুরু হয়। জন্মলগ্ন থেকেই নবীনদের আধিক্য ছিল, ছিল আবৃত্তি চিন্তনে নতুনত্ব।\r\n\r\nসাংগঠনিক দৃঢ়তা ও আবৃত্তি শৈলীতে ভিন্নমাত্রা সংযোজন এবং আবৃত্তি প্রযোজনা মঞ্চায়নের ধারাবহিকতায় সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র স্বল্প সময়ে আবৃত্তি প্রাঙ্গনে বিশেষ অবস্থান অর্জন করে। ২৯ জুলাই ২০১২, ররিবার সন্ধ্যায় অভ্যন্তরীণ ক্রীড়া কক্ষ, টিএসসি, ঢাকা বিশ্ববিদ্যালয়ে আয়োজন করা হয় সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র এর সপ্তম প্রতিষ্ঠাবার্ষিকী উদযাপন ও ইফতার। সংবৃতার এই আয়োজনে অংশগ্রহন করে সহযোগী সংগঠনের সদস্যসহ সাংস্কৃতিক অঙ্গনের বিশিষ্ট্যজন।\r\n\r\nসংগঠনের সাধারণ সম্পাদক সামসুজ্জামান বাবু-র উপস্থাপনয় ও সভাপতি এ. কে. এম সামছুদ্দোহা-র সভাপতিত্বে অনুষ্ঠিত সংক্ষিপ্ত আলোচনা সভায় বক্তব্য দেন সম্মিলিত সাংস্কৃতিক জোটের সভাপতি নাসির উদ্দিন ইউসূফ, সহসভাপতি গোলাম কুদ্দুছ এবং সোনালী ব্যাংকের পরিচালক সাইমুম সরওয়ার কমল। অষ্টাদশ আবর্তনের সৌজন্যে প্রতিষ্ঠাবার্ষিকীর কেক উপস্থিত অতিথি ও সংবৃতার সহযাত্রীদের নিয়ে কাটেন সংগঠনের প্রতিষ্ঠাতা সদস্য এ. কে. এম সামছুদ্দোহা, কাজী রাজেশ, জহির উদ্দিন গাজী, সামসুজ্জামান বাবু, জালাল উদ্দিন হীরা, আরিফ আহমেদ ও নাসির উদ্দিন পিটু। সংগঠনের নিয়মিত অনিয়মিত সকল সদস্যের উপস্থিতিতে পরিবেশ উৎসবমুখর হয়ে উঠে।', './images/hpa_image/ifter2.jpg', '2013-08-02', '2015-06-06 06:50:19'),
(40, 0, 'আবৃত্তি উৎসব-২০১২', '\r\nআবৃত্তি উৎসব-২০১২\r\nআবৃত্তি উৎসব-২০১২\r\n\r\nউৎসব মানেই জাঁকজমক সাজ-সজ্জা, ভরপুর আনন্দ। আবৃত্তি উৎসব- আবৃত্তির আনন্দমেলা। আবৃত্তিপ্রিয় মানুষের মিলনমেলা। সারাদিন আবৃত্তির নানান বিষয় নিয়ে বিশাল এক আয়োজন। দেশের বিশিষ্ট্য জনদের সান্নিধ্য। আবৃত্তি প্রযোজনা মঞ্চায়ন, প্রখ্যাত আবৃত্তিশিল্পীদেও একক আবৃত্তি, বিভিন্ন দলের বৃন্দ আবৃত্তি পরিবেশন এবং আবৃত্তির ব্যাপন প্রক্রিয়ার অংশ হিসাবে আবৃত্তি প্রতিযোগিতা।\r\n\r\nসংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র ২০০৮ সাল থেকে সংগঠনের লক্ষ্য ও উদ্দেশ্য অনুযায়ী প্রতিষ্ঠাবার্ষিকী উদযাপনকে ঘিরে আবৃত্তির পরিব্যাপনের জন্য আয়োজন করছে আবৃত্তি উৎসব। আয়োজনের কার্যক্রমে বৈচিত্র থাকলেও অনেকটাই গতানুগতিক। কিন্তু মূল পার্থক্য গঠনগত ও গুণগত। সংগঠনের সকল সদস্যদের মমতায় নির্মিত অনুষ্ঠান যেন জীবন্ত সত্ত্বার মত সকলের প্রাণ স্পর্শ করে।\r\n\r\nআবৃত্তি উৎসব ২০১২\r\n\r\nসংবৃতা’র সপ্তম প্রতিষ্ঠাবর্ষে আয়োজন করা হচ্ছে দুই দিনব্যাপি পঞ্চম সংবৃতা আবৃত্তি উৎসব ২০১২। উৎসবে দেশবরেণ্য আবৃত্তি শিল্পীদের একক আবৃত্তি, বিভিন্ন সংগঠনের বৃন্দ প্রযোজনা এবং পূর্ণাঙ্গ প্রযোজনা পরিবেশিত হবে এবং প্রাথমিক ও মাধ্যমিক বিদ্যালয়, মহাবিদ্যালয়, বিশ্ববিদ্যালয় পর্যায় এর শিক্ষার্থীদের অংশগ্রহণে আবৃত্তি প্রতিযোগিতা অনুষ্ঠিত হবে। প্রতিযোগিতায় বিভিন্ন শিক্ষা প্রতিষ্ঠান থেকে সহস্রাধিক প্রতিযোগী অংশগ্রহন করবে।\r\n\r\nআবৃত্তি প্রতিযোগিতায় পদক প্রদান করা হবে এবং অংশগ্রহণকারী সকলকে পুরষ্কৃত করা হবে।\r\n\r\nক. শিশু পর্যায়- কবি শামসুর রাহমান পদক\r\n\r\nখ. বিদ্যালয় পর্যায়- শিল্পী ওয়াহিদুল হক পদক\r\n\r\nগ. মহাবিদ্যালয় পর্যায়- বাক্-শিল্পাচার্য নরেন বিশ্বাস পদক\r\n\r\nঘ. বিশ্ববিদ্যালয় পর্যায়- আবৃত্তিশিল্পী গোলাম মুস্তাফা পদক\r\n\r\nতারিখ\r\n\r\n১৩ ও ১৪ জুলাই ২০১২\r\n\r\nসকাল ৯টা থেকে রাত ৮টা।\r\n\r\nস্থান\r\n\r\nশওকত ওসমান স্মৃতি মিলনায়তন\r\n\r\nকেন্দ্রীয় গণগ্রন্থাগার, শাহবাগ, ঢাকা।', './images/hpa_image/Untitled-111.png', '2013-07-28', '2015-06-06 06:55:27');

-- --------------------------------------------------------

--
-- Table structure for table `honored_songbritan`
--

CREATE TABLE IF NOT EXISTS `honored_songbritan` (
`sl` int(4) NOT NULL,
  `upoma` varchar(40) NOT NULL,
  `full_name` varchar(60) NOT NULL,
  `father_name` varchar(60) NOT NULL,
  `mother_name` varchar(60) NOT NULL,
  `year` varchar(4) NOT NULL,
  `year_eng` int(4) NOT NULL,
  `address` varchar(140) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `academic` varchar(150) NOT NULL,
  `abortan` varchar(6) NOT NULL,
  `picture` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `honored_songbritan`
--

INSERT INTO `honored_songbritan` (`sl`, `upoma`, `full_name`, `father_name`, `mother_name`, `year`, `year_eng`, `address`, `mobile`, `academic`, `abortan`, `picture`) VALUES
(2, 'উৎসবে অনন্যা', 'সাদিয়া সাইদ', 'সাইদুর রহমান', 'নাসরিন রহমান', '২০১৪', 2014, 'কলসিফুকরা, কাশিয়ানি, গোপালগঞ্জ', '০১৬৮৬৩০৫৫০৬', '৩য় বর্ষ, হিসাব বিজ্ঞান, ইডেন মহিলা কলেজ', '0', './images/honored_songbritan/doha.png'),
(3, 'উৎসবে অনন্যা', 'মোঃ মনিরুজ্জামান', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', '২০১৪', 2014, 'কলসিফুকরা, কাশিয়ানি, গোপালগঞ্জ', '০১৭১৯২৭৯৩৮', '৩য় বর্ষ, হিসাব বিজ্ঞান, ইডেন মহিলা কলেজ', '২২তম', './images/honored_songbritan/doha1.png'),
(4, 'উৎসবে অনন্যা', 'সাদিয়া সাইদ', 'সাইদুর রহমান', 'নাসরিন রহমান', '২০১৫', 2015, 'কলসিফুকরা, কাশিয়ানি, গোপালগঞ্জ', '০১৯৩৭৮০৭৫৯৪', '৩য় বর্ষ, হিসাব বিজ্ঞান, ইডেন মহিলা কলেজ', '২৫', './images/honored_songbritan/doha2.png'),
(5, 'উৎসবে অনন্যা', 'সাদিয়া সাইদ', 'সাইদুর রহমান', 'নাসরিন রহমান', '২০১৩', 2013, 'কলসিফুকরা, কাশিয়ানি, গোপালগঞ্জ', '০১৯৩৭৮০৭৫৯৪', '৩য় বর্ষ, হিসাব বিজ্ঞান, ইডেন মহিলা কলেজ', '২৫', './images/honored_songbritan/doha3.png'),
(6, 'উৎসবে অনন্যা', 'সাদিয়া সাইদ', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', '২০১৬', 2016, 'কলসিফুকরা, কাশিয়ানি, গোপালগঞ্জ', '০১৭৭৪৯২৫৫৫১', '৩য় বর্ষ, হিসাব বিজ্ঞান, ইডেন মহিলা কলেজ', '২৫', './images/honored_songbritan/doha4.png');

-- --------------------------------------------------------

--
-- Table structure for table `online_application`
--

CREATE TABLE IF NOT EXISTS `online_application` (
`sl` int(4) NOT NULL,
  `abortan` int(4) NOT NULL,
  `full_name` varchar(70) NOT NULL,
  `father_name` varchar(70) NOT NULL,
  `mother_name` varchar(70) NOT NULL,
  `gender` varchar(5) NOT NULL,
  `permanent_address` varchar(250) NOT NULL,
  `present_address` varchar(250) NOT NULL,
  `birth_place` varchar(25) NOT NULL,
  `date_of_birth` varchar(10) NOT NULL,
  `blood_group` varchar(4) NOT NULL,
  `mobile_number` varchar(11) NOT NULL,
  `email_address` varchar(35) NOT NULL,
  `academic_qualification` varchar(30) NOT NULL,
  `institution` varchar(70) NOT NULL,
  `other_experience` varchar(20) NOT NULL,
  `photograph` int(11) NOT NULL,
  `heard_from` varchar(40) NOT NULL,
  `apllication_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online_application`
--

INSERT INTO `online_application` (`sl`, `abortan`, `full_name`, `father_name`, `mother_name`, `gender`, `permanent_address`, `present_address`, `birth_place`, `date_of_birth`, `blood_group`, `mobile_number`, `email_address`, `academic_qualification`, `institution`, `other_experience`, `photograph`, `heard_from`, `apllication_date`) VALUES
(2, 0, 'মোঃ মনিরুজ্জামান', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', 'male', 'গ্রাম- বিজয়পুর, ডাকঘর- বাংলা, উপজেলা- নেত্রকোনা, জেলা- নেত্রকোনা', 'গ্রাম- বিজয়পুর, ডাকঘর- বাংলা, উপজেলা- নেত্রকোনা, জেলা- নেত্রকোনা', 'নেত্রকোনা', '০৪-০১-১৯৮৯', 'এ-', '০১৯৩৭৮০৭৫৯৪', 'sheikh.autb@gmail.com', 'বিএ', 'বাংলাদেশ পরমাণু শক্তি কমিশন', 'আবৃত্তি', 0, 'বন্ধুর কাছে', '0000-00-00 00:00:00'),
(3, 25, 'মোঃ মনিরুজ্জামান', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', 'male', 'fxxfx', 'jcj, nm ', 'b gvmb', '886', 'বি+', '64564576586', 'acbd@gmail.com', 'gcg vnv', 'nvnccnb nbn', 'গান', 0, 'পত্রিকার মাধ্যমে', '0000-00-00 00:00:00'),
(4, 0, 'asjfasl', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', 'male', 'asdfa', 'sdf', 'asfsf', '০৪-০১-১৯৮৯', 'এ+', '0950858', 'sheikh.autb@gmail.com', 'gcg vnv', 'বাংলাদেশ পরমাণু শক্তি কমিশন', 'নৃত্য', 0, 'ওয়েব সাইটের মাধ্যমে', '2015-05-16 05:28:04'),
(5, 0, 'asjfasl', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', 'male', 'asdfa', 'sdf', 'asfsf', '০৪-০১-১৯৮৯', 'এ+', '0950858', 'sheikh.autb@gmail.com', 'gcg vnv', 'বাংলাদেশ পরমাণু শক্তি কমিশন', 'নৃত্য', 0, 'ওয়েব সাইটের মাধ্যমে', '2015-05-16 05:31:47'),
(6, 0, 'asjfasl', 'মোঃ আবু সিদ্দিক', 'খোদেজা আক্তার', 'femal', 'asfdsaf', 'fsfdsafs', 'b gvmb', '০৪-০১-১৯৮৯', 'এ-', '0950858', 'sheikh.autb@gmail.com', 'gcg vnv', 'বাংলাদেশ পরমাণু শক্তি কমিশন', 'গান', 0, 'বন্ধুর কাছে', '2015-05-16 05:32:30'),
(7, 0, 'asjfasl', 'sfjsf', 'খোদেজা আক্তার', 'male', 'fsd', 'asfd', 'asfsf', '০৪-০১-১৯৮৯', 'এ+', '0950858', 'sheikh.autb@gmail.com', 'gcg vnv', 'বাংলাদেশ পরমাণু শক্তি কমিশন', 'গান', 0, 'বন্ধুর কাছে', '2015-05-16 05:34:22'),
(8, 56, 'rtyu', 'tyu', 'tyui', 'male', 'tyui', 'dfghj', 'dfghj', 'dfghj', 'এ-', 'fghjk', 'sheikh.autb@gmail.com', 'rtyu', 'বাংলাদেশ পরমাণু শক্তি কমিশন', 'গান', 0, 'পত্রিকার মাধ্যমে', '2015-05-16 11:56:47');

-- --------------------------------------------------------

--
-- Table structure for table `picture_gallery`
--

CREATE TABLE IF NOT EXISTS `picture_gallery` (
`sl` int(4) NOT NULL,
  `picture` varchar(500) NOT NULL,
  `picture_thumb` varchar(500) NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `picture_gallery`
--

INSERT INTO `picture_gallery` (`sl`, `picture`, `picture_thumb`, `upload_date`) VALUES
(13, './gallery_asset/img/PictureGalleryPicture01.png', './gallery_asset/img/PictureGalleryThumbnail01.png', '2015-05-30 07:03:58'),
(14, './gallery_asset/img/PictureGalleryPicture02.png', './gallery_asset/img/PictureGalleryThumbnail02.png', '2015-05-30 07:04:06'),
(15, './gallery_asset/img/PictureGalleryPicture03.png', './gallery_asset/img/PictureGalleryThumbnail03.png', '2015-05-30 07:04:15'),
(16, './gallery_asset/img/PictureGalleryPicture04.png', './gallery_asset/img/PictureGalleryThumbnail04.png', '2015-05-30 07:05:04'),
(17, './gallery_asset/img/PictureGalleryPicture05.png', './gallery_asset/img/PictureGalleryThumbnail05.png', '2015-05-30 07:05:18'),
(18, './gallery_asset/img/PictureGalleryPicture06.png', './gallery_asset/img/PictureGalleryThumbnail06.png', '2015-05-30 07:05:25'),
(19, './gallery_asset/img/PictureGalleryPicture07.png', './gallery_asset/img/PictureGalleryThumbnail07.png', '2015-05-30 07:06:10'),
(20, './gallery_asset/img/PictureGalleryPicture08.png', './gallery_asset/img/PictureGalleryThumbnail08.png', '2015-05-30 07:06:40'),
(21, './gallery_asset/img/PictureGalleryPicture09.png', './gallery_asset/img/PictureGalleryThumbnail09.png', '2015-05-30 07:07:01'),
(22, './gallery_asset/img/PictureGalleryPicture10.png', './gallery_asset/img/PictureGalleryThumbnail10.png', '2015-05-30 07:07:09'),
(23, './gallery_asset/img/PictureGalleryPicture11.png', './gallery_asset/img/PictureGalleryThumbnail11.png', '2015-05-30 07:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(2) NOT NULL,
  `admin_full_name` varchar(50) NOT NULL,
  `admin_email_address` varchar(50) NOT NULL,
  `admin_password` varchar(32) NOT NULL,
  `access_level` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_full_name`, `admin_email_address`, `admin_password`, `access_level`) VALUES
(1, 'Md. Maniruzzaman', 'sheikh.autb@gmail.com', '4e3858b8cfa8d5cfcc051ddc7d91d655', 1),
(2, 'Md. Maniruzzaman', 'monir_ntk@yahoo.com', '96e79218965eb72c92a549dd5a330112', 2),
(3, 'Faruq Mojumder', 'faruqmojumder@gmail.com', 'b61c28a3777e27c4ec45b412fd6dde79', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certified_songbritan`
--
ALTER TABLE `certified_songbritan`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `executive_committe`
--
ALTER TABLE `executive_committe`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `home_article`
--
ALTER TABLE `home_article`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `honored_songbritan`
--
ALTER TABLE `honored_songbritan`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `online_application`
--
ALTER TABLE `online_application`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `picture_gallery`
--
ALTER TABLE `picture_gallery`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certified_songbritan`
--
ALTER TABLE `certified_songbritan`
MODIFY `sl` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `executive_committe`
--
ALTER TABLE `executive_committe`
MODIFY `sl` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `home_article`
--
ALTER TABLE `home_article`
MODIFY `sl` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `honored_songbritan`
--
ALTER TABLE `honored_songbritan`
MODIFY `sl` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `online_application`
--
ALTER TABLE `online_application`
MODIFY `sl` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `picture_gallery`
--
ALTER TABLE `picture_gallery`
MODIFY `sl` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
