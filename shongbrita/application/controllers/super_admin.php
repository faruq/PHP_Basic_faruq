<?php
session_start();
class Super_Admin extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==NULL)
        {
            redirect('admin', 'refresh');
        }
        
        $this->load->model('super_admin_model');
    }
    
    public function index()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/dashboard_contents', '', true);
        $this->load->view('admin/dashboard', $data);
    }
    
    
    public function home_page_article()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/home_page_article', '', true);
        $this->load->view('admin/dashboard', $data);
    }
    
    public function save_home_page_article()
    {
       $data=array();
       $data['article_heading']=$this->input->post('article_heading');
       $data['full_article']=$this->input->post('full_article');
       $data['date']=$this->input->post('date');
       
       //File Upload Start
       $config['upload_path'] = './images/hpa_image/';
       $config['allowed_types'] = 'gif|jpg|png';
       $config['max_size'] = '1000';
       $config['max_width'] = '1024';
       $config['max_height'] = '768';

       $this->load->library('upload', $config);
       $this->upload->initialize($config);
       
       $error='';
       $file_data=array();

        if (!$this->upload->do_upload('picture')) {
            $error= $this->upload->display_errors();
            $sdata=array();
            $sdata['msg']=$error;
            $this->session->set_userdata($sdata);
            redirect('super_admin/home_page_article');
        } 
        else {
            $file_data = $this->upload->data();
            $data['picture']=$config['upload_path'].$file_data['file_name'];
        }

        //File upload end
       
       
       $this->super_admin_model->save_home_page_article_db($data);
       $sdata=array();
       $sdata['msg']='Your Home Page Article saved successfully';
       $this->session->set_userdata($sdata);
       redirect('super_admin/home_page_article');
    }
    
    public function create_admin()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/create_admin', '', true);
        $this->load->view('admin/dashboard', $data);
    }
    
    public function create_admin_data()
    {
        $data=array();
        $data['admin_full_name']=$this->input->POST('admin_full_name');
        $data['admin_email_address']=$this->input->POST('admin_email_address');
        $password=$this->input->POST('admin_password');
        $data['admin_password']=md5($password);
        $data['access_level']=$this->input->POST('access_level');
        
        //INSERT INTO `shongbrita`.`tbl_admin` (`admin_id`, `admin_full_name`, `admin_email_address`, 
        //`admin_password`, `access_level`) VALUES (NULL, 'Faruq Mojumder', 'farqumojumder@gmail.com', MD5('faruq19'), '0');
        
        $this->super_admin_model->create_admin_data_db($data);
        $sdata=array();
        $sdata['msg']='Your Data Saved Successfully';
        $this->session->set_userdata($sdata);
        redirect('super_admin/create_admin');
    }

    public function certified_shongbritan()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/certified_shongbritan', '', true);
        $this->load->view('admin/dashboard', $data);
    }
    
    

    
    public function certified_shongbritan_db()
    {
        $data=array();
        $data['name']=$this->input->POST('name');
        $data['abortan']=$this->input->POST('abortan');
        $data['abortan_eng']=$this->input->POST('abortan_eng');
        $data['position']=$this->input->POST('position');
        $data['mobile']=$this->input->POST('mobile');
        $this->super_admin_model->certified_shongbritan_db($data);
        $sdata=array();
        $sdata['msg']='Your Data Saved Successfully';
        $this->session->set_userdata($sdata);
        redirect('super_admin/certified_shongbritan');
    }
    
    public function executive_committee()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/executive_committee', '', true);
        $this->load->view('admin/dashboard', $data);
    }
    
    public function executive_committee_db()
    {
        $data=array();
        $data['designation']=$this->input->POST('designation');
        $data['name']=$this->input->POST('name');
        $data['mobile']=$this->input->POST('mobile');
        $this->super_admin_model->executive_committee_db($data);
        $sdata=array();
        $sdata['msg']='Your Data Saved Successfully';
        $this->session->set_userdata($sdata);
        redirect('super_admin/executive_committee');
    }
    
    public function picture_gallery()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/picture_gallery', '', true);
        $this->load->view('admin/dashboard', $data);
    }
    
        
    public function picture_gallery_upload()
    {
       $data=array();
              
       //File Upload Start
       $config['upload_path'] = './gallery_asset/img/';
       $config['allowed_types'] = 'gif|jpg|png';
       $config['max_size'] = '1000';
       $config['max_width'] = '1024';
       $config['max_height'] = '768';

       $this->load->library('upload', $config);
       $this->upload->initialize($config);
       
       $error='';
       $file_data=array();

        if (!$this->upload->do_upload('picture')) {
            $error= $this->upload->display_errors();
            $sdata=array();
            $sdata['msg']=$error;
            $this->session->set_userdata($sdata);
            redirect('super_admin/picture_gallery');
        } 
        else {
            $file_data = $this->upload->data();
            $data['picture']=$config['upload_path'].$file_data['file_name'];
          
        }

        if (!$this->upload->do_upload('picture_thumb')) {
            $error= $this->upload->display_errors();
            $sdata=array();
            $sdata['msg']=$error;
            $this->session->set_userdata($sdata);
            redirect('super_admin/picture_gallery');
        } 
        else {
            $file_data = $this->upload->data();
            $data['picture_thumb']=$config['upload_path'].$file_data['file_name'];
        }
        //File upload end
       
       
       $this->super_admin_model->picture_gallery_upload_db($data);
       $sdata=array();
       $sdata['msg']='Your Image Uploaded successfully';
       $this->session->set_userdata($sdata);
       redirect('super_admin/picture_gallery');
    }
  
    public function honored_songbritan()
    {
        $data=array();
        $data['dashboard_contents']=$this->load->view('admin/honored_songbritan', '', true);
        $this->load->view('admin/dashboard', $data);
    }

    public function save_honored_songbritan()
    {
        $data=array();
        $data['upoma']=$this->input->POST('upoma');
        $data['full_name']=$this->input->POST('full_name');
        $data['father_name']=$this->input->POST('father_name');
        $data['mother_name']=$this->input->POST('mother_name');
        $data['year']=$this->input->POST('year');
        $data['year_eng']=$this->input->POST('year_eng');
        $data['address']=$this->input->POST('address');
        $data['mobile']=$this->input->POST('mobile');
        $data['academic']=$this->input->POST('academic');
        $data['abortan']=$this->input->POST('abortan');
        
        //File Upload Start
       $config['upload_path'] = './images/honored_songbritan/';
       $config['allowed_types'] = 'gif|jpg|png';
       $config['max_size'] = '1000';
       $config['max_width'] = '1024';
       $config['max_height'] = '768';

       $this->load->library('upload', $config);
       $this->upload->initialize($config);
       
       $error='';
       $file_data=array();

        if (!$this->upload->do_upload('picture')) {
            $error= $this->upload->display_errors();
            $sdata=array();
            $sdata['msg']=$error;
            $this->session->set_userdata($sdata);
            redirect('super_admin/honored_songbritan');
        } 
        else {
            $file_data = $this->upload->data();
            $data['picture']=$config['upload_path'].$file_data['file_name'];
          
        }

       //File upload end
        
        $this->super_admin_model->save_honored_songbritan_db($data);
        $sdata=array();
        $sdata['msg']='Your Data Saved Successfully';
        $this->session->set_userdata($sdata);
        redirect('super_admin/honored_songbritan');
    }
    
    public function logout()
    {
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_full_name');
        $sdata=array();
        $sdata['message']='You are successfully logout';
        $this->session->set_userdata($sdata);
        redirect('admin');
    }
}
