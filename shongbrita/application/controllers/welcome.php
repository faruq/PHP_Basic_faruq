<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
            $data=array();
            $data['slider']=true;
            $data['side_bar']=true;
            $data['title']='সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র';
            
             //----------
            $this->load->library('pagination');

            $config['base_url'] = base_url().'welcome/index/';
            $config['total_rows'] = $this->db->count_all('home_article');
            $config['per_page'] = 3;

            $this->pagination->initialize($config);
                 
                       
            $data['select_article']=$this->welcome_model->select_article($config['per_page'], $this->uri->segment(3));
            $data['master_content']=$this->load->view('master_content', $data, true);
            $this->load->view('master', $data);
	}
        
	public function porichoy()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='পরিচয়';
            $data['master_content']=$this->load->view('porichoy', '', true);
            $this->load->view('master', $data);
	}        
        
	public function karmoshala()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='কর্মশালা';
            $data['master_content']=$this->load->view('karmoshala', '', true);
            $this->load->view('master', $data);
	}  

	public function utshob()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='উৎসব';
            $data['master_content']=$this->load->view('utshob', '', true);
            $this->load->view('master', $data);
	}  
        
	public function picture_gallery()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=false;
            $data['title']='ছবির গ্যালারি';
            $data['select_picture']=$this->welcome_model->select_picture();
            $data['master_content']=$this->load->view('picture_gallery', $data, true);
            $this->load->view('master', $data);
	}
        
	public function contact()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='যোগাযোগ';
            $data['master_content']=$this->load->view('contact', '', true);
            $this->load->view('master', $data);
	}
        
        public function karmosala_message()
        {
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='যোগাযোগ';
            $data['master_content']=$this->load->view('karmoshala_message', '', true);
            $this->load->view('master', $data);
        }

        public function online_application()
        {
            $data=array();
            $data['abortan']=$this->input->POST('abortan');
            $data['full_name']=$this->input->POST('full_name');
            $data['father_name']=$this->input->POST('father_name');
            $data['mother_name']=$this->input->POST('mother_name');
            $data['gender']=$this->input->POST('gender');
            $data['permanent_address']=$this->input->POST('permanent_address');
            $data['present_address']=$this->input->POST('present_address');
            $data['birth_place']=$this->input->POST('birth_place');
            $data['date_of_birth']=$this->input->POST('date_of_birth');
            $data['blood_group']=$this->input->POST('blood_group');
            $data['mobile_number']=$this->input->POST('mobile_number');
            $data['email_address']=$this->input->POST('email_address');
            $data['academic_qualification']=$this->input->POST('academic_qualification');
            $data['institution']=$this->input->POST('institution');
            $data['other_experience']=$this->input->POST('other_experience');
            $data['photograph']=$this->input->POST('photograph');
            $data['heard_from']=$this->input->POST('heard_from');
            $this->welcome_model->online_aplication_db($data);            
            redirect('welcome/karmosala_message');                   
        }
        
        public function certified_songbritan()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='সনদপ্রাপ্ত সংবৃতান';
            //----------
            $this->load->library('pagination');

            $config['base_url'] = base_url().'welcome/certified_songbritan/';
            $config['total_rows'] = $this->db->count_all('certified_songbritan');
            $config['per_page'] = 20;

            $this->pagination->initialize($config);

            $data['certified_songbritan']=$this->welcome_model->certified_songbritan_db($config['per_page'], $this->uri->segment(3));
            
            //--------------
            $data['master_content']=$this->load->view('certified_songbritan', $data, true);
            $this->load->view('master', $data);
	}

        public function honored_songbritan()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='পদকপ্রাপ্ত সংবৃতান';
            $data['honored_songbritan']=$this->welcome_model->honored_songbritan_db();
            $data['master_content']=$this->load->view('honored_songbritan', $data, true);
            $this->load->view('master', $data);
	}
        
        public function permanent_council()
	{
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='স্থায়ী পরিষদ';
            $data['master_content']=$this->load->view('permanent_council', '', true);
            $this->load->view('master', $data);
	}
        
        public function founding_member()
        {
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='প্রতিষ্ঠাতা সদস্য';
            $data['master_content']=$this->load->view('founding_member', '', true);
            $this->load->view('master', $data);
        }

        public function complete_production()
        {
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='পূর্ণাঙ্গ প্রযোজনা';
            $data['complete_production']=$this->welcome_model->complete_production();
            $data['master_content']=$this->load->view('complete_production', $data, true);
            $this->load->view('master', $data);
        }
        
        public function executive_body()
        {
            $data=array();
            $data['slider']=false;
            $data['side_bar']=true;
            $data['title']='নির্বাহী পরিষদ';
            $data['executive_body']=$this->welcome_model->executive_body();
            $data['master_content']=$this->load->view('executive_body', $data, true);
            $this->load->view('master', $data);
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */