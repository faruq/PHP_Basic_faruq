<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>সংবৃতা | <?php echo $title; ?></title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta content="Author" description="Md. Maniruzzaman, Md. Faruk Majumder" />
        <meta name="description" content=""/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!-- CSS LINKS-->
        <link href="<?php echo base_url() ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url() ?>css/pages.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url() ?>css/coin-slider.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url() ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>

        <!-- JAVASCRIPT LINKS-->
        <script type="text/javascript" src="<?php echo base_url() ?>js/cufon-yui.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/cufon-aller.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/script.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/coin-slider.min.js"></script>

        <!-- FAV_ICON -->
        <link rel="shortcut icon" href="images/ico/favicon.ico"/>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png"/>
    </head>


    <body>
        
        <div id="fb-root"></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557545eb339c3393" async="async"></script>


        <div class="main">
            <div class="header">
                <div class="header_resize">



                    <div class="menu_nav"> <!-- MENU-->

                        <nav id="primary_nav_wrap">
                            <ul>
                                <li class="current-menu-item"><li class="active"><a href="<?php echo base_url() ?>welcome/index.html"><span>প্রথম পাতা</span></a></li></li>
                                <li><a href="<?php echo base_url() ?>welcome/porichoy.html"><span>পরিচয়</span></a>
                                    <ul>
                                        <li><a href="<?php echo base_url() ?>welcome/founding_member.html"><span>প্রতিষ্ঠাতা সদস্য</span></a></li>
                                        <li><a href="<?php echo base_url() ?>welcome/permanent_council.html"><span>স্থায়ী পরিষদ</span></a></li>
                                        <li><a href="<?php echo base_url() ?>welcome/executive_body.html"><span>নির্বাহী পরিষদ</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url() ?>welcome/karmoshala.html"><span>কর্মশালা</span></a></li>
                                <li><a href="<?php echo base_url() ?>welcome/utshob.html"><span>উৎসব</span></a></li>
                                <li><a href="<?php echo base_url() ?>welcome/utshob.html"><span>প্রযোজনা</span></a>
                                    <ul>
                                    <li><a href="<?php echo base_url() ?>welcome/complete_production.html"><span>পূর্ণাঙ্গ প্রযোজনা</span></a></li>
                                    <li><a href="<?php echo base_url() ?>welcome/founding_member.html"><span>বৃন্দ প্রযোজনা</span></a></li>
                                    <li><a href="<?php echo base_url() ?>welcome/founding_member.html"><span>বিশেষ প্রযোজনা</span></a></li>
                                    
                                    </ul>

                                </li>
                                
                                <li><a href="<?php echo base_url() ?>welcome/picture_gallery.html"><span>ছবির গ্যালারি</span></a></li>
                                <li><a href="<?php echo base_url() ?>welcome/contact.html"><span>যোগাযোগ</span></a></li> 
                            </ul>
                        </nav>
                    </div>

                    <div class="clr"></div>
                    <div class="banner"><!-- BANNER -->
                        <a href="<?php echo base_url() ?>welcome/index.html"><img src="<?php echo base_url() ?>images/banner.png" height="200px" width="960px"/></a>
                    </div>
                    <div class="marquee"> <!-- MARQUEE-->
                        <div class="marquee_title">বিশেষ খবর:</div>
                        <div class="marquee_item"><marquee> &uArr; <a href="#">ভর্তি চলছে ভর্তি চলছে ভর্তি চলছে </a>&uArr; ভর্তি চলছে ভর্তি চলছে ভর্তি চলছে &uArr; ভর্তি চলছে ভর্তি চলছে ভর্তি চলছে</marquee></div>
                    </div>

                    <?php
                    if ($slider) {
                        ?>

                        <div class="slider"><!-- SLIDER -->
                            <div id="coin-slider"> 
                                <a href="#"><img src="<?php echo base_url() ?>images/slide1.jpg" width="960" height="307" alt="" /> </a> 
                                <a href="#"><img src="<?php echo base_url() ?>images/slide2.jpg" width="960" height="307" alt="" /> </a> 
                                <a href="#"><img src="<?php echo base_url() ?>images/slide3.jpg" width="960" height="307" alt="" /> </a> 
                                <a href="#"><img src="<?php echo base_url() ?>images/slide4.jpg" width="960" height="307" alt="" /> </a> 
                                <a href="#"><img src="<?php echo base_url() ?>images/slide5.jpg" width="960" height="307" alt="" /> </a> 
                            </div>
                        </div>
                    <?php } ?>

                    <div class="clr"></div>

                </div>
            </div>

            <?php echo $master_content; ?>

            <?php
            if ($side_bar) {
                ?> 
                <div class="sidebar"><!-- SIDE_BAR -->

                    <div class="clr"></div>

                    <div class="sidebar_headings">সাম্প্রতিক</div><!-- SIDE_BAR-1 -->
                    <div class="gadget">            
                        <div class="clr"></div>
                        <ul class="sb_menu">
                            <li><a href="#">সর্বশেষ আবর্তনের বিস্তারিত...</a></li><hr/>
                            <li><a href="#">উৎসবের তথ্য</a></li><hr/>
                            <li><a href="#">এখানে নোটিশ যাবে</a></li><hr/>
                            <li><a href="#">এখানে নোটিশ যাবে</a></li><hr/>
                            <li><a href="#">এখানে নোটিশ যাবে</a></li><hr/>
                            <li><a href="#"></a>....</li><hr/>
                        </ul>
                    </div>

                    <div class="sidebar_headings">আরও যা থাকছে</div><!-- SIDE_BAR-2 -->
                    <div class="gadget">
                        <div class="clr"></div>
                        <ul class="ex_menu">
                            <li><a href="#">কবিতাসমূহ</a></li><hr/>
                            <li><a href="#">অডিও কবিতা</a></li><hr/>
                            <li><a href="#">ভিডিও কবিতা</a></li><hr/>
                            <li><a href="<?php echo base_url() ?>welcome/honored_songbritan">পদকপ্রাপ্ত সংবৃতানদের তালিকা</a></li><hr/>
                            <li><a href="<?php echo base_url() ?>welcome/certified_songbritan">সনদপ্রাপ্ত সংবৃতানদের তালিকা</a></li><hr/>            
                        </ul>
                    </div>
                    
                                        <div class="sidebar_headings">ফেসবুক পেজ</div><!-- SIDE_BAR-2 -->
                    <div class="NOTNEEDgadget">
                        <div class="clr"></div>
                        <div class="fb-page" data-href="https://www.facebook.com/shongbrita" data-width="260" data-height="500" data-hide-cover="true" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/shongbrita"><a href="https://www.facebook.com/shongbrita">সংবৃতা</a></blockquote></div></div>          
                       <hr/>
                    </div>

                </div>
<?php } ?>
            <div class="clr"></div>
        </div>
        </div>

        <div class="fbg"><!-- FOOTER -->
            <div class="fbg_resize">

                <div class="col c2">
                    <div class="headings">
                        আমাদের কার্যক্রমসমূহ
                    </div>
                    <ul class="fbg_ul">
                        <li>বাংলা প্রমিত উচ্চারণ</li>       
                        <li>শুদ্ধ পাঠ</li>
                        <li>সাংগঠনিক আবৃত্তি চর্চা</li>
                        <li>সাবলীল উপস্থাপনা</li>
                    </ul>
                </div>
                <div class="col c3">
                    <div class="headings">যোগাযোগ</div>
                    <p>সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র</p>
                    <p class="contact_info"> 
                        <span>ঠিকানা:</span> টিএসসি, ঢাকা বিশ্ববিদ্যালয়<br />
                        <span>মোবাইল:</span> ০১৭১১ ২২২৬৯৯ | ০১৭১১ ১৮৯৩৫৯ | ০১৬৮৬ ৭৯৭০১০<br />
                        <span>ই-মেইল:</span>info@shongbrita.com</p>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="footer">
            <div class="footer_resize">
                <p class="lf"> কপিরাইট &copy; <a href="#"> www.songbrita.com</a> &nbsp; &nbsp; <span class="color">|</span> &nbsp;&nbsp; <a href="#" target="_blank">ফেসবুক পেইজ</a> &nbsp; &nbsp; <span class="color">|</span> &nbsp; &nbsp; সাইটটি তৈরি করেছেন <a href="https://www.facebook.com/native.moniruzzaman" target="_blank"> মোঃ মনিরুজ্জামান </a> ও <a href="#" target="_blank"> মোঃ ফারুক মজুমদার </a></p>
                <div style="clear:both;"></div>
            </div>
        </div>
        </div>

    </body>
</html>
