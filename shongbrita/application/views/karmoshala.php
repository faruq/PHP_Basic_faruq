
<div class="content">
    <div class="content_resize">
        <div class="mainbar">
            <div class="article">


                <div class="page_heading"> 
                    <p align="center">কর্মশালা</p>               
                </div>
                <div>প্রতিষ্ঠালগ্ন থেকেই সংবৃতা কর্মশালার মাধ্যমে আবৃত্তি চর্চা ও বিকাশের প্রয়াস চালিয়ে আসছে।
                    কর্মশালায় প্রমিত উচ্চারণ, বাচন ভঙ্গি, উপস্থাপনা, উচ্চারণ সৌন্দর্য, নন্দন তথ্য ইত্যাদি বিষয়ে দেশ বরেণ্য 
                    প্রশিক্ষকদের দিয়ে ক্লাশ পরিচালনা করে থাকে। প্রতি চার মাস অন্তর একটি কর্মশালা অনুষ্ঠিত হয়। চার মাস ব্যাপ্তীর এ কর্মশালায়।
                    <p class="text_color_underline">অনলাইন ফরম পূরণের নিয়মাবলি:</p>               
                    * প্রাথমিক আবর্তন ৪ (চার) মাস ব্যাপী।<br/>
                    * সকল তথ্য বাংলায় (ইউনিকোড)পূরণ করুন।<br/>                
                    * সকল তথ্য বাংলায় (ইউনিকোড)পূরণ করুন।<br/>                
                    <hr/>	

                </div>
                <div>
                    <form action="<?php echo base_url(); ?>welcome/online_application" method="POST">
                        <div class="input_lebel">কত তম আবর্তন</div>
                        <div class="input_box"><input type="text" name="abortan" size="80" placeholder="কত তম আবর্তনের জন্য আবেদন করছেন। যেমন- ২৪" required/></div>

                        <div class="input_lebel">নামঃ</div>
                        <div class="input_box"><input type="text" name="full_name" size="80" placeholder="নিজের পূর্ণ নাম লিখুন" required/></div>

                        <div>পিতা/স্বামীর নামঃ</div>
                        <div class="input_box"><input type="text" name="father_name" size="80" placeholder="পিতার নাম লিখুন" required/></div>

                        <div>মাতার নামঃ</div>
                        <div class="input_box"><input type="text" name="mother_name" size="80" placeholder="মাতার নাম লিখুন" required/></div>

                        <div>লিঙ্গঃ</div>
                        <div class="input_box">
                            <input type="radio" name="gender" value="male" />ছেলে
                            <input type="radio" name="gender" value="female" />মেয়ে
                        </div>

                        <div>স্থায়ী ঠিকানাঃ</div>
                        <div class="input_box"><textarea name="permanent_address" rows="5" cols="77" placeholder="স্থায়ী ঠিকানা লিখুন" required></textarea></div>

                        <div>বর্তমান ঠিকানাঃ</div>
                        <div class="input_box"><textarea name="present_address" rows="5" required cols="77" placeholder="বর্তমান ঠিকানা লিখুন" required></textarea></div>

                        <div>জন্মস্থানঃ</div>
                        <div class="input_box"><input type="text" name="birth_place" size="80" placeholder="জন্মস্থান লিখুন"/></div>

                        <div>জন্ম তারিখঃ</div>
                        <div class="input_box"><input type="text" name="date_of_birth" size="80" required/></div>

                        <div>রক্তের গ্রুপঃ</div>
                        <div class="input_box"><select name="blood_group" required>
                                <option value="">বাছাই করুন</option>
                                <option value="এ+">এ+</option>
                                <option value="এ-">এ-</option>
                                <option value="বি+">বি+</option>
                                <option value="বি-">বি-</option>				
                                <option value="এবি+">এবি+</option>
                                <option value="এবি-">এবি-</option>
                                <option value="ও+">ও+</option>
                                <option value="ও-">ও-</option>
                            </select></div>

                        <div>মোবাইল নাম্বারঃ</div>
                        <div class="input_box"><input type="text" name="mobile_number" size="80" placeholder="মোবাইল নাম্বার লিখুন" required/></div>

                        <div>ই-মেইলঃ</div>
                        <div class="input_box"><input type="email" name="email_address" size="80" placeholder="ই-মেইল লিখুন"/></div>

                        <div>সর্বশেষ শিক্ষাগত যোগ্যতাঃ</div>
                        <div class="input_box"><input type="text" name="academic_qualification" size="80" placeholder="শিক্ষাগত যোগ্যতা লিখুন" required/></div>

                        <div>শিক্ষা পতিষ্ঠান/কর্ম স্থলঃ</div>
                        <div class="input_box"><input type="text" name="institution" size="80" placeholder="শিক্ষা পতিষ্ঠান/কর্ম স্থল লিখুন" required/></div>

                        <div>অন্যান্য ক্ষেত্রে অভিজ্ঞতাঃ</div>
                        <div class="input_box">
                            
                            <input type="checkbox" name="other_experience" value="আবৃত্তি"/> আবৃত্তি
                            <input type="checkbox" name="other_experience" value="গান" /> গান
                            <input type="checkbox" name="other_experience" value="গান" /> নাটক 
                            <input type="checkbox" name="other_experience" value="নৃত্য" /> নৃত্য
                            <input type="checkbox" name="other_experience" value="বাদ্যযন্ত্র" /> বাদ্যযন্ত্র
                        </div>

                        <div>ছবি (পাসপোর্ট সাইজ) </div>
                        <div class="input_box"><input type="file" name="photograph" placeholder="ছবি বাছাই করুন" required/></div>

                        <div>সংবৃতা  সম্পর্কে কোথা থেকে শুনেছেন?</div>
                        <div class="input_box"><select name="heard_from">
                                <option value="">বাছাই করুন</option>
                                <option value="বন্ধুর কাছে">বন্ধুর কাছে</option>
                                <option value="পত্রিকার মাধ্যমে"> পত্রিকার মাধ্যমে</option>
                                <option value="ওয়েব সাইটের মাধ্যমে">ওয়েব সাইটের মাধ্যমে</option>
                                <option value="পোস্টারের মাধ্যমে">পোস্টারের মাধ্যমে</option>			 	
                                <option value="অন্যান্য ভাবে">অন্যান্য ভাবে</option>
                            </select></div>

                        <hr/>
                        <div>সংবৃতার নিয়মাবলি মেনে রাজি থাকলে ফরমটি জমাদানের জন্য “আবেদন করুন” বাটনে ক্লিক করুন </div><div></div>
                        <hr/>

                        <div class="submit_button">
                            <input class="button" type="submit" name="submit" value="আবেদন করুন"/> &nbsp; &nbsp; &nbsp; &nbsp;
                            <input class="button" type="reset" name="reset" value="মুছে ফেলুন"/>
                        </div>

                    </form>
                </div>


            </div>

        </div>