
<div class="content">
    <div class="content_resize">
        <div class="mainbar">
            <div class="article">

                <div class="page_heading"> 
                    <p align="center">স্থায়ী পরিষদ</p>               
                </div>

                <div class="each_member">
                    <div class="members_image"><img src="<?php echo base_url(); ?>doha.png"/></div>
                    <div class="members_details">
                        <strong> একেএম সামছুদ্দোহা</strong><br/>
                        সভাপতি<br/>
                        জাতীয় গৃহায়ন কর্তৃপক্ষ<br/>
                        গণপূর্ত মন্ত্রণালয়, গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br/>
                        মোবাইল: ০১৭১১-২২২৬৯৯, ০১৯১৬-৪২৭২৮০

                    </div>
                </div>

                <div class="each_member">
                    <div class="members_image"><img src="<?php echo base_url(); ?>babu.png"/></div>
                    <div class="members_details">
                        <strong>  মোঃ সামসুজ্জামান বাবু </strong><br/>
                        সাধারণ সম্পাদক<br/>
                        সহকারী পুলিশ সুপার<br/>
                        বাংলাদেশ পুলিশ, গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br/>
                        মোবাইল: ০১৭১১-১৮৯৩৫৯                   
                    </div>
                </div>

                <div class="each_member">
                    <div class="members_image"><img src="<?php echo base_url(); ?>jahir.png"/></div>
                    <div class="members_details">
                        <strong>জহির উদ্দিন গাজী</strong><br/>
                        সহ-সভাপতি<br/>
                        বিদ্যুৎ উন্নয়ন বোর্ড, কুমিল্লা<br/>
                        মোবাইল: ০১৭১৪৭৫৪৩৩০                    
                    </div>
                </div>

                <div class="each_member">
                    <div class="members_image"><img src="<?php echo base_url(); ?>pitu.png"/></div>
                    <div class="members_details">
                        <strong> মুহাম্মদ নাসির উদ্দিন পিটু </strong><br/>
                        সহ-সভাপতি<br/>
                        সিনিয়র এক্সিকিউটিভ<br/>
                        ক্রয় বিভাগ,                         কাজী ফার্মস<br/>
                        মোবাইল: ০১৭১২৮৪২৩৫৩                    
                    </div>
                </div>

                <div class="each_member">
                    <div class="members_image"><img src="<?php echo base_url(); ?>arif.png"/></div>
                    <div class="members_details">
                        <strong> অরুন আরিফ </strong><br/>
                        যুগ্ম সাধারণ সম্পাদক<br/>
                        ব্যবসায়ী<br/>
                        মোবাইল: ০১৭১২৭৫৭৬৭৬                    
                    </div>
                </div>
                <hr/>



                <div class="clr"></div>
            </div>

        </div>
