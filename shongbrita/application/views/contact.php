
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
           
          <div class="page_heading"> 
                <p align="center">যোগাযোগ</p>               
            </div>
            
            <p>
                সংবৃতা আবৃত্তি চর্চা ও বিকাশ কেন্দ্র<br/>
                <stong>ঠিকানা:</stong><br/> টিএসসি, ঢাকা বিশ্ববিদ্যালয় <br/>
                <strong>মোবাইল:</strong><br/> ০১৭১১ ২২২৬৯৯ | ০১৭১১ ১৮৯৩৫৯ | ০১৭১৪ ৭৫৪৩৩০ | ০১৭১২ ৮৪২৩৫৩ | ০১৬৮৬ ৭৯৭০১০ <br/>
                <strong> ই-মেইল:</strong><br/> info@shongbrita.com                
            </p>
            <br/>
            <hr/>
            
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>    <!-- Google Maps API -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>   
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script>
    var geocoder;
var map;
var marker;

function initialize(){
  //MAP
  var latlng = new google.maps.LatLng(23.731219200000000000,	90.396215900000020000);
  var options = {
    zoom: 18,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), options);

  //GEOCODER
  geocoder = new google.maps.Geocoder();
/*
  marker = new google.maps.Marker({
    map: map,
    draggable: false
  });
  */
  var markerImage = {
        url: "http://placehold.it/64/ff0000", 
        size: new google.maps.Size(64, 64),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(32, 32)
   };
  
  marker = new google.maps.Marker({
        position: new google.maps.LatLng(23.731219200000000000,	90.396215900000020000),
        map: map,
        title: 'Some marker',
        //icon: markerImage,
		draggable: false
    });

	
  //CIRCLE 
  var circle = new google.maps.Circle({
    map: map,
    center: new google.maps.LatLng(23.731219200000000000,	90.396215900000020000),
    fillColor: '#204617',
    fillOpacity: 0.2,
    strokeColor: '#6DE953',
    strokeOpacity: 0.4,
    strokeWeight: 2
  });
 
  circle.setRadius(10000);
}
</script>

<script>
$(document).ready(function() { 

  initialize();

  $(function() {
    $("#address").autocomplete({
      //This bit uses the geocoder to fetch address values
      source: function(request, response) {
        geocoder.geocode( {'address': request.term }, function(results, status) {
          response($.map(results, function(item) {
            return {
              label:  item.formatted_address,
              value: item.formatted_address,
              latitude: item.geometry.location.lat(),
              longitude: item.geometry.location.lng()
            }
          }));
        })
      },
      //This bit is executed upon selection of an address
      select: function(event, ui) {
        var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
        marker.setPosition(location);
        map.setCenter(location);
      }
    });
    document.getElementById("address").focus();
  });
});


    </script>
    <style>
    /* style settings for Google map */
    #map_canvas
    {
        width : 650px;  /* map width */
        height: 500px;  /* map height */
    }
    </style>
</head>
<body onload="initialize()"> 
    <!-- Dislay Google map here -->
    <div id='map_canvas' ></div>
    <input id="address" type="textbox">
           <hr/>  
        </div>
       
      </div>
        