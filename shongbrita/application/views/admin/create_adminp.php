<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Form Elements</h2>

            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="<?php echo base_url(); ?>super_admin/create_admin_data" method="POST">
                <fieldset>
                    <legend>Article Add</legend>
                    <h3 align="center"><?php
                        $msg = $this->session->userdata('msg');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('msg');
                        }
                        ?></h3>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Full Name</label>
                        <div class="controls">
                            <input type="text" name="admin_full_name" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Email Address</label>
                        <div class="controls">
                            <input type="text" name="admin_email_address" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Password</label>
                        <div class="controls">
                            <input type="text" name="admin_password" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Access Level</label>
                        <div class="controls">
                            <input type="text" name="access_level" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    </div> 
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

