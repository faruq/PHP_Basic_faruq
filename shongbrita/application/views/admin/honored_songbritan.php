<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Form Elements</h2>

            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="<?php echo base_url(); ?>super_admin/save_honored_songbritan" method="POST" enctype="multipart/form-data">
                <fieldset>
                    <legend>Article Add</legend>
                    <h3 align="center"><?php
                        $msg = $this->session->userdata('msg');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('msg');
                        }
                        ?></h3>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">upoma</label>
                        <div class="controls">
                            <input type="text" name="upoma" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>

                                        <div class="control-group">
                        <label class="control-label" for="typeahead">full_name</label>
                        <div class="controls">
                            <input type="text" name="full_name" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">father_name</label>
                        <div class="controls">
                            <input type="text" name="father_name" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">mother_name</label>
                        <div class="controls">
                            <input type="text" name="mother_name" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">year</label>
                        <div class="controls">
                            <input type="text" name="year" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">year_eng</label>
                        <div class="controls">
                            <input type="text" name="year_eng" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">address</label>
                        <div class="controls">
                            <input type="text" name="address" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">mobile</label>
                        <div class="controls">
                            <input type="text" name="mobile" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">academic</label>
                        <div class="controls">
                            <input type="text" name="academic" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">abortan</label>
                        <div class="controls">
                            <input type="text" name="abortan" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>
                    
                    
                     <div class="control-group">
                        <label class="control-label"  for="fileInput">Picture [120px X 140px]</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="picture" id="fileInput" type="file">
                        </div>
                    </div> 
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

