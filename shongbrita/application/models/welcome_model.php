<?php

class Welcome_Model extends CI_Model {
    //put your code here
    public function select_article($per_page, $offset)
    {
//        $this->db->select('*');
//        $this->db->from('home_article');
//        $query_result=$this->db->get();
//        $result=$query_result->result();
//        return $result;
        if($offset==NULL)
        {
            $offset=0;
        }
        $sql="SELECT * FROM home_article ORDER BY home_article.date DESC LIMIT $offset, $per_page";
        $query_result=$this->db->query($sql);
        $result=$query_result->result();
        return $result;
    }
    
    public function online_aplication_db($data)
    {
        $this->db->insert('online_application', $data);
    }
    
    public function select_picture()
    {
        $this->db->select('*');
        $this->db->from('picture_gallery');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    public function certified_songbritan_db($per_page, $offset)
    {
       // $this->db->select('*');
       // $this->db->from('certified_songbritan');
       // $this->db->order_by()
        if($offset==NULL)
        {
            $offset=0;
        }
        $sql="SELECT * FROM certified_songbritan ORDER BY certified_songbritan.abortan_eng ASC LIMIT $offset, $per_page";
        $query_result=$this->db->query($sql);
        $result=$query_result->result();
        return $result;
    }
    
    public function honored_songbritan_db()
    {
        $sql="SELECT * FROM honored_songbritan ORDER BY honored_songbritan.year_eng ASC";
        $query_result=$this->db->query($sql);
        $result=$query_result->result();
        return $result;
    }
    
    public function complete_production()
    {
        $this->db->select('*');
        $this->db->from('complete_production');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
        public function executive_body()
    {
        $this->db->select('*');
        $this->db->from('executive_body');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
}
