<?php

class Super_Admin_Model extends CI_Model {
    //put your code here
    public function save_home_page_article_db($data)
    {
        $this->db->insert('home_article', $data);
    }
    
    public function certified_shongbritan_db($data)
    {
        $this->db->insert('certified_songbritan', $data);
    }
    
    public function executive_committee_db($data)
    {
        $this->db->insert('executive_committe', $data);
    }
    
    public function picture_gallery_upload_db($data)
    {
        $this->db->insert('picture_gallery', $data);
    }
    
    public function save_honored_songbritan_db($data)
    {
        $this->db->insert('honored_songbritan', $data);
    }
    
    public function create_admin_data_db($data)
    {
        $this->db->insert('tbl_admin', $data);
    }
            
}
