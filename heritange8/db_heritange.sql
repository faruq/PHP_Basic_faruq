-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2015 at 01:48 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_heritange`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
`sl` int(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`sl`, `name`, `email`, `phone`, `company_name`, `subject`, `message`) VALUES
(1, '0', '0', '0', '0', '0', '0'),
(2, 'faruqmojumder@gmail.com', 'faruqmojumder@gmail.com', '0125411', 'lkasdfkj', 'lkasdjf', 'sfsfsdafsadff'),
(3, 'faruqmojumder@gmail.com', 'faruq@yahoo.com', '45345454', 'lkasdfkj', 'lkasdjf', 'sfsfsdfsdf'),
(4, 'faruq', 'faruqmojumder@gmail.com', '74654353.', 'hdfh hdfhdfh', 'hdfhdfhfdh  dh dh dh', 'gagzs     dzgzdgd sgsd g'),
(5, 'faruq', 'faruqmojumder@gmail.com', '64634534', 'gsdgsdfgsfd', 'gwdfsdgsdfg', 'vbsfvfdzvzv f safafasf');

-- --------------------------------------------------------

--
-- Table structure for table `our_activities`
--

CREATE TABLE IF NOT EXISTS `our_activities` (
`id` int(5) NOT NULL,
  `title` varchar(256) NOT NULL,
  `long_description` text NOT NULL,
  `picture` varchar(256) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `our_activities`
--

INSERT INTO `our_activities` (`id`, `title`, `long_description`, `picture`, `status`, `date`) VALUES
(1, 'হাতে হাতে বৈধ অস্ত্র ', 'ফার্মগেটের পাবলিক টয়লেট চালান শাহ আলম। বর্তমান সরকার আমলে তিনি বৈধ অস্ত্রের লাইসেন্স পেয়েছেন। একই এলাকার টেম্পোস্ট্যান্ডের টাকা ওঠান চুন্নু। টেম্পোস্ট্যান্ড দখল নিয়ে তার বিরুদ্ধে মামলাও হয়েছিল। তবে তিনিও লাইসেন্সধারী বৈধ অস্ত্রের মালিক। ফুটপাথ বাণিজ্য করেন নজরুল। অস্ত্রের লাইসেন্স পেতে তিনিও বাদ যাননি। এরা তিনজনই ওয়ার্ড যুবলীগ নেতা। এদের বৈধ ব্যবসা বা সামাজিক কোনো অবস্থান না থাকলেও রাজনৈতিক বিবেচনায় বাগিয়ে নিয়েছেন বৈধ অস্ত্রের লাইসেন্স। বৈধ অস্ত্র নিয়ে প্রকাশ্যেই এরা ঘোরাফেরা করেন। অস্ত্রের লাইসেন্স প্রাপ্তির এই চিত্র এখন সারা দেশের। দলীয় পরিচয়ে ওয়ার্ড পর্যায়ের নেতা-কর্মীরা পর্যন্ত অস্ত্রের লাইসেন্স পাচ্ছেন। সংশ্লিষ্টরা বলছে, রাজনৈতিক কর্মী হলেই অস্ত্রের লাইসেন্স মিলছে। বাছ-বিচার ছাড়াই গণহারে দেওয়া হচ্ছে অস্ত্রের লাইসেন্স। এ সুযোগে সন্ত্রাসী-অপরাধী এমনকি রাস্তার লোকজনের অনেকেই রাজনৈতিক পরিচয়ে বৈধ অস্ত্র হাতিয়ে নিয়েছে। এ অবস্থায় হাতে হাতে এখন বৈধ অস্ত্র। আর এসব বৈধ অস্ত্র ব্যবহার হচ্ছে খুন-খারাবি থেকে শুরু করে টেন্ডারবাজিসহ নানা অপরাধ কর্মকাণ্ডে। গত ১৫ আগস্ট কুষ্টিয়ায় জাতীয় শোকদিবসের অনুষ্ঠানে, ক্ষমতাসীন দলের দুই গ্রুপের সংঘর্ষে একজন নিহতের পর ১০ জনের অস্ত্রের লাইসেন্স বাতিল করেছে প্রশাসন। এরাও দলীয় বিবেচনায় অস্ত্রের লাইসেন্স পেয়েছিলেন। একই ভাবে মহাজোট সরকার আমলে অস্ত্রের লাইসেন্স পেয়েছিলেন নারায়ণগঞ্জের সিদ্ধিরগঞ্জের গডফাদার হত্যাসহ অন্তত দুই ডজন মামলার আসামি নূর হোসেন ও তার সহযোগীরা। তাদের নামে অস্ত্রের লাইসেন্স দেওয়া হয় নয়টি। এর মধ্যে নূর হোসেনের নামেই দুটি।\r\n', 'activiets/031.jpg', 1, '2015-08-19'),
(2, 'প্রণবপত্নীর শেষকৃত্য সম্পন্ন', 'ভারতের রাষ্ট্রপতি প্রণব মুখার্জির স্ত্রী শুভ্রা মুখার্জির শেষকৃত্য সম্পন্ন হয়েছে। বুধবার সকালে দিল্লির লোদি রোডের শ্মশানে ‍বৈদ্যুতিক চুল্লিতে তার দাহ সম্পন্ন হয়।', 'activiets/02.jpg', 1, '0000-00-00'),
(3, 'ক্ষেতের মধ্যে ১৫ দিনের নবজাতক', 'রাজশাহীর পবা উপজেলার বায়া বালিকা উচ্চ বিদ্যালয়ের পাশের একটি ক্ষেত থেকে জীবিত নবজাতক উদ্ধার করেছে পুলিশ। বুধবার সকালে নবজাতকটিকে উদ্ধার করা হয়। পুলিশের ধারণা শিশুটির বয়স আনুমানিক ১৫ দিন।', 'activiets/013.jpg', 1, '0000-00-00'),
(4, 'ক্ষেতের মধ্যে ১৫ দিনের নবজাতক', 'রাজশাহীর পবা উপজেলার বায়া বালিকা উচ্চ বিদ্যালয়ের পাশের একটি ক্ষেত থেকে জীবিত নবজাতক উদ্ধার করেছে পুলিশ। বুধবার সকালে নবজাতকটিকে উদ্ধার করা হয়। পুলিশের ধারণা শিশুটির বয়স আনুমানিক ১৫ দিন।', 'activiets/013.jpg', 1, '0000-00-00'),
(5, 'হাতে হাতে বৈধ অস্ত্র ', 'ফার্মগেটের পাবলিক টয়লেট চালান শাহ আলম। বর্তমান সরকার আমলে তিনি বৈধ অস্ত্রের লাইসেন্স পেয়েছেন। একই এলাকার টেম্পোস্ট্যান্ডের টাকা ওঠান চুন্নু। টেম্পোস্ট্যান্ড দখল নিয়ে তার বিরুদ্ধে মামলাও হয়েছিল। তবে তিনিও লাইসেন্সধারী বৈধ অস্ত্রের মালিক। ফুটপাথ বাণিজ্য করেন নজরুল। অস্ত্রের লাইসেন্স পেতে তিনিও বাদ যাননি। এরা তিনজনই ওয়ার্ড যুবলীগ নেতা। এদের বৈধ ব্যবসা বা সামাজিক কোনো অবস্থান না থাকলেও রাজনৈতিক বিবেচনায় বাগিয়ে নিয়েছেন বৈধ অস্ত্রের লাইসেন্স। বৈধ অস্ত্র নিয়ে প্রকাশ্যেই এরা ঘোরাফেরা করেন। অস্ত্রের লাইসেন্স প্রাপ্তির এই চিত্র এখন সারা দেশের। দলীয় পরিচয়ে ওয়ার্ড পর্যায়ের নেতা-কর্মীরা পর্যন্ত অস্ত্রের লাইসেন্স পাচ্ছেন। সংশ্লিষ্টরা বলছে, রাজনৈতিক কর্মী হলেই অস্ত্রের লাইসেন্স মিলছে। বাছ-বিচার ছাড়াই গণহারে দেওয়া হচ্ছে অস্ত্রের লাইসেন্স। এ সুযোগে সন্ত্রাসী-অপরাধী এমনকি রাস্তার লোকজনের অনেকেই রাজনৈতিক পরিচয়ে বৈধ অস্ত্র হাতিয়ে নিয়েছে। এ অবস্থায় হাতে হাতে এখন বৈধ অস্ত্র। আর এসব বৈধ অস্ত্র ব্যবহার হচ্ছে খুন-খারাবি থেকে শুরু করে টেন্ডারবাজিসহ নানা অপরাধ কর্মকাণ্ডে। গত ১৫ আগস্ট কুষ্টিয়ায় জাতীয় শোকদিবসের অনুষ্ঠানে, ক্ষমতাসীন দলের দুই গ্রুপের সংঘর্ষে একজন নিহতের পর ১০ জনের অস্ত্রের লাইসেন্স বাতিল করেছে প্রশাসন। এরাও দলীয় বিবেচনায় অস্ত্রের লাইসেন্স পেয়েছিলেন। একই ভাবে মহাজোট সরকার আমলে অস্ত্রের লাইসেন্স পেয়েছিলেন নারায়ণগঞ্জের সিদ্ধিরগঞ্জের গডফাদার হত্যাসহ অন্তত দুই ডজন মামলার আসামি নূর হোসেন ও তার সহযোগীরা। তাদের নামে অস্ত্রের লাইসেন্স দেওয়া হয় নয়টি। এর মধ্যে নূর হোসেনের নামেই দুটি।\r\n', 'activiets/031.jpg', 1, '2015-08-19'),
(6, '‘মাস্তানি’ করতে নগরবাসীর সমর্থন চাইলেন আনিসুল', '‘মাস্তানি’ করতে নগরবাসীর সমর্থন চাইলেন ঢাকা সিটি উত্তরের মেয়র আনিসুল হক।\r\nযানজট-জলাবদ্ধতা ও বর্জ্য ব্যবস্থানা নিয়ে সমালোচনার মধ্যে শনিবার এক সংবাদ সম্মেলনে সড়কে উচ্ছেদে ‘মাস্তানি’ করতে তিনি নগরবাসীর সমর্থন চান।\r\nআনিসুল হক বলেন, “ মাস্তানি করে রাস্তা দখল করে ট্রাক রেখে দিয়েছে। আমি কি এখন তাদের সঙ্গে মাস্তানি করব?\r\n“আমি ব্যবসায়ী মানুষ, তবে মাস্তানি করতেই এখানে এসেছি,”।\r\nতিনি বলেন, “আমি চাই মানুষজন ব্যবসা করুক। বিলবোর্ড অ্যাসোসিয়েশনকে ডেকেছি, ইফতার খাইয়েছি, তিনশ লোককে খাইয়েছি। উনারা আমাকে ফরমালি চিঠি দিয়েছেন যে আমরা ইললিগাল এক হাজার বিলবোর্ড তুলে ফেলবে। কিন্তু গত জুন মাসে সব বিলবোর্ডর মেয়াদ চলে গেছে।\r\n“আমি তাদের বলেছি, আমি একজন ব্যবসায়ী, আপনাদের মারতে চাই না। আমি তিন মাস সময় দিয়েছি অবৈধ বিলবোর্ড খুলে ফেলার জন্য। তারা ১০ জনে মিলেও ৫০টা বিলবোর্ড খোলেনি।”\r\nআলোচনায় ডাকার পর তাতে সাড়া না দিয়ে বিলবোর্ড মালিকদের রাজপথে মানববন্ধন করার সমালোচনাও করেন মেয়র।', 'activiets/ad7_6.jpg', 1, '2015-09-05'),
(7, 'ছোট ছোট অপহরণের ঘটনা বেড়ে গেছে', 'বাংলাদেশের বিশেষ পুলিশ র‌্যাব বলছে, সম্প্রতি দেশে ছোট ছোট অপহরণের ঘটনা বেড়ে গেছে। গতরাতে ঢাকার কাছে সাভারে একজন অপহৃত ব্যবসায়ীকে উদ্ধার এবং আজ একটি অপহরণকারী চক্রের সাতজনকে আটক করার পর র‌্যাবের কর্মকর্তারা এ কথা বললেন।\r\nআজ গ্রেফতার হওয়া চক্রটির লোকেরা ডিবি পুলিশ পরিচয় দিয়ে অপহরণ করে মুক্তিপণ আদায় করতো বলে অভিযোগ।\r\nতবে অপহরণ বেড়ে যাওয়ার ব্যাপারে মানবাধিকার কর্মীরা অভিযোগ করছেন, বেশ কিছুকাল ধরেই র‌্যাব ও পুলিশের মতো আইনশৃঙ্খলা রক্ষাকারী বাহিনীর লোকদের বিরুদ্ধে যেসব অপহরণ-গুমে জড়িয়ে পড়ার অভিযোগ উঠছে, সেগুলো গুরুত্বের সাথে না নেয়াতেই দুর্বৃত্তরাও এখন এটা করতে বেশী উৎসাহ পাচ্ছে।\r\nকথা হচ্ছিল ঢাকার কাছে সাভারের একজন ক্ষুদ্র ব্যবসায়ী মোহাম্মদ পারভেজ-এর সাথে। তৈরি পোশাক কারখানার অবিক্রিত শার্ট কিনে নিয়ে স্থানীয় বাজারে বিক্রি করেন তিনি। এক বন্ধুকে নিয়ে অংশিদারী ব্যবসা তার।\r\nগত বৃহস্পতিবার সন্ধ্যেবেলায় তাকে অপহরণ করে নিয়ে যায় এক দল দুর্বৃত্ত, তার কাছে থাকা ৫০ হাজার টাকা ও একশো পিস শার্ট কেড়ে নেয় তারা এবং এক লাখ টাকা মুক্তিপণ দাবি করে।\r\nশুক্রবার গভীর রাতে তাকে উদ্ধার করে র‌্যাব, আটক করে একজন অপহরণকারীকে। অপহরণকারীদের কাছে চব্বিশ ঘন্টার বেশী সময়ে বন্দি ছিলেন মি: পারভেজ।\r\nআজ তাকে ও অভিযুক্ত অপহরণকারী এরশাদুজ্জামান রুবেলকে জবানবন্দী দেবার জন্য নেয়া হয়েছে। দেখা যাচ্ছে উদ্ধার পাবার পরও আতঙ্ক তাকে তাড়িয়ে বেড়াচ্ছে।\r\nযেমনটি মি: পারভেজ বলছেন, আটকাবস্থা থেকেও অভিযুক্ত এরশাদুজ্জামান তাকে ক্রমাগ্রত হুমকি দিয়ে যাচ্ছে, টেলিফোনে হুমকি দিচ্ছে পলাতক অপহরণকারীরাও।\r\nর‌্যাব বলছে, মুক্তিপণের জন্য এ ধরণের অপহরণের ঘটনা সম্প্রতি বেশ বেড়ে গেছে। সাভারে র‌্যাব চারের অধিনায়ক মেজর মাসুদুর রহমান:\r\nএদিকে ঢাকায় র‌্যাব আজ সকালে বিমানবন্দর এলাকা থেকে সঙ্ঘবদ্ধ এক অপহরণ চক্রের সাতজন সদস্যকে আটক করেছে।\r\nতাদের কাছ থেকে একটি মাইক্রোবাস, ডিবি লেখা স্টিকার, হাতকড়া ইত্যাদি জব্দ করা হয়েছে। র‌্যাব বলছে, বিভিন্ন সড়ক থেকে এরা ডিবি পুলিশ পরিচয় দিয়ে পথচারীদের তুলে নিতো এবং মুক্তিপণ আদায় করতো।\r\nবাংলাদেশে আইনশৃঙ্খলা রক্ষাকারী বাহিনী, পুলিশ র‌্যাব ইত্যাদি সংগঠনের বিরুদ্ধে সম্প্রতি অপহরণ, গুম ইত্যাদির অভিযোগ তুলছে বিভিন্ন মানবাধিকার সংস্থা ও ভূক্তভোগীরা।\r\nতাদের বিরুদ্ধে যে পন্থায় অপহরণের অভিযোগ আছে এখন দেখা যাচ্ছে দুর্বৃত্তরাও একই পদ্ধতিতে অপহরণ করছে মুক্তিপণ আদায়ের জন্য।\r\nমানবাধিকার সঙ্ঘঠন আইন ও শালিস কেন্দ্রের পরিচালক নুর খান লিটন বলছেন, আইনশৃঙ্খলা বাহিনীর বিরুদ্ধে ওঠা অপহরণের অভিযোগগুলো সরকার গুরুত্বের সাথে না নেয়ায় এখন দুর্বৃত্তরাও এতে উৎসাহিত হচ্ছে।\r\nএখানে উল্লেখ করা যেতে পারে, সম্প্রতি এক কথিত বন্দুকযুদ্ধে ঢাকায় ছাত্রলীগের এক নেতা নিহত হবার পর তার পরিবারের সদস্যরা র‌্যাবের এক কর্মকর্তাসহ কয়েকজন আইনশৃঙ্খলা রক্ষাকারী বাহিনীর সদস্যের বিরুদ্ধে অপহরণ ও হত্যা মামলা করে।\r\nএর আগেও বিভিন্ন সময় র‌্যাবের বিরুদ্ধে অপহরণ, গুম, বিচার বহির্ভূত হত্যা ইত্যাদি নানা অভিযোগ ওঠার পর সম্প্রতি র‌্যাবের তরফ থেকে বিবিসিকে জানানো হয়েছে, বাহিনীর সদস্যদের বিরুদ্ধে যেকোন অভিযোগ র‌্যাব স্বচ্ছতার সাথে তদন্ত করে ব্যবস্থা নিয়ে থাকে।', 'activiets/ad24.jpg', 1, '2015-09-05');

-- --------------------------------------------------------

--
-- Table structure for table `picture_gallery`
--

CREATE TABLE IF NOT EXISTS `picture_gallery` (
`id` int(4) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `picture_thumb` varchar(200) NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `picture_gallery`
--

INSERT INTO `picture_gallery` (`id`, `picture`, `picture_thumb`, `upload_date`) VALUES
(1, '0', '0', '2015-08-19 19:53:11'),
(2, 'picture/07.jpg', 'picture_thumb/thumb-12.jpg', '2015-08-19 19:56:16'),
(3, 'picture/11.jpg', 'picture_thumb/thumb-10.jpg', '2015-08-19 19:58:15'),
(4, 'picture/01.jpg', 'picture_thumb/thumb-01.jpg', '2015-08-19 20:23:26'),
(5, 'picture/01.jpg', 'picture_thumb/thumb-01.jpg', '2015-08-19 20:24:26'),
(6, 'picture/02.jpg', 'picture_thumb/thumb-02.jpg', '2015-08-19 20:24:46'),
(7, 'picture/03.jpg', 'picture_thumb/thumb-03.jpg', '2015-08-19 20:24:59'),
(8, 'picture/04.jpg', 'picture_thumb/thumb-04.jpg', '2015-08-19 20:25:17'),
(9, 'picture/05.jpg', 'picture_thumb/thumb-05.jpg', '2015-08-19 20:26:45'),
(10, 'picture/051.jpg', 'picture_thumb/thumb-051.jpg', '2015-08-19 20:50:58'),
(11, 'picture/011.jpg', 'picture_thumb/thumb-021.jpg', '2015-08-19 20:51:19'),
(12, 'picture/08.jpg', 'picture_thumb/thumb-08.jpg', '2015-08-20 06:05:05'),
(13, 'picture/11.jpg', 'picture_thumb/thumb-11.jpg', '2015-08-20 06:06:29');

-- --------------------------------------------------------

--
-- Table structure for table `recent_work`
--

CREATE TABLE IF NOT EXISTS `recent_work` (
`sl` int(5) NOT NULL,
  `category_name` varchar(25) NOT NULL,
  `category_description` varchar(100) NOT NULL,
  `picture1` varchar(100) NOT NULL,
  `picture2` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recent_work`
--

INSERT INTO `recent_work` (`sl`, `category_name`, `category_description`, `picture1`, `picture2`) VALUES
(1, 'gerwtgwe', 'regrqefgqegq', 'recent_workpc/11921688_389695321227637_5013998323743427379_n2.jpg', 'recent_workpc/11921688_389695321227637_5013998323743427379_n3.jpg'),
(2, 'nazmul hasan', 'etrwetgwe', 'recent_workpc/11921773_389695317894304_337619583219902356_n2.jpg', 'recent_workpc/11921773_389695317894304_337619583219902356_n3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(2) NOT NULL,
  `admin_full_name` varchar(50) NOT NULL,
  `admin_email_address` varchar(100) NOT NULL,
  `admin_password` varchar(32) NOT NULL,
  `access_label` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_full_name`, `admin_email_address`, `admin_password`, `access_label`) VALUES
(1, 'faruq', 'faruqmojumder@gmail.com', 'cb37e1e5eff7c1893df9845b406a150e', 1),
(2, 'prince', 'prince@gmail.com', '95e512b6a142a522c0f9e00580419056', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `our_activities`
--
ALTER TABLE `our_activities`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picture_gallery`
--
ALTER TABLE `picture_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recent_work`
--
ALTER TABLE `recent_work`
 ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
MODIFY `sl` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `our_activities`
--
ALTER TABLE `our_activities`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `picture_gallery`
--
ALTER TABLE `picture_gallery`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `recent_work`
--
ALTER TABLE `recent_work`
MODIFY `sl` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
