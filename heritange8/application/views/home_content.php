<section>
    <div>
        <!-- Start WOWSlider.com BODY section -->
        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                    <li><img src="<?php echo base_url(); ?>data1/images/115.jpg" alt="115" title="robi 3leg tower" id="wows1_0"/></li>
                    <li><img src="<?php echo base_url(); ?>data1/images/132.jpg" alt="132" title="NAMDB11 Site Pic.... Naogaon" id="wows1_1"/></li>
                    <li><img src="<?php echo base_url(); ?>data1/images/10686777_740501485996996_6171168105989360865_n.jpg" alt="electric tower" title="electric tower" id="wows1_2"/></li>
                    <li><img src="<?php echo base_url(); ?>data1/images/ingj.jpg" alt="10686777_740501485996996_6171168105989360865_n" title="4leg  tower" id="wows1_2"/></li>
                    <li><img src="<?php echo base_url(); ?>data1/images/good_morning__from_beautiful_quotes.jpg" alt="good_morning__from_beautiful_quotes" title="good_morning__from_beautiful_quotes" id="wows1_4"/></li>
                </ul></div>
            <div class="ws_bullets"><div>
                    <a href="#" title="115"><span><img src="<?php echo base_url(); ?>data1/tooltips/115.jpg" alt="115"/>1</span></a>
                    <a href="#" title="132"><span><img src="<?php echo base_url(); ?>data1/tooltips/132.jpg" alt="132"/>2</span></a>
                    <a href="#" title="10686777_740501485996996_6171168105989360865_n"><span><img src="<?php echo base_url(); ?>data1/tooltips/10686777_740501485996996_6171168105989360865_n.jpg" alt="10686777_740501485996996_6171168105989360865_n"/>3</span></a>
                    <a href="#" title="ingj.jpg"><span><img src="<?php echo base_url(); ?>data1/tooltips/10686777_740501485996996_6171168105989360865_n.jpg" alt="10686777_740501485996996_6171168105989360865_n"/>3</span></a>
                    <a href="#" title="good_morning__from_beautiful_quotes"><span><img src="<?php echo base_url(); ?>data1/tooltips/good_morning__from_beautiful_quotes.jpg" alt="good_morning__from_beautiful_quotes"/>5</span></a>
                </div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.com">jquery carousel</a> by WOWSlider.com v8.2</div>
            <div class="ws_shadow"></div>
        </div>	
        <script type="text/javascript" src="<?php echo base_url(); ?>engine1/wowslider.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>engine1/script.js"></script>
        <!-- End WOWSlider.com BODY section -->
    </div>
</section>



<section id="recent-works">
    <div class="container">
        <div class="center wow fadeInDown">


            <h2>Recent Works</h2>
            <p class="lead">Our Recent Work</p>
        </div>

        <div class="row">

            <?php
            foreach ($recent_work as $v_recent_work) {
                ?>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="<?php echo base_url(); ?><?php echo $v_recent_work->picture1 ?>" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#"><?php echo $v_recent_work->category_name ?></a> </h3>
                                <p><?php echo $v_recent_work->category_description ?></p>
                                <a class="preview" href="<?php echo base_url(); ?><?php echo $v_recent_work->picture2 ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                            </div> 
                        </div>
                    </div>
                </div>   
            <?php } ?>

        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#recent-works-->
<div>
    <section id="services" class="service-item">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Our Services</h2>

            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/services/services1.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Civil Works </h3>
                            <p>Roof Top,G.F & G.F.R.T Site</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/services/services2.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Telecom tower </h3>
                            <p>Tower Erection Maintenance and Painting.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/services/services3.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Power Substation</h3>
                            <p>Substation, Erection & Testing Commissioning.</p>
                        </div>
                    </div>
                </div>  

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/services/services4.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading"> Aviation Light </h3>
                            <p>Installation of Aviation Light. </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/services/services5.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Steel Building </h3>
                            <p>Erection & Fabrications</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/services/services6.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading"></h3>
                            <p></p>
                        </div>
                    </div>
                </div>

            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#services-->
</div>