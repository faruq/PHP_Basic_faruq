   
    <section id="about-us">
        <div class="container">
			<div class="center wow fadeInDown">
				<h2>Company Performance</h2>
                                <p class="lead"><strong>Finding Real World Solutions.</strong> Our works are not only best for today but also for tomorrow. We compromise with two things; a) Quality & b) Time duration of work. We can ensure these two because we have a reservoir of expert engineers and technical hands.</p>
                                <p>Looking ahead to the telecom infrastructure development technology that is expected to be the reality of the near future, we believe that our innovative effort will increase in importance and our technology will serve to fill the gaps in this specialized sector in our country Bangladesh.</p>
                                <p><strong>Heritage Associates</strong> has always placed an innovative technology and quality, making the most of our vast experience and many achievements in this sector, will continue to offer a verity of products from microwave tower to telecom hardware to build and support social infrastructure. Our basic and innovate R&D efforts will continue to improve our quality and technology a to provide new products and services of higher quality to meet not only local demand but also abroad.</p>
                                <p>We re-call our customers and grateful to them for giving us an opportunity to be specialized in the respective field. We hope that our activity in this specialized field will contribute to realizing a society of higher quality in our country in which we are involved.</p>
			</div>
						
			<!-- our-team -->
			<div class="team">
				<div class="center wow fadeInDown">
					<h2>COMPANY DETAILS</h2>
                                        <p class="lead">
                                        <form>
                                            Company Name &nbsp;&nbsp;&nbsp;	:&nbsp;&nbsp;<strong>	Heritage Associates</strong><br><br>
                                            
                                            Registration Number	 &nbsp;&nbsp;&nbsp; :  &nbsp;&nbsp; 	00002222<br><br>
                                            
                                            VAT Registration Number &nbsp;&nbsp;&nbsp;:   &nbsp;&nbsp;	5061889998<br><br>
                                            
                                            TIN      &nbsp;&nbsp;&nbsp;           :<br><br>
                                            
                                            Trade License Number    &nbsp;&nbsp;&nbsp;: <br><br>
                                            
                                            Corporate Office &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp; 	House-384/2,.West Shewrapara.  <br>Mirpur, Dhaka-1216. <br> <br> 
                                            
                                             E-mail address &nbsp;&nbsp;&nbsp;	: &nbsp;&nbsp;	heritage.associates@yahoo.com <br> <br> 
                                             
                                             Type of Company &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp; 	 1st Class Contractor & Supplier<br> <br> 
                                             
                                             Name of C. E .O. &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;  Md. Mamun Uddin Sheikh<br> <br> 
                                             
                                             Contact Person  &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;	Md.Mamun Uddin Sheikh,<br> Mobile – 01979200275, 01921200275.<br> <br> 
		                             
                                                                                                     
                                             Type of Work &nbsp;&nbsp;&nbsp;     : &nbsp;&nbsp;	Civil & Electro-Mechanical Construction, <br> Installation, Infrastructure & Telecommunication contractor,<br> Importer   & Supplier. Manpower Service.<br><br>
                                                 
                                             Bank Details  &nbsp;&nbsp;&nbsp;     :  &nbsp;&nbsp;Prime Bank Limited, Foreign Exchange Branch<br>Account type :Current<br>Account No : 12611010022054<br><br>
                                               
                                              Number of Employee  &nbsp;&nbsp;&nbsp;     :  &nbsp;&nbsp; 45 Nos.                                              
                                             
                                        </form>
                                            
                                        </p>
				</div>
                          </div>
                          <div class="center wow fadeInDown">
                            <h2>WORK DETAILS</h2>
                            
                               <li>
                               <li>All Civil Works : Roof Top, G.F & G.F.R.T Site.</li><br>
                               <li>Installation of tower Grounding System.</li><br>
                               <li>Installation of Aviation Light.</li><br>
                               <li>Telecom tower Maintenance and Painting work.</li><br>
                               <li>Supply work C-Sleeve / Connector + Cotton other Equipment</li><br>
                               <li>A.C. Controller.</li><br>
                               <li>A.C. Servicing.</li><br>
                               <li>Steel building Erection & Fabrications.</li><br>
                               <li>  Substation, Erection & Testing Commissioning.</li><br>
                               </li>  
                           
                        </div>
                        <div class="center wow fadeInDown" >
                            <h2>WORK EXPERIENCE</h2>
                            <p><strong># Grameen Phone ,Airtel , Robi, Bangla link Tower erection work  Co-Ordinate on Ahona Engineers under the Project of Confidence Steel Ltd.</strong></p><br><br>
                            <p><strong>#  Workshop on health, Safety and Environment at work  organized  by Grameen Phone and Confidence Steel Ltd. At Jamuna Resort.</strong></p><br><br>
                            <p><strong>#  Supervised civil site works & grounding works under project of  Airtel.   </strong></p><br><br>
    
                            
                            <p><strong>     #  Banglalink Tower Erection & Civil works Co-Ordinate On Bahadur Trading International Ltd. </strong></p><br><br>

                 

                        </div>
                      
		</div><!--/.container-->
    </section><!--/about-us-->