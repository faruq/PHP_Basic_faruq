<section id="about-us">
        <div class="container">
			<div class="center wow fadeInDown">
				<h2>Our Mission</h2>
				<p class="lead"><strong>Heritange Associates</strong> 
                               boasts a wide array of products (manpower) and services that caters to the needs and desires of local and international residents and businessmen
                               It emphasizes the whims and dreams of its customers. It reveals of its clients wishes.
                               We will continue to attain this through only representing companies who have come to understand and exceed the rigorous standards and expectations associated with doing business in today’s global economy.</p>
            	        </div>
			<!-- about us slider -->
			<div id="about-slider">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
				  	<ol class="carousel-indicators visible-xs">
					    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-slider" data-slide-to="1"></li>
					    <li data-target="#carousel-slider" data-slide-to="2"></li>
				  	</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img src="<?php echo base_url();?>images/slider_mission.jpg" class="img-responsive" alt=""> 
					   </div>
					   <div class="item">
							<img src="<?php echo base_url();?>images/slider_mission.jpg" class="img-responsive" alt=""> 
					   </div> 
					  
					</div>
					
					<a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
						<i class="fa fa-angle-left"></i> 
					</a>
					
					<a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
						<i class="fa fa-angle-right"></i> 
					</a>
				</div> <!--/#carousel-slider-->
			</div><!--/#about-slider-->
			</section>
          