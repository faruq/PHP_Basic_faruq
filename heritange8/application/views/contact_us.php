   <section id="contact-info">
        <div class="center">                
            <h2>Contact-Us</h2>
         </div>
        <div class="gmap-area">
            <div class="container">
                <div class="r ow">
                    <div class="col-sm-5 text-center">
                  
 <!DOCTYPE html>
<html>
<head>
    <title></title>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>    <!-- Google Maps API -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>   
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script>
    var geocoder;
var map;
var marker;

function initialize(){
  //MAP
  var latlng = new google.maps.LatLng(23.7903011,	90.3756939);
  var options = {
    zoom: 18,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), options);

  //GEOCODER
  geocoder = new google.maps.Geocoder();
/*
  marker = new google.maps.Marker({
    map: map,
    draggable: false
  });
  */
  var markerImage = {
        url: "http://placehold.it/64/ff0000", 
        size: new google.maps.Size(64, 64),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(32, 32)
   };
  
  marker = new google.maps.Marker({
        position: new google.maps.LatLng(23.7903011,	90.3756939),
        map: map,
        title: 'Some marker',
        //icon: markerImage,
		draggable: false
    });

	
  //CIRCLE 
  var circle = new google.maps.Circle({
    map: map,
    center: new google.maps.LatLng(23.7903011,90.3756939),
    fillColor: '#204617',
    fillOpacity: 0.2,
    strokeColor: '#6DE953',
    strokeOpacity: 0.4,
    strokeWeight: 2
  });
 
  circle.setRadius(10000);
}
</script>

<script>
$(document).ready(function() { 

  initialize();

  $(function() {
    $("#address").autocomplete({
      //This bit uses the geocoder to fetch address values
      source: function(request, response) {
        geocoder.geocode( {'address': request.term }, function(results, status) {
          response($.map(results, function(item) {
            return {
              label:  item.formatted_address,
              value: item.formatted_address,
              latitude: item.geometry.location.lat(),
              longitude: item.geometry.location.lng()
            }
          }));
        })
      },
      //This bit is executed upon selection of an address
      select: function(event, ui) {
        var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
        marker.setPosition(location);
        map.setCenter(location);
      }
    });
    document.getElementById("address").focus();
  });
});


    </script>
    <style>
    /* style settings for Google map */
    #map_canvas
    {
        width : 500px;  /* map width */
        height: 500px;  /* map height */
    }
    </style>
</head>
<body onload="initialize()"> 
    <!-- Dislay Google map here -->
    <div id='map_canvas' ></div>
   
</body>
</html>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <address>
                                    <h5>Head Office</h5>
                                    <p>384/2,West Shewrapara,Mirpur <br>
                                        Dhaka-1216.</p><br>
                                    Mobile :+8801921-200275.
                                    <p>
                                    Email Address:heritage.associates@yahoo.com</p>
                                </address>
                            </li>
                         </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->

    <section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2>Drop Your Message</h2>
            </div> 
            <div class="row contact-wrap"> 
                <div class="status alert alert-success" style="display: none"></div>
               
                <form method="post" action="<?php echo base_url();?>welcome/contact_us_data">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label>Name *</label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="number" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" name="company_name" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Subject *</label>
                            <input type="text" name="subject" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Message *</label>
                            <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->