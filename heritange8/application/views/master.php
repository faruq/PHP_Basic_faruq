<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $title;?></title>
	
	<!-- core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>js/respond.min.js"></script>
    <![endif]-->       
  </head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://www.facebook.com/pages/Heritage-Associates/358919184305251?pnref=story"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>images/logo.png" alt="logo" width="100px" height="100px"></a>
                </div>
		
                                 <div class="collapse navbar-collapse navbar-left">
                     <p class="name">Heritage Associates</p>
                     <p class="tagline">Finding real world solutions</p>
                     
                </div>
                
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo base_url();?>index.php/welcome/index.html">Home</a></li>
                        <li><a href="<?php echo base_url();?>welcome/activities.html">Our-Activities</a></li>
                         <li><a href="<?php echo base_url();?>welcome/about_us.html">About Us</a></li>
			 <li><a href="<?php echo base_url();?>welcome/image_galary.html">Image-Galary</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vision and Mission <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url();?>welcome/vision.html">Vision</a></li>
                                <li><a href="<?php echo base_url();?>welcome/mission.html">Mission</a></li>
                                <li><a href="<?php echo base_url();?>welcome/dhagram.html">Dhagram</a></li>
                            </ul>
                        </li>						
                        <li><a href="<?php echo base_url();?>welcome/contact_us.html">Contact</a></li>                        
                    </ul>
                </div>
                
                

                
                
            </div><!--/.container-->
        </nav><!--/nav-->
		<!-- Start WOWSlider.com HEAD section  slider-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>engine1/style.css" />
<script type="text/javascript" src="<?php echo base_url();?>engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
		
    </header><!--/header-->
<?php echo $maincontent;?>
    
     <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 <a target="_blank" href="#" title="">bangladesh</a>. All Rights Reserved.
                </div>
                
        </div>
    </footer><!--/#footer-->

    <script src="<?php echo base_url();?>js/jquery.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>js/jquery.isotope.min.js"></script>
    <script src="<?php echo base_url();?>js/main.js"></script>
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
</body>
</html>