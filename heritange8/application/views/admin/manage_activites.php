
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Tables</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th> Id</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Long Description</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    foreach($manageactiv as $v_category)
                    {
                    ?>
                    <tr>
                  
                   
                        <td class="center"><?php echo $v_category->id?></td>
                        <td class="center"><?php echo $v_category->title?></td>
                        <td class="center"><img  src="<?php echo base_url(). $v_category->picture ?>" width="150"></td>
                        <td class="center"><?php echo $v_category->long_description?></td>
                        <td class="center">
                            
                            <?php if( $v_category->status==1){
                           echo 'Published';
                             }else{
                                 echo 'Unpublished';
                             }
                                ?></td>
                         <td class="center">
                             <?php
                             if($v_category->status==1)
                             {
                             ?>
                             <a class="btn btn-success" href="<?php echo base_url();?>super_admin/unpublished_activ/<?php echo $v_category->id?>">
                                <i class="icon-lock icon-white"></i>  
                                                                         
                            </a>
                             <?php } 
                             
                             else{
                               ?>
                              <a class="btn btn-danger" href="<?php echo base_url();?>super_admin/published_activ/<?php echo $v_category->id?>">
                                <i class="icon-ok icon-white"></i>  
                                                                         
                            </a>
                             
                             <?php } ?>
                             <a class="btn btn-info" href="#" title="Edit">
                                <i class="icon-edit icon-white"></i>  
                                                                       
                            </a>
                             <a class="btn btn-danger" href="<?php echo base_url();?>super_admin/delete_activ/<?php echo $v_category->id?>" title="Delete">
                                <i class="icon-trash icon-white"></i> 
                              
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->
