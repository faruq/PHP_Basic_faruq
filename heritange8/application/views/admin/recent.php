<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Form Elements</h2>
           
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="<?php echo base_url();?>super_admin/manage_recent" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>Add Category</legend>
                    <h3>
                        <?php
                            $msg=$this->session->userdata('message');
                            if($msg)
                            {
                                echo $msg;
                                $this->session->unset_userdata('message');
                            }
                        ?>
                    </h3>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Title-Name </label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" name="category_name" >                            
                        </div>
                    </div>
                   
    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">recent-Description</label>
                        <div class="controls">
                            <textarea name="category_description" id="textarea2" rows="1"></textarea>
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label"  for="fileInput">Picture [290px X 220px]</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="picture1" id="fileInput" type="file">
                        </div>
                    </div> 
                    
                    
                    <div class="control-group">
                        <label class="control-label"  for="fileInput">Picture [600px X 455px]</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="picture2" id="fileInput" type="file">
                        </div>
                    </div> 
                 
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->