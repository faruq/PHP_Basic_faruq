<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Form Elements</h2>

            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="<?php echo base_url(); ?>super_admin/save_home_page_article" method="POST" enctype="multipart/form-data">
                <fieldset>
                    <legend>Article Add</legend>
                    <h3 align="center"><?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('msg');
                        }
                        ?></h3>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Article Heading</label>
                        <div class="controls">
                            <input type="text" name="title" class="span6 typeahead" id="typeahead"  >
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="textarea2">Article</label>
                        <div class="controls">
                            <textarea class_DISABLED="cleditor" name="long_description" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Date</label>
                        <div class="controls">
                            <input type="date" name="date" class="span6 typeahead" id="typeahead" placeholder="YYYY-MM-DD" >
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="fileInput">Picture [640px X 213px]</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="picture" id="fileInput" type="file">
                        </div>
                    </div> 

                    <div class="control-group">
                        <label class="control-label" for="textarea2"> Publication Status (<span style="color:red">*</span>)</label>
                        <div class="controls">
                            <select name="status" err="Please Select Publication Status" required exclude=" ">
                                <option value=" ">Select Publication Status</option>
                                <option value="1">Published</option>
                                <option value="0">Un Published</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->


