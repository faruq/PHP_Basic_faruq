<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Tables</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category Name</th>
                        <th>Category Description</th>
                        <th>Picture1</th>
                        <th>Picture2</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    foreach ($manage_recent_work_data as $v_manage_recent_work_data) {
                        ?>
                        <tr>
                            <td class="center"><?php echo $v_manage_recent_work_data->sl ?></td>
                            <td class="center"><?php echo $v_manage_recent_work_data->category_name ?></td>
                            <td class="center"><?php echo $v_manage_recent_work_data->category_description ?></td>
                            <td class="center"><img  src="<?php echo base_url() . $v_manage_recent_work_data->picture1 ?>" width="150"></td>
                            <td class="center"><img  src="<?php echo base_url() . $v_manage_recent_work_data->picture2 ?>" width="150"></td>                                                      

                            <td class="center">                               
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_recent_work/<?php echo $v_manage_recent_work_data->sl ?>" title="Delete">
                                    <i class="icon-trash icon-white"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->
