
    <section id="blog" class="container">
        
		<div class="blog">
            <div class="row">
                 <div class="col-md-8">
                   
                    <div class="blog-item">
                        <?php 
                        foreach($our_act as $v_our_act)
                        {
                            ?>
                        <div class="row">                                                            
                            <div class="col-xs-12 col-sm-10 blog-content">
                                <a href="#"><img class="img-responsive img-blog" src="<?php echo base_url();?><?php echo $v_our_act->picture?>" width="100%" alt="" /></a>
                                <h2><a href="#"><?php echo $v_our_act->title?></a></h2>
                                <h3><?php echo $v_our_act->long_description?></h3>
                              
                            </div>
                        </div>  
                        <?php 
                        }
                        ?>
                    </div><!--/.blog-item-->
                    <h3>Pagination</h3>
           <?php echo $this->pagination->create_links(); ?>
                        
                    
                </div><!--/.col-md-8-->

                <aside class="col-md-4">
                      				
    				<div class="widget categories">
                              
                    </div><!--/.recent comments-->
                     
                        				
    				<div class="widget archieve">
                        <h3>Archieve</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="blog_archieve">
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> December 2015 <span class="pull-right">(7)</span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> November 2014 <span class="pull-right">(32)</a></li>
                                   
                                </ul>
                            </div>
                        </div>                     
                    </div><!--/.archieve-->
    				
    			</aside>  
            </div><!--/.row-->
        </div>
    </section><!--/#blog-->
	
	