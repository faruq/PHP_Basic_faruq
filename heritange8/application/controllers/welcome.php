<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index() {
        $data = array();
        $data['title'] = 'Home';
        $data['recent_work'] = $this->welcome_model->recent_work();
        $data['maincontent'] = $this->load->view('home_content', $data, true);

        $this->load->view('master', $data);
    }

    public function activities() {
        $data = array();
        $data['title'] = 'Activities';  
        
        //----------pagination------------
        
        $this->load->library('pagination');

        $config['base_url'] = base_url().'welcome/activities/';
        $config['total_rows'] = $this->db->count_all('our_activities');
        $config['per_page'] = 3;
        $this->pagination->initialize($config);
        $data['our_act']=$this->welcome_model->select_ouract($config['per_page'], $this->uri->segment(3));
        $data['maincontent'] = $this->load->view('activities', $data, true);
        $this->load->view('master', $data);
    }
    
    
    public function about_us() {
        $data = array();
        $data['title'] = 'About-us';
        $data['maincontent'] = $this->load->view('about_us', '', true);
        $this->load->view('master', $data);
    }

    public function Image_galary() {
        $data = array();
        $data['title'] = 'image_galary';
        $data['image'] = $this->welcome_model->select_image();
        $data['maincontent'] = $this->load->view('image_galary', $data, true);
        $this->load->view('master', $data);
    }

    public function vision() {
        $data = array();
        $data['title'] = 'Vision';
        $data['maincontent'] = $this->load->view('vision', '', true);
        $this->load->view('master', $data);
    }

    public function mission() {
        $data = array();
        $data['title'] = 'Mission';
        $data['maincontent'] = $this->load->view('mission', '', true);
        $this->load->view('master', $data);
    }

    public function dhagram() {
        $data = array();
        $data['title'] = 'Dhagram';
        $data['maincontent'] = $this->load->view('dhagram', '', true);
        $this->load->view('master', $data);
    }

    public function contact_us() {
        $data = array();
        $data['title'] = 'Contact_Us';
        $data['maincontent'] = $this->load->view('contact_us', '', true);
        $this->load->view('master', $data);
    }

    public function contact_us_data() {
        $data = array();
        $data['name'] = $this->input->POST('name');
        $data['email'] = $this->input->POST('email');
        $data['phone'] = $this->input->POST('phone');
        $data['company_name'] = $this->input->POST('company_name');
        $data['subject'] = $this->input->POST('subject');
        $data['message'] = $this->input->POST('message');

        $this->welcome_model->contact_us_data_db($data);
        redirect('welcome/contact_us');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */