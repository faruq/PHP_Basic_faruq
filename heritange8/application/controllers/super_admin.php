<?php
session_start();
class Super_Admin extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id == NULL)
        {
            redirect('admin','refresh');
        }
       // $this->load->model('super_admin_model');
    }
    
    public function index()
    {
        $data=array();
        $data['admin_maincontent']=$this->load->view('admin/deshbord','',true);
        $this->load->view('admin/admin_master',$data);
    }
       
    public function save_category()
    {
        $data=array();
        $data['category_name']=$this->input->post('category_name',true);
        $data['category_description']=$this->input->post('category_description',true);
        $data['publication_status']=$this->input->post('publication_status',true);
        $this->super_admin_model->save_category_info($data);
        $sdata=array();
        $sdata['message']='Save Category Information Successfully !';
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_category');
    }
    
      public function recent_work()
    {
        $data=array();
        $data['admin_maincontent']=$this->load->view('admin/recent','',true);
        $this->load->view('admin/admin_master',$data);
    }
    
     public function manage_category()
    {
        $data=array();
        $data['manage_recent']=$this->super_admin_model->select_all_category();
        $data['admin_maincontent']=$this->load->view('admin/manage_recent',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function unpublished_category($category_id)
    {
        $this->super_admin_model->unpublished_category_info($category_id);
        redirect('super_admin/manage_category');
    }
    
    public function published_category($category_id)
    {
        $this->super_admin_model->published_category_info($category_id);
        redirect('super_admin/manage_category');
    }
    
      public function add_image()
    {
        $data=array();
        $data['admin_maincontent']=$this->load->view('admin/add_image_from','',true);
        $this->load->view('admin/admin_master',$data);
    }
    public function picture_gallery_upload(){
        $data=array(); 
        $config['upload_path'] = 'picture/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error='';
        $fdata=array();
        
        
        if ( ! $this->upload->do_upload('picture'))
        {
                $error = $this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('super_admin/add_image');  
             
        }
        else
        {
                $fdata = $this->upload->data();
                $data['picture']=$config['upload_path'] .$fdata['file_name'];
                
        }
        
        $config['upload_path'] = 'picture_thumb/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '500';
        $config['max_width']  = '72';
        $config['max_height']  = '72';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error='';
        $fdata=array();
        
        
        if ( ! $this->upload->do_upload('picture_thumb'))
        {
                $error = $this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('super_admin/add_image');  
             
        }
        else
        {
                $fdata = $this->upload->data();
                $data['picture_thumb']=$config['upload_path'] .$fdata['file_name'];
        }
        
        $this->super_admin_model->save_image($data);
        $sdata=array();
        $sdata['message']='Save image Information Successfully !';
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_image');
    }
    
    public function our_act(){
        $data=array();
        $data['admin_maincontent']=$this->load->view('admin/our-activieties','',true);
        $this->load->view('admin/admin_master',$data);
    }
    public function save_home_page_article(){
        
        $data=array(); 
        $data['title']=$this->input->post('title',true);
        $data['long_description']=$this->input->post('long_description',true);
        $data['status']=$this->input->post('status',true);
        $data['date']=$this->input->post('date',true);
        
        $config['upload_path'] = 'activiets/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error='';
        $fdata=array();
        
        
        if ( ! $this->upload->do_upload('picture'))
        {
                $error = $this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('super_admin/add_image');  
             
        }
        else
        {
                $fdata = $this->upload->data();
                $data['picture']=$config['upload_path'] .$fdata['file_name'];
                
        }
        
        $this->super_admin_model->save_activiets($data);
        $sdata=array();
        $sdata['message']='Save Actvites Information Successfully !';
        $this->session->set_userdata($sdata);
        redirect('super_admin/our_act');
        
    }
    public function manage_activies(){
        $data=array();
        $data['manageactiv']=$this->super_admin_model->select_all_activ();
        $data['admin_maincontent']=$this->load->view('admin/manage_activites',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function unpublished_activ($id){
      $this->super_admin_model->unpublished_activ($id);  
      redirect('super_admin/manage_activies');
    }
    public function published_activ($id){
      $this->super_admin_model->published_activ($id); 
      redirect('super_admin/manage_activies');
    }
    public function delete_activ($id){
      $this->super_admin_model->delete_activ($id); 
      redirect('super_admin/manage_activies');
    }
    

    
    
    
     public function manage_recent(){
        
        $data=array(); 
        $data['category_name']=$this->input->post('category_name',true);
        $data['category_description']=$this->input->post('category_description',true);
      
        $config['upload_path'] = 'recent_workpc/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error='';
        $fdata=array();
        
        
        if ( ! $this->upload->do_upload('picture1'))
        {
                $error = $this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('super_admin/add_image');  
             
        }
        else
        {
                $fdata = $this->upload->data();
                $data['picture1']=$config['upload_path'] .$fdata['file_name'];
                
        }
        
        
        
                if ( ! $this->upload->do_upload('picture2'))
        {
                $error = $this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('super_admin/add_image');  
             
        }
        else
        {
                $fdata = $this->upload->data();
                $data['picture2']=$config['upload_path'] .$fdata['file_name'];
                
        }
        
        
        $this->super_admin_model->save_recent_pic($data);
        $sdata=array();
        $sdata['message']='Save Actvites Information Successfully !';
        $this->session->set_userdata($sdata);
        redirect('super_admin/our_act');
        
    }
    
    
    //Start Manage Recent work
    
      public function delete_recent_work($sl){
      $this->super_admin_model->delete_recent_work_db($sl); 
      redirect('super_admin/manage_recent_work');
    }
    
           public function manage_recent_work(){
        $data=array();
        $data['manage_recent_work_data']=$this->super_admin_model->manage_recent_work_data();
        $data['admin_maincontent']=$this->load->view('admin/manage_recent_work',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    
    //End Manage Recent Work
    
    
        
    public function logout()
    {
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_full_name');
        $sdata=array();
        $sdata['message']='You are successfully logout !';
        $this->session->set_userdata($sdata);
        redirect('admin/index');
    }
}
