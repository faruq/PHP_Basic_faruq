<?php

class Super_Admin_Model extends CI_Model {
    //put your code here
    public function recent_work($data)
    {
        $this->db->insert('recent_work',$data);
    }
    public function save_image($data){
        $this->db->insert('picture_gallery',$data);
    }
    public function save_activiets($data){
        $this->db->insert('our_activities',$data);
    }
    public function select_all_activ(){
        $this->db->select('*');
        $this->db->from('our_activities');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function unpublished_activ($id){
        $this->db->set('status',0);
        $this->db->where('id',$id);
        $this->db->update('our_activities');
    }
    public function published_activ($id){
        $this->db->set('status',1);
        $this->db->where('id',$id);
        $this->db->update('our_activities');
    }
    public function delete_activ($id){
        $this->db->where('id',$id);
         $this->db->delete('our_activities');
    }

    public function manage_recent()
    {
        $this->db->select('*');
        $this->db->from('recent_work');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function unpublished_category_info($category_id)
    {
        $this->db->set('publication_status',0);
        $this->db->where('category_id',$category_id);
        $this->db->update('recent_work');
    }
    
     public function published_category_info($category_id)
    {
        $this->db->set('publication_status',1);
        $this->db->where('category_id',$category_id);
        $this->db->update('recent_work');
    }
    
       public function save_recent_pic($data){
        $this->db->insert('recent_work',$data);
    }
    
        public function manage_recent_work_data(){
        $this->db->select('*');
        $this->db->from('recent_work');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
        public function delete_recent_work_db($sl){
        $this->db->where('sl',$sl);
         $this->db->delete('recent_work');
    }
    
    
    
}
 