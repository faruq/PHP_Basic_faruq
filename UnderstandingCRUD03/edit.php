<?php
include_once('lib/app.php');

$data = get();


?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Understanding CRUD : Edit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>
        <form action="store.php" method="post">
            <input type="hidden" name="id" value="<?php echo $_GET['id']?>" />
            <h1> Edit...</h1>
            <p> Information goes here </p>
            
            <fieldset><legend>Section Title</legend>
                <ul>
                    <li><label for="email" >Enter your email </label> 
                        <input  type="text" name="email" id="email" autofocus="autofocus" placeholder="ex. john@yahoo.com" value="<?php if(array_key_exists('email',$data) && !empty($data['email'])){   
            echo $data['email'];
        }else{
            echo "";
        } ?>" />
                    </li>
                    <li><label for="fullname" >Enter your full name </label> 
                        <input  type="text" name="fullname" id="fullname"  placeholder="ex. John Smith" value="<?php if(array_key_exists('fullname',$data) && !empty($data['fullname'])){   
            echo $data['fullname'];
        }else{
            echo "";
        }?>" />
                    </li>
                </ul>
                <input type="submit" name="btnSave" value="Submit" />
                 <input type="reset" name="reset" value="Reset" />
            </fieldset>
                        
            
        </form>
            
            <a href="index.php">Back</a>
            
        </section>
    </body>
</html>
