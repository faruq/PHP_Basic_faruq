<?php
include_once('lib/app.php');
$data = all();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head> 
    <body>
        <p><a href="create.html">Click Here</a> to add an email</p>
        
        |Search|
        |Download PDF | XL |
        <table border="1">
            <tr>
                <th>Sl</th>
                <th>Email</th>
                <th>Full Name</th>
                <th>Action</th>
            </tr>
           <?php
           
           if( !empty($data)){
           foreach($data as $key=>$value){
          ?>    
           
            <tr>
                <td><?php echo $key+1;?></td>
                <td><?php 
                if(array_key_exists('email', $value) && !empty($value['email'])){
                    echo $value['email'];
                }
                  ?></td>
                <td><?php
                if(array_key_exists('fullname', $value) && !empty($value['fullname'])){
                    echo $value['fullname'];
                }
                ?></td>
                <td>
                    <a href="show.php?id=<?php echo $key;  ?>">View</a>
                    |<a href="edit.php?id=<?php echo $key;  ?>">Edit</a>
                    |<a href="delete.php?id=<?php echo $key;  ?>">Delete</a>
                </td>
            </tr>
            <?php
             }
           }else{
           ?>
            <tr><td colspan="4">
                No data available
                </td>  
            </tr>
            
            <?php
           }
            ?>
        </table>
        |Paging|
    </body>
</html>
