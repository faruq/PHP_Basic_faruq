<?php
include_once('inc/application.php');
//debug($_SESSION);
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>home</title>
</head>
<body>
	<p><a href="create.html">Create</a> a new record</p>
	<p><?php echo $_SESSION['message']; ?></p>
	<section>
		<table border="1">
			<tr>
				<th>Sl</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Action</th>
			</tr>
			<?php
			foreach($_SESSION['info'] as $key => $value) {
			?>
			<tr>
				<td><?php echo $key; ?></td>
				<td>
				<?php
				if(array_key_exists('f_name', $value) &&!empty($value['f_name'])){
					echo $value['f_name'];
				}
				else{
					echo "not define";
				}
				?>
				</td>
				<td>
				<?php
				if(array_key_exists('l_name', $value) &&!empty($value['l_name'])){
					echo $value['l_name'];
				}
				else{
					echo "not define";
				}
				?>
				</td>
				<td>
				<?php
				if(array_key_exists('email', $value) &&!empty($value['email'])){
					echo $value['email'];
				}
				else{
					echo "not define";
				}
				?>
				</td>
				<td>
					<a href="show.php?id=<?php echo $key; ?>">View</a> |
					<a href="#">Edit</a> |
					<a href="delete.php?id=<?php echo $key; ?>">Delete</a>
				</td>
			</tr>
			<?php			
			}
			?>
		</table>
	</section>
</body>
</html>