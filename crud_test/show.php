<?php
include_once('inc/application.php');

$data = $_SESSION['info'][$_GET['id']];
//debug($_SESSION['info'][$_GET['id']]);
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>view</title>
</head>
<body>
	<h2>View page</h2>
	<section>
		<dl>
			<dt>First Name</dt>
			<dd>
			<?php
				if(array_key_exists('f_name', $data) &&!empty($data['f_name'])){
					echo $data['f_name'];
				}
				else{
					echo "not define";
				}
			?>
			</dd>
			<dt>Last Name</dt>
			<dd>
			<?php
				if(array_key_exists('l_name', $data) &&!empty($data['l_name'])){
					echo $data['l_name'];
				}
				else{
					echo "not define";
				}
			?>
			</dd>
			<dt>Email</dt>
			<dd>
			<?php
				if(array_key_exists('email', $data) &&!empty($data['email'])){
					echo $data['email'];
				}
				else{
					echo "not define";
				}
			?>
			</dd>
		</dl>
	</section>
	<nav>
		<li><a href="index.php">list</a></li>
		<li><a href="create.html">Create</a></li>
	</nav>
</body>
</html>